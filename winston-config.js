var winston = require("winston");
var path = require("path");
var fs = require("fs");
var appRoot = require("app-root-path");
require("winston-daily-rotate-file");

var logDirectory = path.resolve(`${appRoot}`, "logs");
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  http: 3,
  debug: 4,
};

const level = () => {
  const env = process.env.NODE_ENV || "development";
  const isDevelopment = env === "development";
  return isDevelopment ? "debug" : "warn";
};

const colors = {
  error: "red",
  warn: "yellow",
  info: "green",
  http: "magenta",
  debug: "white",
};

winston.addColors(colors);

const format = winston.format.combine(
  // Add the message timestamp with the preferred format
  winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss:ms" }),
  // Tell Winston that the logs must be colored
  winston.format.colorize({ all: true }),
  // Define the format of the message showing the timestamp, the level and the message
  winston.format.printf(
    (info) => `${info.timestamp} ${info.level}: ${info.message}`
  )
);

// Define which transports the logger must use to print out messages.
// In this example, we are using three different transports
const transports = [
  // Allow the use the console to print the messages
  new winston.transports.Console(),
  // Allow to print all the error level messages inside the error.log file
  new winston.transports.DailyRotateFile({
    level: "error",
    filename: path.resolve(logDirectory, "selfcare-%DATE%-error.log"),
    datePattern: "YYYY-MM-DD",
    zippedArchive: false,
    maxSize: "20m",
    maxFiles: "30d", // keep logs for 30 days
  }),

  new winston.transports.DailyRotateFile({
    level: "info",
    filename: path.resolve(logDirectory, "selfcare-%DATE%-info.log"),
    datePattern: "YYYY-MM-DD",
    zippedArchive: false,
    maxSize: "100m",
    maxFiles: "14d", // keep logs for 14 days
  }),
];

const logger = winston.createLogger({
  leval: level(),
  levels,
  format,
  transports,
});

// create a stream object with a 'write' function that will be used by `morgan`. This stream is based on node.js stream https://nodejs.org/api/stream.html.
logger.stream = {
  write: function (message, encoding) {
    // use the 'info' log level so the output will be picked up by both transports
    logger.http(message);
  },
};

// create a format method for winston, it is similar with 'combined' format in morgan
logger.combinedFormat = function (err, req, res) {
  console.log("jestem w combinedFormat");
  // :remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"
  return `${req.ip} - - [${clfDate(new Date())}] \"${req.method} ${
    req.originalUrl
  } HTTP/${req.httpVersion}\" ${err.status || 500} - ${
    req.headers["user-agent"]
  }`;
  /*return `${clfDate(new Date())} - ${req.method} ${req.originalUrl} HTTP/${req.httpVersion}  ${err.status ||
        500} - ${req.headers["user-agent"]}`*/
};

module.exports = logger;

let env = require("dotenv").config();
const dotenvParseVariables = require("dotenv-parse-variables");
env = dotenvParseVariables(env.parsed);
console.log(env);

// const jwt = require("jsonwebtoken");
// var LocalStorage = require("node-localstorage").LocalStorage,
//   localStorage = new LocalStorage("./generator/codes");

const parseDriveCodeTimestampFromHoursString = (hour) => {
  // console.log({ hour });
  // console.log(typeof hour);
  // console.log(hour.getTime());
  let d = new Date(); // Creates a Date Object using the clients current time

  let [hours, minutes, seconds] = hour.split(":");

  d.setHours(+hours); // Set the hours, using implicit type coercion
  d.setMinutes(minutes); // can pass Number or String - doesn't really matter
  d.setSeconds(seconds);

  return d.getTime();
};
// generate tokens for user to connect external device (local storage as mini database) with 6 digit number
// uzytkownik przychodzi i prosi o kod -> sprawdzamy czy jest kod w bazie i czy jest ważny przynajmniej minutę do końca ---> jak tak to go dajemy uzytkownikowi, a jak nie to generujemy nowy i wpisujemy do bazy
exports.getNetworkDriveCode = (knex, userID, expirationTime = 600000) => {
  return new Promise(async (resolve, reject) => {
    // 1. sprawdzenie czy w bazie mamy jakis kod wygenerowany dla uzytkownika o zadanym ID
    const postgresql = require("../google_cloud/google.cloud.postgresql");
    let sqlData;
    try {
      // await postgresql.connect();
      sqlData = await postgresql.selectDriveCode(knex, { user_id: userID });
      console.log({ sqlData });
      // await postgresql.close();
    } catch (err) {
      // ...
      console.log({ err });
      // try {
      //   await postgresql.close();
      // } catch (err) {
      //   console.log({ err });
      //   // ...
      // }

      reject("cannot get drive code from postgresql...");
    }

    const timestamp = new Date().getTime();
    let drive_code_valid_to;
    if (sqlData.length > 0 && sqlData[0].drive_code_valid_to) {
      if (typeof sqlData[0].drive_code_valid_to != "object") {
        drive_code_valid_to = parseDriveCodeTimestampFromHoursString(
          sqlData[0].drive_code_valid_to
        );
      } else {
        drive_code_valid_to = sqlData[0].drive_code_valid_to.getTime();
      }
    }

    console.log({ drive_code_valid_to });

    if (
      sqlData.length > 0 &&
      sqlData[0].drive_code &&
      drive_code_valid_to &&
      drive_code_valid_to > timestamp - 60000
    ) {
      // drive code is valid
      console.log(`drive code in db is valid more then minute...`);
      resolve(sqlData[0].drive_code);
    } else {
      // none or code expired
      console.log(`no drive code or drive code is expired in db...`);
      const generator = require("./random_generator");
      let randomNumber = generator.random();
      const valid_to = new Date().getTime() + expirationTime;
      console.log({ randomNumber }, { valid_to });
      console.log(`saving new drive code to db...`);

      // check if there exist such a code in postgres db
      let allCodes;
      try {
        // await postgresql.connect();
        allCodes = await postgresql.selectAllDriveCodes(knex);
        console.log({ allCodes });
        // await postgresql.close();
        resolve(randomNumber);
      } catch (err) {
        // ...
        console.log({ err });
        // try {
        //   await postgresql.close();
        // } catch (err) {
        //   console.log({ err });
        //   // ...
        // }

        reject("drive code couldn't be saved in db...");
      }

      const verifyDuplicate = () => {
        if (allCodes.length > 0) {
          for (let i = 0; i < allCodes.length; i++) {
            if (allCodes[i].drive_code === randomNumber) {
              // duplicate code --> needs to be changed
              console.log(`found duplicate... change random number`);
              randomNumber = generator.random();
              verifyDuplicate();
            }
          }
        }
      };
      verifyDuplicate();

      // set proper drive code to user
      try {
        // await postgresql.connect();
        await postgresql.setDriveCode(knex, {
          user_id: userID,
          drive_code: randomNumber,
          drive_code_valid_to: valid_to,
        });
        console.log(`drive code saved in db...`);
        // await postgresql.close();
        resolve(randomNumber);
      } catch (err) {
        console.log({ err });
        // ...
        // try {
        //   await postgresql.close();
        // } catch (err) {
        //   console.log({ err });
        //   // ...
        // }
        reject("drive code couldn't be saved in db...");
      }
    }

    // if (localStorage.getItem(userID)) {
    //   const tokenVerified = jwt.verify(
    //     localStorage.getItem(userID),
    //     env.SECRET,
    //     (err, decode) => {
    //       if (err) {
    //         console.log("generating new token beacuse of expired...");
    //         const token = signToken(
    //           { code: randomNumber, data: data },
    //           expirationTime
    //         );
    //         console.log("token saved in local storage....");
    //         localStorage.setItem(userID, token);
    //         resolve(token);
    //       } else {
    //         console.log(decode);
    //         console.log("token is valid...");
    //         resolve(localStorage.getItem(userID));
    //       }
    //     }
    //   );
    // } else {
    //   console.log("generating new token...");
    //   const token = signToken(
    //     { code: randomNumber, data: data },
    //     expirationTime
    //   );
    //   localStorage.setItem(userID, token);
    //   console.log("token saved in local storage....");
    //   resolve(token);
    // }
  });
};

// const signToken = (data, expirationTime) => {
//   const token = jwt.sign(data, env.SECRET, {
//     expiresIn: expirationTime,
//   });
//   console.log({ token });
//   return token;
// };

exports.verifyNetworkDriveCode = (knex, code) => {
  return new Promise(async (resolve, reject) => {
    const postgresql = require("../google_cloud/google.cloud.postgresql");
    let sqlData;
    try {
      // await postgresql.connect();
      // zwraca user_id, drive_code i drive_code_valid_to
      sqlData = await postgresql.selectUserByDriveCode(knex, {
        drive_code: code,
      });
      console.log(`=== user found in db by drive code === `, { sqlData });
      // await postgresql.close();
    } catch (err) {
      // ...
      // try {
      //   await postgresql.close();
      // } catch (err) {
      //   // ...
      // }
      reject("cannot get user from postgresql by drive code...");
    }

    const timestamp = new Date().getTime();

    let drive_code_valid_to;
    if (sqlData.length > 0 && sqlData[0].drive_code_valid_to) {
      if (typeof sqlData[0].drive_code_valid_to != "object") {
        drive_code_valid_to = parseDriveCodeTimestampFromHoursString(
          sqlData[0].drive_code_valid_to
        );
      } else {
        drive_code_valid_to = sqlData[0].drive_code_valid_to.getTime();
      }
    }
    console.log({ drive_code_valid_to });

    if (
      sqlData.length > 0 &&
      sqlData[0].drive_code &&
      drive_code_valid_to &&
      drive_code_valid_to > timestamp
    ) {
      const user_id = sqlData[0].user_id;
      // valid code
      console.log(`code ${code} is valid, resolving user ${user_id}`);
      // user was validated so we can delete code
      try {
        // await postgresql.connect();
        await postgresql.setDriveCode(knex, {
          user_id: user_id,
          drive_code: null,
          drive_code_valid_to: null,
        });
        // await postgresql.close();
      } catch (err) {
        // ...
        // try {
        //   await postgresql.close();
        // } catch (err) {}
      }
      resolve(user_id);
    } else {
      reject(`invalid code`);
    }

    // const length = localStorage.length;
    // let token;
    // for (let i = 0; i < length; i++) {
    //   const key = localStorage.key(i);
    //   const value = localStorage.getItem(key);
    //   console.log({ key });
    //   console.log({ value });
    //   if (jwt.decode(value)?.code === code) {
    //     console.log("==== user found ===");
    //     token = value;
    //     localStorage.removeItem(key);
    //   } else console.log("user not found....");
    // }
    // resolve(token);
  });
};

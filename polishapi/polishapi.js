let env = require("dotenv").config();
const dotenvParseVariables = require("dotenv-parse-variables");
env = dotenvParseVariables(env.parsed);
const Logger = require("../winston-config");

const createId = () => {
  length = 32;
  var result = "";
  var characters = "abcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  var result_converted =
    result.slice(0, 8) +
    "-" +
    result.slice(8, 12) +
    "-" +
    result.slice(12, 16) +
    "-" +
    result.slice(16, 20) +
    "-" +
    result.slice(20);
  return result_converted;
};

exports.authorize = () => {
  return new Promise((resolve, reject) => {
    let url = env.PKOBP_API_AUTHORIZE_URL;
    const id = createId();
    const data = {
      scope: "ais",
      redirect_uri: env.PKOBP_API_REDIRECT_URL,
      response_type: "code",
      requestHeader: {
        requestId: id,
        tppId: env.PKOBP_API_TPP_ID,
      },
      client_id: env.PKOBP_API_CLIENT_ID,
      scope_details: {
        privilegeList: [
          {
            "ais:getAccount": {
              scopeUsageLimit: "multiple",
            },
            "ais:getTransactionsDone": {
              maxAllowedHistoryLong: 50,
              scopeUsageLimit: "multiple",
            },
          },
        ],
        scopeGroupType: "ais",
        throttlingPolicy: "psd2Regulatory",
        consentId: "TPP-CONSENT-ID-123456",
        scopeTimeLimit: "2019-12-28T00:00:00.000Z",
      },
      state: "STATE123456",
    };

    const config = {
      headers: {
        "X-REQUEST-ID": id,
        "X-JWS-SIGNATURE": env.PKOBP_API_JWS,
      },
    };

    Logger.info(`PKOBP API authorize request sent!`);
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .post(url, data, config)
      .then((response) => {
        const data = response.data;
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.access_token = (code) => {
  return new Promise((resolve, reject) => {
    let url = env.PKOBP_API_ACCESS_TOKEN_URL;
    const id = createId();
    const data = {
      requestHeader: {
        requestId: id,
        tppId: env.PKOBP_API_TPP_ID,
        client_id: env.PKOBP_API_CLIENT_ID,
      },
      grant_type: "authorization_code",
      Code: code,
      redirect_uri: env.PKOBP_API_REDIRECT_URL,
      client_id: env.PKOBP_API_CLIENT_ID,
    };

    const config = {
      headers: {
        "X-REQUEST-ID": id,
        "X-JWS-SIGNATURE": env.PKOBP_API_JWS,
      },
    };

    Logger.info(`PKOBP API access token request sent!`);
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .post(url, data, config)
      .then((response) => {
        const data = response.data;
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.get_account = (obj) => {
  return new Promise((resolve, reject) => {
    let url = env.PKOBP_API_GET_ACCOUNT_URL;
    const id = createId();
    const data = {
      requestHeader: {
        requestId: id,
        tppId: env.PKOBP_API_TPP_ID,
        clientId: env.PKOBP_API_CLIENT_ID,
        token: obj.accessToken,
      },
      accountNumber: obj.accountNumber,
    };

    const config = {
      headers: {
        Authorization: `Bearer ${obj.accessToken}`,
        "X-JWS-SIGNATURE": env.PKOBP_API_JWS,
      },
    };

    Logger.info(`PKOBP API get account information request sent!`);
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .post(url, data, config)
      .then((response) => {
        const data = response.data;
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.get_transactions_done = (obj) => {
  return new Promise((resolve, reject) => {
    let url = env.PKOBP_API_GET_TRANSACTIONS_DONE_URL;
    const id = createId();
    const data = {
      requestHeader: {
        requestId: id,
        tppId: env.PKOBP_API_TPP_ID,
        clientId: env.PKOBP_API_CLIENT_ID,
        token: obj.accessToken,
      },
      accountNumber: obj.accountNumber,
    };

    const config = {
      headers: {
        Authorization: `Bearer ${obj.accessToken}`,
        "X-JWS-SIGNATURE": env.PKOBP_API_JWS,
      },
    };

    Logger.info(`PKOBP API get transactions done request sent!`);
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .post(url, data, config)
      .then((response) => {
        const data = response.data;
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

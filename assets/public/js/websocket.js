// https://cyber.test
// do wywalenia
// document.cookie =
//   "tokens=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiQmVhcmVyIiwiYWNjZXNzX3Rva2VuIjoiSGhnUTQtd1B6OE9RNDhYRlFxY2NLV1gwam9faWJqdjFVQU93dXhTX3JzVXF5NGVuWS1XbUIwVHMzbFMya3d1VFBhZVpxeGk4NVhjd1NIR2dyT2gxTXVEWDZ4VERhOEJ3VDRyUyIsImV4cGlyZXNfaW4iOjM1OTksInNjb3BlIjoicHJvZmlsZSIsInJlZnJlc2hfdG9rZW4iOiJIY1FTQklMRG4wOVJjYkMxNEx5YXRtTlQ5aU5BRlFGXzJEVTZSSThFekxqQU83MXYta2Jwb3ZycUVJdGJfbkhJNzIzc2U2MnFFYTd6OWxDYnp3c1FxVkcxNVBfYWJ0dDlhT25TIiwiaWRfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuZXlKcGMzTWlPaUpvZEhSd09pOHZNVEF1TWpBMExqRXdNQzR5TURVaUxDSjFjMlZ5WDJsa0lqb2lNemhpTVRSbU5qRmhZakV5TURneE1URmpaV0ptTTJaa1lXVXdPR1ppWVRZMk9XTm1OR1F6WVRNeFpqSmhZMkUwTVRObFlqUXpOamxrWm1RMU9XWmxNbUkwTkRkbU5qQTFPV1ptTkdKbE9HTmtNekV4WkRZeVpHRTVOR1U0WldSaElpd2lkWE5sY201aGJXVWlPaUp3YzNwaGJDSXNJbWRwZG1WdVgyNWhiV1VpT2lKUVlYZGx4WUlpTENKbVlXMXBiSGxmYm1GdFpTSTZJbE42WVd3aUxDSndhRzl1WlY5dWRXMWlaWEp6SWpwYklpczBPRFV6TVRZek5USTBNaUpkTENKbGJXRnBiSE1pT2x0ZExDSmhkV1FpT2lKamVXSmxjbDkwWlhOMElpd2laWGh3SWpveE5qTXhNRGswTnpnd0xDSnBZWFFpT2pFMk16RXdPVFF4T0RBeU1UQXNJbXAwYVNJNklqRm1OVEEyTldJekxUSmlORE10TkRNNE5pMWlOV1JoTFRnd05HSXhOV1l4WVRBelppSjkuLVdpd0pPUWh2VEljMDdZV0dXN0ZwSks2WjhGdTc2bXk5QkNMTzBCR3NrMCIsImlhdCI6MTYzMTA5NDE4MH0.shg7EtE4a-dDN9S7-5jnr_GKJgI_MMwvlttplnM-ero;path=/";
// document.cookie =
//   "id=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIzOGIxNGY2MWFiMTIwODExMWNlYmYzZmRhZTA4ZmJhNjY5Y2Y0ZDNhMzFmMmFjYTQxM2ViNDM2OWRmZDU5ZmUyYjQ0N2Y2MDU5ZmY0YmU4Y2QzMTFkNjJkYTk0ZThlZGEifQ.d5dr0dQq_4b9JWk3arZ4ynjos-5doxWACm1B7tEObx0;path=/";

// function heartbeat() {
//   clearTimeout(this.pingTimeout);
//
//   // Use `WebSocket#terminate()`, which immediately destroys the connection,
//   // instead of `WebSocket#close()`, which waits for the close timer.
//   // Delay should be equal to the interval at which your server
//   // sends out pings plus a conservative assumption of the latency.
//   this.pingTimeout = setTimeout(() => {
//     this.terminate();
//   }, 5000 + 1000);
// }
// import WebSocket from "ws";
let WS_ENPOINT;

(async () => {
  WS_ENPOINT = await GET("/wsEndpoint");
  console.log({ WS_ENPOINT });
})();

const socketConnect = () => {
  const socket = new WebSocket(WS_ENPOINT);

  // socket.addEventListener("ping", heartbeat);

  // Connection opened
  socket.onopen = (event) => {
    try {
      // heartbeat();
      // console.log({ event });
      // setInterval(() => {
      //   socket.send(JSON.stringify({ event: "ping" }));
      // }, 5000);
      // const token = readCookie("id")[1];
      // const userId = parseJwt(token).userId;
      // const userId = parseJwt(id_token).user_id;
      // console.log({ userId });
      socket.send(JSON.stringify({ event: "connect", data: {} }));
    } catch (err) {
      console.log(err);
    }
  };

  socket.onerror = (error) => {
    console.log({ error });
    alert(`[error] ${error}`);
  };

  return socket;
};

const getCheckedDataLabels_Array = () => {
  const arr = [];
  const checkboxes = $(".clipboard__list-item .form-check-input");
  for (const checkbox of checkboxes) {
    if ($(checkbox).is(":checked")) {
      arr.push(
        $(checkbox)
          .parent()
          .find(".clipboard__list-item__secondary-text")
          .data("label")
      );
      // object[$(checkbox).parent().find('.clipboard__list-item__secondary-text').data('label')] = $(checkbox).parent().find('.clipboard__list-item__secondary-text').data('label');
    }
  }
  return arr;
};

$(document).ready(() => {
  // check user data collapsible elements on page
  const userDataRows = $('.page .page-content [data-bs-toggle="collapse"]');
  let currentElement;
  for (const dataRow of userDataRows) {
    $(dataRow).on("click", (e) => {
      // const collapsibleElements = $(".page .page-content .user__data-collapse");
      const collapsibleElements = $(".page .page-content .collapse");
      for (const element of collapsibleElements) {
        $(element).parent().parent().removeClass("main-background");
        $(element).collapse("hide");
      }
      currentElement != e.currentTarget &&
        $(e.currentTarget).parent().addClass("main-background");
      // $(e.currentTarget).find(".user__data-collapse").collapse("show");
      $(e.currentTarget).find(".collapse").collapse("show");
      if (currentElement != e.currentTarget) {
        currentElement = e.currentTarget;
      } else {
        currentElement = "";
        $(e.currentTarget).parent().removeClass("main-background");
      }
    });
  }

  const jobEducationSelects = $(".profile_extended-select");
  $(jobEducationSelects).change((e) => {
    const selected = $(e.currentTarget)
      .find("option")
      .filter(":selected")
      .val();
    // console.log({
    //   selected,
    // });

    const label = $(e.currentTarget)
      .parent()
      .parent()
      .parent()
      .parent()
      .find(".user__data-label")
      .data("label");
    const value = $(e.currentTarget).find("option").filter(":selected").val();

    // console.log({ label }, { value });

    const labelText = $(e.currentTarget)
      .parent()
      .parent()
      .parent()
      .parent()
      .find(".user__data-label")
      .html();
    const valueText = $(e.currentTarget)
      .find("option")
      .filter(":selected")
      .html();

    if (selected && selected != "" && selected != undefined) {
      $("body").append(`
  <div class="modal fade" id="selectModalSave" tabindex="-1" role="dialog" aria-labelledby="selectModalSaveTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="selectModalSaveTitle">zapisywanie danych</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        czy chcesz zapisać <b>${labelText} ${valueText} <b>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">odrzuć</button>
        <button id="selectModalSaveButton" type="button" class="btn btn-primary">zapisz</button>
      </div>
    </div>
  </div>
  </div>`);

      $("#selectModalSaveButton").on("click", async (e) => {
        $("#selectModalSave").modal("hide");
        const data = {};
        data[label] = value;
        // console.log({ data });
        try {
          const loadingContent = await POST("/loading", {});
          $(".page .page-section").html(loadingContent);
          const res = await POST("/updateManually", data);
          // console.log({ res });
          // const resJSON = JSON.parse(res);
          setTimeout(async () => {
            const pageContent = await POST(`/identities/profile_extended`, {});
            $(".page .page-section").html(pageContent);
            showToast(
              "_add-success",
              '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
              "tożsamość rozszerzona",
              `dane zostały zaktualizowane`
            );
          }, 5000);
        } catch (err) {
          showToast(
            "_danger-no-data",
            '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
            "profil rozszerzony",
            `${labelText} ${valueText} nie został/o zaktualizowana/e`
          );
        }
      });

      $("#selectModalSave").modal("show");
      $("#selectModalSave").on("hidden.bs.modal", async (e) => {
        $("#selectModalSave").remove();
        // const loadingContent = await POST("/loading", {});
        // $(".page .page-section").html(loadingContent);
        // const pageContent = await POST(`/identities/profile_extended`, {});
        // $(".page .page-section").html(pageContent);
        // showToast(
        //   "_danger-no-data",
        //   '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        //   "profil rozszerzony",
        //   `${labelText} ${valueText} nie został/o zaktualizowana/e`
        // );
      });
    }
  });
});

var deleteJsonBtn = $(".delete__json-data");

$(deleteJsonBtn).on("click", async (e) => {
  const lang = $("nav.navbar .language__active").data("active-language");
  console.log(`you want to delete ${e}`);
  console.log({
    e,
  });
  const path = $(e.currentTarget).data("path");
  console.log({
    path,
  });
  try {
    $(".page .page-section").html(`<div class="container-fluid h-100">
  <div class="d-flex justify-content-center align-items-center" style="height: 100%">
    <div class="spinner-border" style="color: #003574; width: 3rem; height: 3rem;" role="status">
    </div>
    <span class="text-center ms-3">${language[lang].delete_json_file}</span>
  </div>`);
    const response = await POST("/api/gcs/deleteFile", {
      path: path,
    });
    const json = JSON.parse(response);
    console.log({ json });
    $("nav.sidebar .list-item.active a").click();
  } catch (err) {
    console.log({
      err,
    });
  }
});

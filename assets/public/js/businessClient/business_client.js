var sendBCData = async () => {
  // console.log("accept button klikniety");
  let sessionId, redirectUri;
  let clientHash = readCookie("client");
  // console.log({ clientHash });
  if (clientHash && clientHash.length > 0) {
    const clientData = JSON.parse(
      decodeURIComponent(b64DecodeUnicode(decodeURIComponent(clientHash[1])))
    );
    // console.log({ clientData });
    sessionId = clientData.sessionId;
    redirectUri = clientData.redirectUri;
  }

  // console.log({ sessionId }, { redirectUri });
  const decision = await showModal("czy chcesz przekazać dane?", [
    "nie",
    "tak",
  ]);
  // console.log({ decision });
  if ($(decision).data("modal-button") === "yes") {
    window.location.href = `${redirectUri}?sessionId=${sessionId}&status=success`;
  }
};

var cancelBCData = async () => {
  const decision = await showModal(
    "czy na pewno chcesz anulować uzupełnianie danych?",
    ["nie", "tak"]
  );
  // console.log({ decision });
  if ($(decision).data("modal-button") === "yes") {
    window.location.href = `${redirectUri}?sessionId=${sessionId}&status=failed`;
  }
};
(() => {
  const navMenu = $("ul.nav-menu");
  $(document).ready(() => {
    window.location.hash = "/businessClient";
    $(navMenu).css("transition", "all .3s");
    $(navMenu).css("margin-right", "150px");
    showClipboardDataManager();
  });

  const checkboxes = $(".clipboard__list-item .form-check-input");
  const clipboardManagerButtons = $(
    ".clipboard__manager .clipboard__manager-button"
  );
  var showClipboardDataManager = () => {
    // console.log('CALL SHOW CLIPBOARD DATA MANAGER');
    // ustawiam opcje jak aktywne --> do zrobienia
    // =======================================
    // const checkboxes = $('.clipboard__list-item .form-check-input');
    const manager = $(".clipboard__manager");
    // const buttons = $('.clipboard__manager .clipboard__manager-button');
    const input = $(".clipboard__manager .clipboard__manager-edit-input");
    for (const checkbox of checkboxes) {
      if ($(checkbox).is(":checked")) {
        if (!$(manager).hasClass("manager__active")) {
          // zmiana background
          $(manager).addClass("manager__active");
          // zmiana ikon
          $(input).css("color", "black");
          $(input).prop("disabled", false);
          $(input).hover(
            () => {
              $(input).css({
                opacity: ".5",
                cursor: "pointer",
              });
            },
            () => {
              $(input).css({
                opacity: "1",
                cursor: "default",
              });
            }
          );
          for (const button of clipboardManagerButtons) {
            $(button).css("pointer-events", "auto");
            $(button).css("color", "black");
            $(button).hover(
              () => {
                $(button).css({
                  opacity: ".5",
                  cursor: "pointer",
                });
              },
              () => {
                $(button).css({
                  opacity: "1",
                  cursor: "default",
                });
              }
            );
          }
        }
        return;
      }
    }
    $(manager).removeClass("manager__active");
    $(input).css("color", "#C8C8C8");
    $(input).prop("disabled", true);
    for (const button of clipboardManagerButtons) {
      $(button).css("pointer-events", "none");
      $(button).css("color", "#C8C8C8");
      $(button).off();
    }
  };
})();

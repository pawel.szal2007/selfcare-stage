$(document).ready(() => {
  const checkboxes = $(".flexible__permission-section input[type='checkbox']");
  console.log({ checkboxes });
  for (const checkbox of checkboxes) {
    // state change
    $(checkbox).change(async (e) => {
      const status = !$(checkbox).is(":checked");
      console.log({ status });

      let text = "";
      if (status) {
        text = "deaktywować";
      } else text = "aktywować";

      var returnVal = confirm(
        `czy chcesz ${text} zgodę dla zakresu: ${$(checkbox).parent().text()}?`
      );
      if (!returnVal) {
        $(checkbox).prop("checked", status);
      } else {
        // zgodził się
        // $(checkbox).prop("checked", !status);

        const viewerId = $(checkbox)
          .parent()
          .parent()
          .parent()
          .parent()
          .parent()
          .parent()
          .data("viewer");
        const permission_id = $(checkbox).data("perm-id");
        const scope = $(checkbox).data("scope");
        // zmienić status na blockchainie
        await permissionUpdate({
          permission_id: permission_id,
          viewerId: viewerId,
          scope: scope,
          status: !status,
          type: "Include",
        });
      }
    });
  }
});

var permanentPermissionUpdate = async (e) => {
  const status = $(e).data("status");
  const permission_id = $(e).data("perm-id");
  console.log({ status }, { permission_id });
  await permissionUpdate({
    permission_id: permission_id,
    viewerId: "cyber",
    scope: "permanent",
    status: status,
    type: "Public",
  });
};

var permissionUpdate = (obj) => {
  return new Promise(async (resolve, reject) => {
    try {
      const loadingContent = await POST("/loading", {});
      $(".page .page-section").html(loadingContent);
      const response = await POST("/permissions/update", obj);
      const jsonRes = JSON.parse(response);
      console.log({ jsonRes });
      if (jsonRes.status === "Success") {
        // przeladuj stronke ze zgodami zeby odswiezyc ją
        showToast(
          "_add-success",
          '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
          "zgody",
          `zgoda została zaktualizowana`
        );
      } else {
        // pokaz błąd w tosterze
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "zgody",
          "zgoda nie została zaktualizowana"
        );
      }
    } catch (err) {
      console.log({ err });
      // pokaz błąd w tosterze
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "zgody",
        "zgoda nie została zaktualizowana"
      );
    }

    const pageContent = await POST(`/permissions?e_show=${obj.viewerId}`, {});
    $(".page .page-section").html(pageContent);
  });
};

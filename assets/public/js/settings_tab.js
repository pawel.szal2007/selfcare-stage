var tabEl = $('button[data-bs-toggle="tab"]');
// sprawdzanie zakładki tab czy jestesmy na biometrii i wywalanie przycisków do akceptacji i zamkniecia
$(tabEl).on("show.bs.tab", (event) => {
  const tabFooter = $("#staticCredentialsBackdrop .modal-footer");
  console.log(event.target);
  if ($(event.target).data("bs-target") === "#nav-biometry") {
    console.log("jest biometria trzeba usunac przycisk");
    $(tabFooter).css("display", "none");
  } else {
    $(tabFooter).css("display", "flex");
  }
});

// dodaj biometrie do sekcji tab po pojawieniu sie modala
var credentailTabModal = $("#staticCredentialsBackdrop");
$(credentailTabModal).on("show.bs.modal", async (e) => {
  const credential = $(e.relatedTarget).data("credential");
  const tabTitle = $(".modal-content #staticBackdropLabel");
  const alert = $(tabTitle).parent().parent().find(".alert");
  if (alert.length > 0) {
    $(alert).remove();
  }
  $("#staticCredentialsBackdrop .modal-body").css("display", "block");
  $("#staticCredentialsBackdrop .modal-footer").css("display", "flex");

  if (credential === "all") {
    // sprawdzenie czy dotyczy pytań pomocniczych i czy jest na pewno więcej niż 3
    const active = $(e.relatedTarget).data("active");
    console.log(`ustawiam data del attribute dla buttona na ${active}`);
    console.log(
      `ustawiam w przycisku pytanie do usuniecia pod question ${$(
        e.relatedTarget
      )
        .parent()
        .parent()
        .parent()
        .parent()
        .find(".question__card-content")
        .data("name")}`
    );
    $(".modal .modal-footer .settings__save").data(
      "question",
      $(e.relatedTarget)
        .parent()
        .parent()
        .parent()
        .parent()
        .find(".question__card-content")
        .data("name")
    );

    let questions = [];
    try {
      const res = await GET("/settings/securityQuestions");
      questions = JSON.parse(res);
    } catch (err) {
      console.log(err);
    }
    console.log({ questions });
    if (active === "nav-questions-tab") {
      if (questions.length < 4) {
        $(tabTitle).parent()
          .after(`<div class="mb-0 mt-3 alert alert-danger" role="alert">
      wymagana liczba pytań pomocniczych: 3, dodaj nowe pytanie przed usunięciem kolejnego
    </div>`);
        $("#staticCredentialsBackdrop .modal-body").css("display", "none");
        $("#staticCredentialsBackdrop .modal-footer").css("display", "none");
        return;
      }
    }

    if (questions.length > 0) {
      // pytania pomocnicze...
      $(".modal #nav-tabContent #nav-questions")
        .append(`<div class="question__container">
          <div class="settings__dropdown-group question__div">
            <label class="settings__dropdown-label question-text" for="questions-dropdown">pytanie:</label>
            <select class="settings__dropdown-select question-select" name="questions" id="questions-dropdown">
            </select>
          </div>
          <div class="settings__dropdown-group answer__block">
            <div class="d-block answer__div">
              <span class="settings__dropdown-label answer-text">odpowiedź: </span>
              <input class="settings__dropdown-input answer-input w-50" type="text" name="" value="">
            </div>
          </div>
        </div>`);
      for (let question of questions) {
        if (question.category != "custom") {
          $(".modal #nav-tabContent #nav-questions select").append(
            `<option class="select-option" data-category="${question.category}">${question.question}</option>`
          );
        }
      }
    }
    // to oznacza usuwanie jakeigos skłądnika uwierzytelniającego
    $(tabTitle).data("task", "remove");
    $(tabTitle).html("weryfikacja składnikiem uwierzytelniającym");
    $(tabTitle).parent()
      .after(`<div class="mb-0 mt-3 alert alert-warning" role="alert">
  przed usunięciem składnika, musisz się uwierzytelnić dowolną metodą
</div>`);
    // z pinu musimy wywalić nowy pin/powtorz nowy pin i nowe hasło/powtóz nowe hasło
    $(".modal .tab-pane .settings__dropdown-input.pin__new-input")
      .parent()
      .hide();
    $(".modal .tab-pane .settings__dropdown-input.pin__new-repeat-input")
      .parent()
      .hide();

    $(".modal .tab-pane .settings__dropdown-input.password__new-input")
      .parent()
      .hide();
    $(".modal .tab-pane .settings__dropdown-input.password__new-repeat-input")
      .parent()
      .hide();
    // przycisk do zapisywania/usuwania
    $(".modal .modal-footer .settings__save").val("usuń");
    // pokaz wszystkie zakłdki i mozliwosci
    // const active = $(e.relatedTarget).data("active");
    // console.log(`ustawiam data del attribute dla buttona na ${active}`);
    $(".modal .modal-footer .settings__save").data("del", active);
    for (let btn of $(".modal .nav-tabs button")) {
      $(btn).removeClass("d-none");
    }
    console.log($(".modal .modal-footer .settings__save").data("del"));

    $(".modal .nav-tabs").find(`#${active}`).click();
  } else {
    // pokaz tylko ten credential o zadanym id
    $(tabTitle).data("task", "define");
    $(tabTitle).html("definiowanie składnika uwierzytelniającego");

    $(".modal .tab-pane .settings__dropdown-input.pin__new-input")
      .parent()
      .show();
    $(".modal .tab-pane .settings__dropdown-input.pin__new-repeat-input")
      .parent()
      .show();

    $(".modal .tab-pane .settings__dropdown-input.password__new-input")
      .parent()
      .show();
    $(".modal .tab-pane .settings__dropdown-input.password__new-repeat-input")
      .parent()
      .show();

    // ustawienie tabsa z pytaniem pomocniczym
    $(".modal #nav-tabContent #nav-questions")
      .html(`<div class="settings__dropdown-group answer__block mt-3"><div class="d-block mb-4 custom__question-div"><span class="settings__dropdown-label custom_question-text">własne pytanie: </span>
      <input class="settings__dropdown-input custom_question-input w-50" type="text" name="" value=""></div>
                        <div class="d-block answer__div">
                          <span class="settings__dropdown-label answer-text">odpowiedź: </span>
                          <input class="settings__dropdown-input answer-input w-50" type="text" name="" value="">
                        </div>
                      </div>`);
    // wpisac pytanie które jest do zdefiniowania
    if (credential === "nav-questions-tab") {
      $(".modal .modal-footer .settings__save").data(
        "question",
        $(e.relatedTarget)
          .parent()
          .parent()
          .parent()
          .parent()
          .find(".question__card-content")
          .data("name")
      );
    }

    // przycisk do zapisywania/usuwania
    $(".modal .modal-footer .settings__save").val("zapisz");

    $(".modal .nav-tabs").find(`#${credential}`).removeClass("d-none");
    $(".modal .nav-tabs").find(`#${credential}`).click();
  }

  const biometryContent = $(
    ".biometry__settings-content .settings__dropdown-group"
  );
  console.log({ biometryContent });

  // ustawienie tabsa z biometrią
  if ($(biometryContent).find("#instruction_biometry").data("enrolled")) {
    const clone_content = biometryContent.clone(true, true);
    $(clone_content)
      .find("#instruction_biometry")
      .html(`Wciśnij ikonę mikrofonu by rozpocząć nagrywanie`);
    // verification veriable from record script
    verification = true;
    console.log({ clone_content });
    $(biometryContent).parent().html("");
    $(".modal .tab-content #nav-biometry").append(clone_content);
  }
});

// wroc z biometria do sekcji ustawien po zamknieciu tego modala
$(credentailTabModal).on("hide.bs.modal", (e) => {
  const biometryContent = $(
    ".modal .tab-content #nav-biometry .settings__dropdown-group"
  );
  console.log({ biometryContent });
  const clone_content = $(biometryContent).clone(true, true);
  console.log({ clone_content });
  const enrolled = $(clone_content)
    .find("#instruction_biometry")
    .data("enrolled");
  // verification veriable from record script
  verification = false;
  if (enrolled) {
    $(clone_content)
      .find("#instruction_biometry")
      .html(
        `Wciśnij ikonę mikrofonu by rozpocząć nagrywanie i zaktualizować biometrię głosową`
      );
  } else
    $(clone_content)
      .find("#instruction_biometry")
      .html(
        `Wciśnij ikonę mikrofonu by rozpocząć nagrywanie i stworzyć nowy profil biometrii głosowej`
      );
  $(biometryContent).parent().html("");
  $(".biometry__settings-content .settings__dropdown-data").append(
    clone_content
  );

  // pytania pomocnicze wywal
  $(".modal #nav-tabContent #nav-questions").html("");
  // chowamy wszystkie linki w tabsach na display none
  // pokaz wszystkie zakłdki i mozliwosci
  for (let btn of $(".modal .nav-tabs button")) {
    $(btn).addClass("d-none");
  }

  // for (let tab of $(".modal #nav-tabContent .tab-pane")) {
  //   $(tab).removeClass("show");
  //   $(tab).removeClass("active");
  // }
});

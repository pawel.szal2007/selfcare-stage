// dodałem po 3 zer... usunac je
var warningTimeout = 180000;
var timoutNow = 60000;
var counter = timoutNow;
var warningTimerID, timeoutTimerID;

function startTimer() {
  // window.setTimeout returns an Id that can be used to start and stop a timer
  warningTimerID = window.setTimeout(warningInactive, warningTimeout);
}

function warningInactive() {
  window.clearTimeout(warningTimerID);

  timeoutTimerID = window.setTimeout(IdleTimeout, timoutNow);
  var logoutInterval = setInterval(() => {
    counter -= 1000;
    const timeLeft = counter / 1000;
    // console.log("Time left: " + timeLeft + "s");
    $("#myModal .session__timer").text(timeLeft + "s");
  }, 1000);

  var myModal = new bootstrap.Modal(document.getElementById("myModal"), {
    keyboard: false,
  });

  $("#myModal .session__timer").text(`${timoutNow / 1000}s`);
  myModal.show();

  // console.log({ myModal });
  // console.log($(myModal));
  $("#myModal .button__dismiss").unbind("click");
  $("#myModal .button__dismiss").on("click", (e) => {
    // console.log("button dismiss clicked, remove timer");
    window.clearTimeout(timeoutTimerID);
    clearInterval(logoutInterval);
    myModal.dispose();
    counter = timoutNow;
  });

  // $("#modalAutoLogout").modal("show");
}

function getTimeLeft(timeout) {
  console.log({ timeout });
  // console.log(timeout._idleStart);
  // console.log(timeout._idleTimeout);
  return (timeout._idleStart + timeout._idleTimeout - Date.now()) / 1000;
}

function resetTimer() {
  // window.clearTimeout(timeoutTimerID);
  window.clearTimeout(warningTimerID);
  startTimer();
}

// Logout the user.
function IdleTimeout() {
  // document.getElementById("logout-form").submit();
  logout();
}

function setupTimers() {
  document.addEventListener("mousemove", resetTimer, false);
  document.addEventListener("mousedown", resetTimer, false);
  document.addEventListener("keypress", resetTimer, false);
  document.addEventListener("touchmove", resetTimer, false);
  document.addEventListener("onscroll", resetTimer, false);
  startTimer();
}

// $(document).on("click", "#btnStayLoggedIn", function () {
//   resetTimer();
//   var myModal = new bootstrap.Modal(document.getElementById("myModal"), {
//     keyboard: false,
//   });
//   myModal.hide();
//   // $("#modalAutoLogout").modal("hide");
// });

var logout = () => {
  window.location.href = "/logout";
};

$(document).ready(function () {
  setupTimers();
});

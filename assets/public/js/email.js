var emailInputCollapse = () => {
  // console.log('collapsing...');
  const emailCollapse = $("#emailInput");
  $(emailCollapse).hasClass("show")
    ? $(emailCollapse).removeClass("show")
    : $(emailCollapse).addClass("show");
};

var emailsRefresh = async (e) => {
  try {
    const lang = $("nav.navbar .language__active").data("active-language");
    const response = await GET("/settings/email/refresh");
    const emails = JSON.parse(response);
    console.log({ emails });

    const emailsContent = $(".email__address-manager .credentials__content");

    if (emails && emails.length > 0) {
      // usunac widok z mailami i wklepac ten nowy widoczek
      $(emailsContent).html("");
      for (const email of emails) {
        const is_verified = email.is_verified;
        $(emailsContent)
          .append(`<div class="py-2 d-flex justify-content-between align-items-center">
                <span class="px-4 email__card-content" data-name="${
                  email.email_address
                }">
                  ${email.email_address}
                  <span class="${
                    is_verified
                      ? "email__card-content-verified"
                      : "email__card-content-not_verified"
                  }">${
          is_verified ? "zweryfikowany" : "niezweryfikowany"
        }</span>
                </span>
                <div class="btn-group dropstart email__card-content-menu">
                  <span id="dropdownMenuButton1" type="button" class="dropdown-toggle material-icons-outlined menu-icon" data-bs-toggle="dropdown" aria-expanded="false">more_vert</span>
                  <ul class="dropdown-menu">
                    <li>
                    ${
                      is_verified
                        ? '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteEmail(this)">usuń</a>'
                        : '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteEmail(this)">usuń</a> <a class="dropdown-item" href="javascript:void(0)" onclick="resendActivationLink(this)">wyślij ponownie link aktywacyjny</a>'
                    }
                    </li>
                  </ul>
                </div>
              </div>`);
      }
    } else {
      $(emailsContent).html(
        `<span class="px-4 email__card-content">${language[lang].email.empty}</span>`
      );
    }
  } catch (err) {
    console.log(err);
  }
};

var addNewEmailAddress = () => {
  // const emailAddContent = $('.email__data-collapse .email__data-add-content');
  // const emailConfirmContent = $('.email__data-confirm-content');
  const email = $(".email__data-add-content .email__input")[0];
  const emailRepeated = $(".email__data-add-content .email__input")[1];
  // check if valid
  validateEmail(email);
  validateEmail(emailRepeated);
  // check if inputs values are correct and the same
  if ($(email).val() != $(emailRepeated).val()) {
    showToast(
      "_warning-repeated",
      '<svg class="bi flex-shrink-0 me-2 blue" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>',
      "aktywacja adresu email",
      "wprowadzone adresy email są różne! wprowadź taki sam adres email"
    );
    return;
  } else if (
    $(email).val() === undefined ||
    $(email).val() === "" ||
    $(email).val() === " " ||
    $(emailRepeated).val() === undefined ||
    $(emailRepeated).val() === "" ||
    $(emailRepeated).val() === " "
  ) {
    showToast(
      "_warning-repeated",
      '<svg class="bi flex-shrink-0 me-2 blue" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>',
      "aktywacja adresu email",
      "pola nie mogą pozostać puste"
    );
    return;
  } else {
    console.log($(email).val());
    // zwijamy kontent
    $("#emailInput").prev().find("input").click();
    const _csrf = $("input:hidden[name='_csrf']").val();
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": _csrf,
      },
      url: "/settings/email/add",
      type: "POST",
      data: JSON.stringify({
        email: $(email).val(),
      }),
      contentType: "application/json",
      dataType: "json",
      error: (error) => {
        console.log({ error });
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "aktywacja adresu email",
          `adres email został już dodany lub wystąpił błąd przy wysyłaniu maila aktywacyjnego`
        );
      },
      success: (data) => {
        console.log({ data });
        showToast(
          "_add-success",
          '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
          "aktywacja adresu email",
          `link aktywacyjny ważny przez 24h został wysłany na podany adres email!`
        );
        // odswiez maile
        $(".email__address-manager .refresh").click();
      },
    });

    // wyczyść pola
    for (const e of $(".email__data-add-content .email__input")) {
      $(e).val("");
    }
  }
};

var validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

var deleteEmail = async (e) => {
  const email = $(e)
    .parent()
    .parent()
    .parent()
    .parent()
    .find(".email__card-content")
    .data("name");
  // console.log({ email });
  try {
    const response = await POST("/settings/email/delete", { email: email });
    const jsonRes = JSON.parse(response);
    // console.log({ jsonRes });
    if (jsonRes.status === 200) {
      const refreshBtn = $(".email__address-manager .refresh").click();
      showToast(
        "_add-success",
        '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
        "ustawienia",
        `adres email został prawidłowo usunięty`
      );
    }
  } catch (err) {
    showToast(
      "_danger-no-data",
      '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
      "ustawienia",
      "adres email nie został usunięty"
    );
  }
};

// var emailCodeConfirmation = () => {
//   const emailAddContent = $('.email__data-collapse .email__data-add-content');
//   const emailConfirmContent = $('.email__data-confirm-content');
//   const codeInput = $('.email__data-confirm-content .email__input-confirm');
//   const email = $('.email__data-add-content .email__input')[0];
//   const code = $(codeInput).val();
//   // console.log(code);
//   if(code) {
//     $.ajax({
//       url: '/emailCodeVerification',
//       type: 'POST',
//       data: JSON.stringify({
//         email: $(email).val(),
//         code: code
//       }),
//       contentType: 'application/json',
//       dataType: 'html',
//       error: (error) => {
//         // console.log(error);
//         showToast('_danger-no-data', '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>', 'aktywacja adresu email', `niepowodzenie, podany kod jest nieprawidłowy`);
//       },
//       success: (data) => {
//         const response = JSON.parse(data);
//         // console.log({response});
//         $(codeInput).val('');
//         if(response.error) {
//           showToast('_danger-no-data', '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>', 'aktywacja adresu email', `podany kod jest nieprawidłowy, adres email nie został dodany.`);
//           $(emailAddContent).css('opacity', '1');
//           $(emailConfirmContent).css('visibility', 'hidden');
//           return;
//         } else {
//           $('#emailInput').toggle('show');
//           $(emailAddContent).css('opacity', '1');
//           $(emailConfirmContent).css('visibility', 'hidden');
//           showToast('_add-success', '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>', 'aktywacja adresu email', `adres email został dodany z powodzeniem!`);
//           $('.email__card-content').html($(email).val());
//         }
//       }
//     });
//   } else {
//     showToast('_warning-repeated', '<svg class="bi flex-shrink-0 me-2 blue" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>', 'aktywacja adresu email', 'pole z kodem aktywacyjnym nie moze pozostac puste!');
//   }
// }

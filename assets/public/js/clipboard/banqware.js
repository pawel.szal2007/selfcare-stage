(() => {
  const banqwareButton = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='banqware']"
  );

  $(banqwareButton).unbind("click");
  $(banqwareButton).on("click", () => {
    $(".page .page-section").html(`
      <span class="button_back m-4 material-icons-outlined" onclick="postCookiesData()">arrow_back</span>
      <nav aria-label="breadcrumb">
      <ol class="ms-4 breadcrumb">
        <li class="breadcrumb-item" aria-current="page">Pobierz dane z rachunku bankowego</li>
      </ol>
    </nav>

    <div class="ms-2 row justify-content-center justify-content-sm-start">
      <div class="margin-bottom-12 col-10 col-sm-6 col-md-4 col-lg-3 col-xlg-2">
        <div class="bcap-card mdc-card">
          <img src="/img/banks/pko_bp.svg.png" data-bank-id="75" class="banqup__image-bank-logo img-fluid rounded mx-auto" alt="...">
        </div>
      </div>
      <div class="margin-bottom-12 col-10 col-sm-6 col-md-4 col-lg-3 col-xlg-2">
        <div class="bcap-card mdc-card">
          <img src="/img/banks/bnp_paribas.webp" data-bank-id="80" class="banqup__image-bank-logo img-fluid rounded mx-auto" alt="...">
        </div>
      </div>
      <div class="margin-bottom-12 col-10 col-sm-6 col-md-4 col-lg-3 col-xlg-2">
        <div class="bcap-card mdc-card">
          <img src="/img/banks/mbank.jpg" data-bank-id="86" class="banqup__image-bank-logo img-fluid rounded mx-auto" alt="...">
        </div>
      </div>
    </div>
    <script src="/js/banqup_api.js" charset="utf-8"></script>`);
  });

  // $(".button_back").unbind("click");
  // $(".button_back").on("click", async () => {
  //   console.log("clicked back button...");
  //   await postCookiesData();
  // });
})();

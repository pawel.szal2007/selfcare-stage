(() => {
  "use strict";

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = $("#forwardDataModal .forward__data-form");

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach((form) => {
    form.addEventListener(
      "submit",
      (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (form.checkValidity()) {
          console.log("dane są poprawne ;)");
          sendMessage();
        }
        form.classList.add("was-validated");
      },
      false
    );
  });

  const sendMessage = async () => {
    const senderEmail = $("#forwardDataModal #sender__email").val();
    const recipientEmail = $("#forwardDataModal #recipient__email").val();
    const password = $("#forwardDataModal #file__password").val();

    const csvArray = [];
    const obj = {};
    const checkboxes = $(".clipboard__list-item .form-check-input");
    for (const checkbox of checkboxes) {
      if ($(checkbox).is(":checked")) {
        obj[
          $(checkbox)
            .parent()
            .find(".clipboard__list-item__secondary-text")
            .text()
        ] = $(checkbox)
          .parent()
          .find(".clipboard__list-item__primary-text")
          .text();
      }
    }
    csvArray.push(obj);
    console.log({ senderEmail }, { recipientEmail }, { password });
    console.log({ csvArray });

    const response = await POST("/clipboard/send", {
      sender: senderEmail,
      recipient: recipientEmail,
      csv: csvArray,
      password: password,
    });

    const closeButton = $("#forwardDataModal .btn-close");
    $(closeButton).click();

    if (JSON.parse(response).status === 200) {
      showToast(
        "_add-success",
        '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
        "przekazywanie danych",
        `dane zostały wysłane na podany adres email`
      );
    } else {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "przekazywanie danych",
        "dane nie zostały wysłane, wystąpił błąd po stronie serwera"
      );
    }
  };
})();

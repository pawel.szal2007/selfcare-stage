(() => {
  const manualMenuButton = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='manual']"
  );
  const reject = $(
    ".clipboard__manager .clipboard__manager-button.clipboard__manager-manual-reject"
  );
  const save = $(
    ".clipboard__manager .clipboard__manager-button.clipboard__manager-manual-save"
  );
  const selectAll = $(".clipboard__page .select__all");

  $(manualMenuButton).unbind("click");
  $(manualMenuButton).on("click", () => {
    updateManually();
  });

  var updateManually = async () => {
    // const checkboxes = $('.clipboard__list-item .form-check-input')
    for (const checkbox of checkboxes) {
      const value = $(checkbox).next().children()[0];
      const itemRow = $(checkbox).parent().parent();
      if ($(checkbox).is(":checked")) {
        const valueText = $(value).html();
        const secondaryText = $(itemRow).find(
          ".clipboard__list-item__secondary-text"
        );
        const dataLabel = $(secondaryText).data("label");
        console.log({ dataLabel });

        // jesli jest strefa czasowa to podpowiadamy wartosc
        if (dataLabel === "zoneinfo") {
          $(value).is("span") &&
            // onblur='spanReset(this)'
            $(value).replaceWith(
              `<input class="clipboard__temp-input-popover clipboard__list-item__primary-text" value='${valueText}' data-bs-container=".clipboard__card-list" data-bs-toggle="popover" data-bs-placement="right" data-bs-content="sprawdź czy sugerowana strefa zamieszkania jest poprawna"/>`
            );

          const res = await GET("/zoneinfo");
          console.log({ res }, $(secondaryText).prev());
          $(secondaryText).prev().val(res);

          // show popover
          var popover = new bootstrap.Popover(
            document.querySelector(".clipboard__temp-input-popover"),
            {
              container: ".clipboard__list-item",
              trigger: "manual",
            }
          );
          popover.show();
          setTimeout(() => {
            popover.hide();
          }, 4000);
        } else if (dataLabel === "picture") {
          $(value).is("span") &&
            // onblur='spanReset(this)'
            $(value).replaceWith(
              `<input class="clipboard__temp-input form-control form-control-sm" id="pictureInput" type="file"><img id="userTempImage" class="d-block" src="#" alt="" />`
            );
          $("#pictureInput").on("change", (e) => {
            console.log("something changed...");
            const readURL = (input) => {
              if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                  $("#userTempImage").attr("src", e.target.result);
                  // .width(144)
                  // .height(180);
                };

                reader.readAsDataURL(input.files[0]);
              }
            };
            readURL(e.currentTarget);
          });
        } else {
          $(value).is("span") &&
            // onblur='spanReset(this)'
            $(value).replaceWith(
              `<input class="clipboard__temp-input clipboard__list-item__primary-text" value='${valueText}'/>`
            );
        }

        $(checkbox).remove();
      } else $(itemRow).remove();
    }
    $(selectAll).remove();

    // check if category and loa needs to be displayed none beacuse there is no items
    const containers = $(".clipboard__page .clipboard__item-container");
    // console.log({ containers });
    for (const container of containers) {
      // console.log({ container });
      const items = $(container).find(".clipboard__list-item");
      // console.log(items.length);
      if (items.length === 0) {
        $(container).remove();
      }
    }

    // change clipboard manager menu
    const menuContent = $(".clipboard__manager .clipboard__manager-content");
    const menuContentAlt = $(
      ".clipboard__manager .clipboard__manager-content-alternative"
    );

    $(menuContent).css("opacity", "0");
    $(menuContentAlt).css("z-index", "0");
    $(menuContentAlt).css("opacity", "1");
  };

  // $(reject).unbind("click");
  // $(reject).on("click", async () => {
  //   // console.log("odrzucone dane");
  //   await postCookiesData();
  // });

  // $(save).unbind("click");
  // $(save).on("click", async () => {
  //   const fields = $(".clipboard__list-item__primary-text");
  //   const dataObj = {};
  //   for (const field of fields) {
  //     const label = $(field).next().data("label");
  //     const value = $(field).val();
  //     dataObj[label] = value;
  //   }
  //   dataObj.loa = "low";
  //   dataObj.source = "manually";
  //   // console.log({ dataObj });
  //   const response = await POST("/updateManually", dataObj);
  //   // console.log({ response });
  //   setTimeout(async () => {
  //     await update();
  //   }, 2000);
  // });
})();

var spanReset = (e) => {
  let txt = e.value;
  const label = $(e).next().data("label");
  $(e).is("input") &&
    $(e).replaceWith(
      `<span class="clipboard__list-item__primary-text">${txt}</span>`
    );
};

var manualModReject = async () => {
  await postCookiesData();
};

var manualModSave = async (e) => {
  // pokazac jakies łądowanie
  $(e).replaceWith(`<div class="small__loader"></div>`);
  const fields = $(".clipboard__list-item__primary-text");
  const dataObj = {};
  for (const field of fields) {
    const label = $(field).next().data("label");
    const value = $(field).val();
    dataObj[label] = value;
  }
  dataObj.loa = "low";
  dataObj.source = "manually";
  // console.log({ dataObj });

  // sprawdzic czy jest zdjęcie
  // console.log($("#pictureInput").files);
  if ($("#pictureInput").length > 0 && $("#pictureInput").prop("files")[0]) {
    const picture = $("#pictureInput").prop("files")[0];
    const fd = new FormData();
    fd.append("picture", picture);
    fd.append("data", JSON.stringify(dataObj));
    // fd.append('type', documentType)
    // fd.append('consentId', consentId)
    const _csrf = $("input:hidden[name='_csrf']").val();
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": _csrf,
      },
      url: `/updateManually`,
      type: "POST",
      data: fd,
      contentType: false,
      processData: false,
      error: (err) => {
        setTimeout(async () => {
          await update();
          showToast(
            "_danger-no-data",
            '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
            "aktualizacja danych",
            "zdjęcie jest zbyt duże lub wybrano niepoprawny format pliku (dozwolone png/jpg/jpeg)"
          );
        }, 3000);
      },
      success: async (data) => {
        // setTimeout(async () => {
        await update();
        // }, 3000);
      },
    });
  } else {
    const response = await POST("/updateManually", dataObj);
    // console.log({ response });
    // setTimeout(async () => {
    await update();
    // }, 3000);
  }
};

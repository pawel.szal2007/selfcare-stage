// const ocrModalButtons = $(".upload__modal .upload__modal-button");

var consentId;
var uploadFile = async () => {
  try {
    const active_lang = $("nav.navbar .language__active").data(
      "active-language"
    );
    const fileInput = document.getElementById("upload__document-input");
    const select = $(".upload__modal .upload__modal-content-title");
    // const rejectButton = $(
    //   ".upload__modal .upload__modal-content .upload__modal-button-reject"
    // );
    // const acceptButton = $(
    //   ".upload__modal .upload__modal-content .upload__modal-button-accept"
    // );
    // const ocrButtons = $('.upload__modal .upload__modal-button');
    const documentsDropdown = $(
      ".upload__modal .upload__modal-content #upload__documents-dropdown"
    );
    const documentType = $(documentsDropdown).val();
    // const ocrDataView = $(
    //   ".upload__modal .upload__modal-content .upload__modal-data"
    // );
    const file = fileInput.files[0];
    if (!file) {
      $(select).html("dodaj plik!");
      return;
    }
    showCustomModal("consent");
    // console.log(
    //   $.data(
    //     $(
    //       ".consent__modal .consent__modal-content .consent__modal-content-button.agree"
    //     ).get(0),
    //     "events"
    //   )
    // );
    if (
      $.data(
        $(
          ".consent__modal .consent__modal-content .consent__modal-content-button.agree"
        ).get(0),
        "events"
      ) &&
      $.data(
        $(
          ".consent__modal .consent__modal-content .consent__modal-content-button.agree"
        ).get(0),
        "events"
      ).click
    ) {
      // console.log("on click exists...");
      return;
    } else {
      $(
        ".consent__modal .consent__modal-content .consent__modal-content-button.agree"
      ).unbind("click");
      $(
        ".consent__modal .consent__modal-content .consent__modal-content-button.agree"
      ).on("click", async (e) => {
        try {
          // let socket = socketConnect();
          // socket.onclose = (event) => {
          //   if (event.wasClean) {
          //     console.log(
          //       `[close] Websocket connection closed cleanly, code=${event.code} reason=${event.reason}`
          //     );
          //   } else if (event.code === 1006) {
          //     alert(`websocket connection closed with event code 1006`);
          //     console.log(`websocket connection closed with event code 1006`);
          //     socket = socketConnect();
          //     getWSMessage(socket);
          //   }
          // };
          // const getWSMessage = (socket) => {
          //   socket.onmessage = async (event) => {
          //     const message = JSON.parse(event.data);
          //     if (message.event === "get_data") {
          //       if (message.checked) {
          //         // console.log({ message });
          //         const checkedData = getCheckedDataLabels_Array();
          //         // console.log({ checkedData });
          //         socket.send(
          //           JSON.stringify({
          //             event: "response_data",
          //             checked: true,
          //             data: checkedData,
          //           })
          //         );
          //         socket.close(1000);
          //       }
          //     }
          //     if (message.event && message.event === "accept") {
          //       console.log({ message });
          //     }
          //   };
          // };
          // getWSMessage(socket);
          // $(rejectButton).css("display", "none");
          // $(acceptButton).css("display", "none");
          // $(ocrDataView).html("");
          // $(select).html(
          //   `<div class="upload__spinner-border spinner-border text-secondary" role="status"><span class="sr-only">Loading...</span></div><span>${language[active_lang].custom_modal.upload.waiting}</span>`
          // );
          closeCustomModal("consent");
          // console.log({
          //   documentType,
          // });

          mainBackgroundView({ event: "show" });
          // pokaz loading na pobieranie zgody
          const text = loadingFullScreen({
            event: "show",
            text: "proszę czekać, zapisujemy Twoją zgodę...",
            background: "transparent",
          });

          loadingFullScreen({ event: "hide", object: "close_button" });

          try {
            consentId = await getConsent(documentType);
            // console.log({
            //   consentId,
            // });
          } catch (err) {
            $(text).html("niepowodzenie, zgoda nie została zapisana :(");
            loadingFullScreen({ event: "visible", object: "close_button" });
            mainBackgroundView({ event: "close" });
            // socket && socket.close(1000);
            throw err;
          }

          clearUploadModal();

          $(text).html(
            "proszę czekać, odczytujemy właśnie Twój dokument, może to zająć kilka minut"
          );
          // $(ocrDataView).data('consent', `${consentId}`)
          // $(".upload__modal .upload__modal-close").css(
          //   "pointer-events",
          //   "none"
          // );
          const checkedData = getCheckedDataLabels_Array();
          const fd = new FormData();
          fd.append("file", file);
          fd.append("checkedData", checkedData);
          // fd.append('type', documentType)
          // fd.append('consentId', consentId)
          const _csrf = $("input:hidden[name='_csrf']").val();
          $.ajax({
            headers: {
              "X-CSRF-TOKEN": _csrf,
            },
            url: `/ocr/${documentType}`,
            type: "POST",
            data: fd,
            contentType: false,
            processData: false,
            error: (err) => {
              // console.log(err);
              // clearUploadModal();
              $(text).html(
                "niestety coś poszło nie tak, nie udało nam się prawidłowo wydobyć Twoich danych, spróbuj ponownie później"
              );

              loadingFullScreen({ event: "visible", object: "close_button" });
              // mainBackgroundView({ event: "close" });
            },
            success: (data) => {
              console.log({
                data,
              });
              loadingFullScreen({ event: "close" });
              let source, loa;
              for (const key in data) {
                source = data[key].data.source;
                loa = data[key].data.loa;
                break;
              }
              const [acceptButton, rejectButton] = userDataView({
                event: "show",
                background: "transparent",
                source: source,
                loa: loa || "low",
                data: data || {},
              });

              $(acceptButton).unbind("click");
              $(acceptButton).on("click", async () => {
                // dataView({ event: "close" });
                // console.log("{user accepted data}");
                userDataView({ event: "close" });
                const text = loadingFullScreen({
                  event: "show",
                  background: "transparent",
                  text:
                    "jeszcze tylko chwila, trwa aktualizacja Twoich danych...",
                });
                loadingFullScreen({ event: "hide", object: "close_button" });
                const response = await POST("/publishDataToTopic", data);
                // console.log({ response });
                setTimeout(async () => {
                  await update();
                  mainBackgroundView({ event: "close" });
                  loadingFullScreen({ event: "close" });
                }, 5000);
              });

              $(rejectButton).unbind("click");
              $(rejectButton).on("click", () => {
                // dataView({ event: "close" });
                // console.log("{user rejected data}");
                mainBackgroundView({ event: "close" });
                userDataView({ event: "close" });
              });
              // $(".upload__modal .upload__modal-close").css(
              //   "pointer-events",
              //   "auto"
              // );
              // $(ocrDataView).append(
              //   `<span class="data-result-text">${language[active_lang].ocr.title}</span>`
              // );
              // const checkboxes = $('.clipboard__list-item .form-check-input');

              // const object = {};
              // for (const checkbox of checkboxes) {
              //   if ($(checkbox).is(":checked")) {
              //     object[
              //       $(checkbox)
              //         .parent()
              //         .find(".clipboard__list-item__secondary-text")
              //         .data("label")
              //     ] = $(checkbox)
              //       .parent()
              //       .find(".clipboard__list-item__secondary-text")
              //       .data("label");
              //   }
              // }
              //
              // for (const property in data) {
              //   if (object[property.toLowerCase()]) {
              //     $(ocrDataView)
              //       .append(`<div class="upload__modal-data-row row">
              //       <span class="data-label col-6" data-label='${property.toLowerCase()}'>${
              //       language[active_lang].ocr[documentType][
              //         property.toLowerCase()
              //       ]
              //     }:</span>
              //       <span class="data-value col-6" data-value='${
              //         data[property]
              //       }'>${data[property]}</span>
              //     </div>`);
              //   }
              // }
              //
              // $(select).html("wybierz plik: ");
              // for (const button of ocrModalButtons) {
              //   $(button).css("display", "inline-block");
              // }
            },
          });
        } catch (err) {
          // console.log({
          //   err,
          // });
          mainBackgroundView({ event: "close" });
          loadingFullScreen({ event: "close" });
          clearUploadModal();
        }
      });
    }
  } catch (err) {
    // console.log(err);
    clearUploadModal();
  }
};

$(".upload__modal .upload__modal-content .upload__button").unbind("click");
$(".upload__modal .upload__modal-content .upload__button").on("click", () => {
  uploadFile();
});

// var ocrButtonClick = async (e) => {
//   // // console.log('ocrButton clicked call correctly', {e});
//   const active_lang = $("nav.navbar .language__active").data("active-language");
//   if ($(e.currentTarget).data("label") === "reject") {
//     clearUploadModal();
//   } else if ($(e.currentTarget).data("label") === "accept") {
//     const dataLabelsToUpdate = $(".upload__modal-data-row .data-label");
//     const dataValuesToUpdate = $(".upload__modal-data-row .data-value");
//     const documentsDropdown = $(
//       ".upload__modal .upload__modal-content #upload__documents-dropdown"
//     );
//     $(
//       ".upload__modal .upload__modal-content .upload__modal-button-accept"
//     ).replaceWith(
//       '<div class="upload__spinner-border spinner-border text-secondary" role="status"><span class="sr-only">Loading...</span></div>'
//     );
//     // const consentId = $('.upload__modal .upload__modal-content .upload__modal-data').data('consent')
//
//     const documentType = $(documentsDropdown).val();
//     const jsObject = {
//       topicName: ocrConfiguration[documentType].topicName,
//       service: ocrConfiguration[documentType].service,
//       consentId: consentId,
//       data: {},
//     };
//     for (let i = 0; i < dataLabelsToUpdate.length; i++) {
//       const label = $(dataLabelsToUpdate[i]).data("label");
//       jsObject.data[label] = $(dataValuesToUpdate[i]).html();
//     }
//     // console.log({ jsObject });
//
//     try {
//       const response = await POST("/publishDataToTopic", jsObject);
//       // console.log({ response });
//       await update();
//       $(".upload__modal .upload__spinner-border.spinner-border").replaceWith(
//         '<span class="upload__modal-button upload__modal-button-accept" data-label="accept">akceptuj</span>'
//       );
//       clearUploadModal();
//     } catch (err) {
//       // console.log({ err });
//       $(".upload__modal .upload__spinner-border.spinner-border").replaceWith(
//         '<span class="upload__modal-button upload__modal-button-accept" data-label="accept">akceptuj</span>'
//       );
//       clearUploadModal();
//     }
//   }
// };

var clearUploadModal = () => {
  const select = $(".upload__modal .upload__modal-content-title");
  const uploadModal = $(".upload__modal");
  const dataView = $(
    ".upload__modal .upload__modal-content .upload__modal-data"
  );
  // const ocrButtons = $('.upload__modal .upload__modal-button');
  $(select).html("wybierz plik:");
  $(uploadModal).css("display", "none");
  $(dataView).html("");
  // for (const button of ocrModalButtons) {
  //   $(button).css("display", "none");
  // }
};

// for (const button of ocrModalButtons) {
//   $(button).unbind("click");
//   $(button).on("click", (e) => {
//     // // console.log('clicked');
//     ocrButtonClick(e);
//   });
// }

$(".upload__modal .upload__modal-close").unbind("click");
$(".upload__modal .upload__modal-close").on("click", (e) => {
  clearUploadModal();
});

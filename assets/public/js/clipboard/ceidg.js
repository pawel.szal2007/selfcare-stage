(() => {
  const ceidgButton = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='ceidg']"
  );
  const radiosButtons = $(".ceidg__modal .ceidg__modal-content .content-radio");
  const ceidgInput = $(".ceidg__modal .ceidg__modal-content .content-input");
  const acceptButton = $(".ceidg__modal .ceidg__modal-content .content-button");
  const closeButton = $(".ceidg__modal .ceidg__modal-content .content-close");

  $(ceidgButton).unbind("click");
  $(ceidgButton).on("click", async () => {
    try {
      showCustomModal("ceidg");
      // const text = loadingFullScreen({
      //   event: "show",
      //   background: "rgb(42,107,176, .9)",
      //   icon: '<span class="material-icons-outlined">storage</span>',
      //   text: "trwa pobieranie danych z rejestru podatników ceidg...",
      // });
      // // window.location.href = response;
      //
      // //do ceidg potrzebny jest albo NIP albo REGON albo numer konta bankowego
      //
      // const response = await GET(`/ceidg/data?params...to...be...done`);
      // // console.log({ response });
    } catch (err) {
      // console.log({ err });
    }
  });

  $(acceptButton).unbind("click");
  $(acceptButton).on("click", async () => {
    const lang = $("nav.navbar .language__active").data("active-language");
    let radioOption;
    for (const radio of radiosButtons) {
      if ($(radio).is(":checked")) {
        radioOption = $(radio).val();
      }
    }
    const inputValue = $(ceidgInput).val();
    // console.log({ radioOption }, { inputValue });
    if (inputValue && radioOption) {
      closeCustomModal("ceidg");

      mainBackgroundView({ event: "show" });

      const text = loadingFullScreen({
        event: "show",
        background: "transparent",
        icon: '<span class="material-icons-outlined">storage</span>',
        text: language[lang].get_data_ceidg,
      });
      loadingFullScreen({ event: "hide", object: "close_button" });
      let data;
      const checkedData = getCheckedDataLabels_Array();
      try {
        let response;
        if (radioOption === "nip") {
          response = await POST(`/ceidg/data`, {
            checkedData: checkedData,
            nip: inputValue,
          });
        } else if (radioOption === "regon") {
          response = await POST(`/ceidg/data`, {
            checkedData: checkedData,
            regon: inputValue,
          });
        } else {
          console.log("radio option ma wartosc: ", { radioOption });
        }

        // const response = await GET(`/ceidg/data?${radioOption}=${inputValue}`);
        // console.log({ response });
        data = JSON.parse(response).data;
        // console.log({ data });
      } catch (err) {
        $(text).html(language[lang].get_data_error);
        // mainBackgroundView({ event: "close" });
        loadingFullScreen({ event: "visible", object: "close_button" });
        return;
      }
      // loadingFullScreen({
      //   event: "close",
      // });
      loadingFullScreen({ event: "close" });
      // show user data
      // let source, loa;
      // for (const key in data) {
      //   source = data[key].data?.source;
      //   loa = data[key].data?.loa;
      //   break;
      // }
      // // console.log({ source }, { loa });
      const [acceptButton, rejectButton] = userDataView({
        event: "show",
        background: "transparent",
        // source: source,
        // loa: loa,
        data: data,
      });

      $(acceptButton).unbind("click");
      $(acceptButton).on("click", async () => {
        // dataView({ event: "close" });
        // console.log("{user accepted data}");
        userDataView({ event: "close" });
        const text = loadingFullScreen({
          event: "show",
          background: "transparent",
          text: language[lang].updating,
        });
        loadingFullScreen({ event: "hide", object: "close_button" });
        const response = await POST("/publishDataToTopic", data);
        // console.log({ response });
        setTimeout(async () => {
          try {
            await update();
          } catch (err) {
            // console.log({ err });
            $(text).html(language[lang].updating_error);
          }
          mainBackgroundView({ event: "close" });
          loadingFullScreen({ event: "close" });
        }, 5000);
      });

      $(rejectButton).unbind("click");
      $(rejectButton).on("click", () => {
        // dataView({ event: "close" });
        // console.log("{user rejected data}");
        mainBackgroundView({ event: "close" });
        userDataView({ event: "close" });
      });
    } else {
      // console.log("error: no input value or no radio option passed...");
    }
  });

  $(closeButton).unbind("click");
  $(closeButton).on("click", () => {
    closeCustomModal("ceidg");
  });
})();

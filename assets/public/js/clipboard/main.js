// // console.log("clipboard page");
// (async () => {
//   const token = await GET("/networkDrive/uniqueCode");
//   const code = parseJwt(token);
//   // console.log({ code });
// })();

// const editDropdown = $(
//   ".clipboard__page .clipboard__manager .clipboard__manager-edit .dropdown .dropdown-menu"
// );
// const editDropdownItems = $(
//   ".clipboard__page .clipboard__manager .clipboard__manager-edit .dropdown .dropdown-menu .dropdown-item"
// );
// const editDropdownCollapseItems = $(
//   ".clipboard__page .clipboard__manager .clipboard__manager-edit .dropdown .dropdown-menu .collapse"
// );
// // check user data collapsible elements on page
// for (const dropdownItem of editDropdownItems) {
//   $(dropdownItem).unbind("click");
//   $(dropdownItem).on("click", (e) => {
//     for (const collapseItem of editDropdownCollapseItems) {
//       $(collapseItem).collapse("hide");
//     }
//   });
// }
//
// $(editDropdown).on("click", (e) => {
//   $(e.target).hasClass("item_collapse") && e.stopPropagation();
// });

var showCustomModal = (name) => {
  const modal = $(`.${name}__modal`);
  // krs automatic
  if (name === "krs") {
    // get user given_name and name
    (async () => {
      try {
        const response = await POST("/user/label", {
          labels: ["given_name", "name"],
          scope: "PROFILE",
        });
        const json = JSON.parse(response);
        // console.log({ json });
        if (json?.given_name) {
          $("input[name=krs_name]").val(json.given_name);
        }
        if (json?.name) {
          $("input[name=krs_surname]").val(json.name);
        }
      } catch (err) {
        //...
      }
    })();
  }

  if (modal) {
    translateModal(name);
    const downloadHeader = $(modal).find(".download__modal-content-header");
    downloadHeader.length > 0 && $(downloadHeader).css("opacity", "1");
    $(modal).css({
      display: "flex",
    });
  }
};

var closeCustomModal = (name) => {
  const modal = $(`.${name}__modal`);
  const collapses = $(modal).find(".collapse");
  const modalUndearneath = $(modal).find(".modal__underneath");
  modalUndearneath.length > 0 && hideUnderneathModal();
  if (collapses.length > 0) {
    for (const collapse of collapses) {
      $(collapse).hasClass("show") && $(collapse).collapse("hide");
    }
  }
  $(modal).css({
    display: "none",
  });
};

var translateModal = (name) => {
  const active_lang = $("nav.navbar .language__active").data("active-language");
  const dictionary = language[active_lang];
  if (name === "upload") {
    const rejectButton = $(
      ".upload__modal .upload__modal-content .upload__modal-button-reject"
    );
    const acceptButton = $(
      ".upload__modal .upload__modal-content .upload__modal-button-accept"
    );
    const uploadButton = $(
      ".upload__modal .upload__modal-content .upload__button"
    );
    const titleButton = $(
      ".upload__modal .upload__modal-content .upload__modal-content-title"
    );
    const documentType = $(
      ".upload__modal .upload__modal-content .upload__modal-content-document-type"
    );
    const uploadSelects = $(
      ".upload__modal .upload__modal-content .upload__modal-select .select-option"
    );

    $(rejectButton).html(dictionary.custom_modal.upload.reject);
    $(acceptButton).html(dictionary.custom_modal.upload.accept);
    $(uploadButton).html(dictionary.custom_modal.upload.upload);
    $(titleButton).html(dictionary.custom_modal.upload.title);
    $(documentType).html(dictionary.custom_modal.upload.document_type);

    for (let i = 0; i < uploadSelects.length; i++) {
      $(uploadSelects[i]).html(dictionary.custom_modal.upload.select[i]);
    }
  } else if (name === "download") {
    const title = $(".download__modal .download__modal-content-title");
    const downloadButton = $(".download__modal .modal-button-download");
    const rejectButton = $(".download__modal .modal-button-reject");
    const instruction = $(
      ".download__modal .download__modal-description-title"
    );
    const instructionList = $(
      ".download__modal .download__modal-description-text ol"
    );
    const instructionSubtitle = $(".download__modal .instruction-text");
    const instructionAlternative = $(
      ".download__modal .alternative-instruction-point"
    );
    const generateCodeButton = $(
      ".download__modal .modal-button-generate-code"
    );
    const confirmButton = $(".download__modal .modal-button-confirm");
    const underneathModalCodeText = $(
      ".download__modal .modal__underneath-text"
    );
    $(title).html(dictionary.custom_modal.download.title);
    $(downloadButton).html(dictionary.custom_modal.download.download);
    $(rejectButton).html(dictionary.custom_modal.download.reject);
    $(instruction).html(dictionary.custom_modal.download.instruction);
    $(instructionSubtitle).html(
      dictionary.custom_modal.download.instruction_subtitle
    );
    $(instructionList).html("");
    for (const property in dictionary.custom_modal.download.instruction_point) {
      $(instructionList).append(
        `<li class="instruction-point">${dictionary.custom_modal.download.instruction_point[property]}</li>`
      );
    }
    $(instructionAlternative).html(
      dictionary.custom_modal.download.instruction__alternative
    );
    $(generateCodeButton).html(dictionary.custom_modal.download.generate_code);
    $(confirmButton).html(dictionary.custom_modal.download.confirm);
    $(underneathModalCodeText).html(
      dictionary.custom_modal.download.login_code_text
    );
  } else if (name === "krs") {
    const labels = $(".krs__modal .krs__modal-content .content-text");
    const acceptButton = $(
      ".krs__modal .krs__modal-content .content-button"
    ).val(dictionary.accept);
    for (let i = 0; i < labels.length; i++) {
      $(labels[i]).html(dictionary.custom_modal.krs.labels[i]);
    }
  } else if (name === "ceidg") {
    const acceptButton = $(
      ".ceidg__modal .ceidg__modal-content .content-button"
    ).val(dictionary.accept);
  } else if (name === "vat") {
    const labels = $(".vat__modal .vat__modal-content .form-check-label");
    const acceptButton = $(
      ".vat__modal .vat__modal-content .content-button"
    ).val(dictionary.accept);
    for (let i = 0; i < labels.length; i++) {
      $(labels[i]).html(dictionary.custom_modal.vat.labels[i]);
    }
  }
};

var checkboxes = $(".clipboard__list-item .form-check-input");
(() => {
  var myCodeInterval;
  var loadingInterval;
  const dataManagerEditDropdownModalItems = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item.modal-item"
  );
  const clipboardManagerButtons = $(
    ".clipboard__manager .clipboard__manager-button"
  );
  const selectAll = $(".clipboard__page .select__all .select__all-checkbox");

  $(document).ready(() => {
    // businessClientButtonsDecision();
    $('[data-bs-toggle="tooltip"]').tooltip();
    for (const button of clipboardManagerButtons) {
      $(button).css("pointer-events", "none");
    }
    $(
      ".consent__modal .consent__modal-content .consent__modal-content-button.disagree"
    ).unbind("click");
    $(
      ".consent__modal .consent__modal-content .consent__modal-content-button.disagree"
    ).on("click", (e) => {
      closeCustomModal("consent");
    });

    $(selectAll).on("click", () => {
      const all = $(selectAll).prop("checked");
      for (const checkbox of checkboxes) {
        if (all && !$(checkbox).prop("checked")) {
          $(checkbox).prop("checked", true);
        } else if (!all && $(checkbox).prop("checked")) {
          $(checkbox).prop("checked", false);
        }
      }
      showClipboardDataManager();
    });
  });

  // var businessClientButtonsDecision = () => {
  //   // for business client buttons special
  //   const cancelButton = $(".clipboard__manager .clipboard__manager-cancel");
  //   const acceptButton = $(".clipboard__manager .clipboard__manager-accept");
  //   let sessionId, redirectUri;
  //   let clientHash = readCookie("client");
  //   // // console.log({ clientHash });
  //   if (clientHash && clientHash.length > 0) {
  //     const clientData = JSON.parse(
  //       decodeURIComponent(b64DecodeUnicode(decodeURIComponent(clientHash[1])))
  //     );
  //     // // console.log({ clientData });
  //     sessionId = clientData.sessionId;
  //     redirectUri = clientData.redirectUri;
  //   }
  //
  //   // console.log({ sessionId }, { redirectUri });
  //
  //   cancelButton && $(cancelButton).unbind("click");
  //   cancelButton &&
  //     $(cancelButton).on("click", async () => {
  //       const decision = await showModal(
  //         "czy na pewno chcesz anulować uzupełnianie danych?",
  //         ["nie", "tak"]
  //       );
  //       // console.log({ decision });
  //       if ($(decision).data("modal-button") === "yes") {
  //         window.location.href = `${redirectUri}?sessionId=${sessionId}&status=failed`;
  //       }
  //     });
  //   // acceptButton && $(acceptButton).unbind("click");
  //   // acceptButton &&
  //   //   $(acceptButton).on("click", async () => {
  //   //
  //   //   });
  // };

  var showClipboardDataManager = () => {
    // // console.log('CALL SHOW CLIPBOARD DATA MANAGER');
    // ustawiam opcje jak aktywne --> do zrobienia
    const sources = $(".clipboard__manager-content .clipboard__manager-edit a");
    for (let src of sources) {
      $(src).addClass("disabled");
    }

    // =======================================
    // const checkboxes = $('.clipboard__list-item .form-check-input');
    const manager = $(".clipboard__manager");
    let mng_enabled = false;
    // const buttons = $('.clipboard__manager .clipboard__manager-button');
    const input = $(".clipboard__manager .clipboard__manager-edit-input");
    for (const checkbox of checkboxes) {
      if ($(checkbox).is(":checked")) {
        mng_enabled = true;
        if (!$(manager).hasClass("manager__active")) {
          // zmiana background
          $(manager).addClass("manager__active");
          // zmiana ikon
          $(input).css("color", "black");
          $(input).prop("disabled", false);
          $(input).hover(
            () => {
              $(input).css({
                opacity: ".5",
                cursor: "pointer",
              });
            },
            () => {
              $(input).css({
                opacity: "1",
                cursor: "default",
              });
            }
          );
          for (const button of clipboardManagerButtons) {
            $(button).css("pointer-events", "auto");
            $(button).css("color", "black");
            $(button).hover(
              () => {
                $(button).css({
                  opacity: ".5",
                  cursor: "pointer",
                });
              },
              () => {
                $(button).css({
                  opacity: "1",
                  cursor: "default",
                });
              }
            );
          }
        }

        // filter edit manager with proper sources
        const mapper = SOURCE_MAPPER;
        const label = $(checkbox)
          .parent()
          .find(".clipboard__list-item__secondary-text")
          .data("label");
        // console.log({ label });
        const src_mapper = SOURCE_MAPPER[label];
        // console.log("source mapper for that label: ", { src_mapper });
        if (src_mapper && src_mapper != undefined) {
          const sources = $(
            ".clipboard__manager-content .clipboard__manager-edit a"
          );
          for (let src of sources) {
            // console.log("checking if i can remove the disabled status");
            // console.log("source from manager: ", $(src).data("name"));
            if (src_mapper.includes($(src).data("name"))) {
              $(src).removeClass("disabled");
            }
          }
        }
      }
    }

    if (!mng_enabled) {
      $(manager).removeClass("manager__active");
      $(input).css("color", "#C8C8C8");
      $(input).prop("disabled", true);
      for (const button of clipboardManagerButtons) {
        $(button).css("pointer-events", "none");
        $(button).css("color", "#C8C8C8");
        $(button).off();
      }
    }
  };

  for (const checkbox of checkboxes) {
    $(checkbox).unbind("click");
    $(checkbox).on("click", showClipboardDataManager);
  }

  for (const item of dataManagerEditDropdownModalItems) {
    $(item).unbind("click");
    $(item).on("click", () => {
      const name = $(item).data("name");
      showCustomModal(name);
    });
  }

  $(".download__modal .download__modal-content .modal-close").unbind("click");
  $(".download__modal .download__modal-content .modal-close").on(
    "click",
    (e) => {
      const name = $(e.currentTarget).data("label");
      closeCustomModal(name);
    }
  );

  // var generateCode = async () => {
  //   if (myCodeInterval) {
  //     return;
  //   }
  //   let expiration;
  //   let interval;
  //   const loading = (exp, intv) => {
  //     const loader = $(".modal__underneath .modal__underneath-load .loader");
  //     const width = Math.floor((exp / intv) * 100);
  //     $(loader).css("width", `${width}%`);
  //   };
  //
  //   const getCode = () => {
  //     return new Promise((resolve) => {
  //       $.ajax({
  //         url: "/networkDrive/uniqueCode",
  //         type: "GET",
  //         error: (error) => {
  //           // console.log(error);
  //           resolve();
  //         },
  //         success: (token) => {
  //           // console.log({
  //           //   token,
  //           // });
  //           const currentTimeInSeconds = Math.floor(
  //             new Date().getTime() / 1000
  //           );
  //           // console.log({
  //           //   currentTimeInSeconds,
  //           // });
  //           const data = parseJwt(token);
  //           expiration = (data.exp - currentTimeInSeconds) * 1000;
  //           interval = (data.exp - data.iat) * 1000;
  //           loading(expiration, interval);
  //           loadingInterval && clearInterval(loadingInterval);
  //           loadingInterval = null;
  //           loadingInterval = setInterval(() => {
  //             expiration -= 100;
  //             loading(expiration, interval);
  //           }, 100);
  //           const codeInput = $(".modal__underneath .modal__underneath-code");
  //           $(codeInput).val(data.code);
  //           resolve();
  //         },
  //       });
  //     });
  //   };
  //   await getCode();
  //   // console.log({
  //   //   expiration,
  //   // });
  //   // console.log({
  //   //   interval,
  //   // });
  //   showUnderneath("download");
  //   setTimeout(async () => {
  //     await getCode();
  //     !myCodeInterval && (myCodeInterval = setInterval(getCode, interval));
  //   }, expiration);
  // };

  $(".download__modal .modal-button-generate-code").unbind("click");
  // $(".download__modal .modal-button-generate-code").on("click", generateCode);

  // var parseJwt = (token) => {
  //     var base64Url = token.split('.')[1];
  //     var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  //     var jsonPayload = decodeURIComponent(atob(base64).split('').map((c) => {
  //         return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  //     }).join(''));
  //
  //     return JSON.parse(jsonPayload);
  // }

  var showUnderneath = (modalName) => {
    const underneathModal = $(".modal__underneath");
    const frontModal = $(`.${modalName}__modal .${modalName}__modal-content`);
    // animate to show code modal
    $(underneathModal).css({
      transform: "translateY(30px)",
    });
    $(frontModal).css({
      transform: "translateY(-70px)",
    });
  };

  var hideUnderneathModal = () => {
    const codeModal = $(".modal__underneath");
    const downloadModal = $(".download__modal .download__modal-content");
    // animate hiding
    $(codeModal).css({
      transform: "",
    });
    $(downloadModal).css({
      transform: "",
    });
    clearAllIntervals();
  };

  $(".download__modal .modal__underneath-text").unbind("click");
  $(".download__modal .modal__underneath-text").on(
    "click",
    hideUnderneathModal
  );

  var clearAllIntervals = () => {
    myCodeInterval && clearInterval(myCodeInterval);
    loadingInterval && clearInterval(loadingInterval);
    myCodeInterval = null;
    loadingInterval = null;
  };

  // var downloadModalButtonClicked = async (e) => {
  //   // console.log({ e });
  //   const downloadModal = $(".download__modal");
  //   const instructionCollapse = $(".download__modal .collapse");
  //   const header = $(".download__modal .download__modal-content-header");
  //   !$(instructionCollapse).hasClass("show") &&
  //     $(instructionCollapse).collapse("show");
  //   $(header).css("opacity", ".2");
  //
  //   if ($(e).data("label") === "reject") {
  //     // odpal stronke z ładowaniem danych
  //   }
  // };
  //
  // $(
  //   ".download__modal .download__modal-content .download__modal-content-header .modal-button"
  // ).unbind("click");
  // $(
  //   ".download__modal .download__modal-content .download__modal-content-header .modal-button"
  // ).on("click", (e) => {
  //   downloadModalButtonClicked(e.currentTarget);
  // });

  var govUpdate = async (e) => {
    // console.log({
    //   e,
    // });
    $(e).replaceWith(
      '<div class="upload__spinner-border spinner-border text-secondary" role="status"><span class="sr-only">Loading...</span></div>'
    );
    try {
      await update();
      const spinner = $(".download__modal .upload__spinner-border");
      $(spinner).replaceWith(
        '<span class="modal-button modal-button-confirm" data-label="reject">zrobione</span>'
      );
      closeCustomModal("download");
    } catch (err) {
      // console.log({
      //   err,
      // });
      const spinner = $(".download__modal .upload__spinner-border");
      $(spinner).replaceWith(
        '<span class="modal-button modal-button-confirm" data-label="reject">zrobione</span>'
      );
      closeCustomModal("download");
    }
  };

  $(".download__modal .modal-button-confirm").unbind("click");
  $(".download__modal .modal-button-confirm").on("click", (e) => {
    govUpdate(e.currentTarget);
  });
})();

var getConsent = (service) => {
  return new Promise(async (resolve, reject) => {
    try {
      const consentId = await POST("/consentId", {
        service: service,
      });
      resolve(consentId);
    } catch (err) {
      reject(err);
    }
  });
};

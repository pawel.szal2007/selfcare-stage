(() => {
  const vatButton = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='vat']"
  );
  const radiosButtons = $(".vat__modal .vat__modal-content .content-radio");
  const vatInput = $(".vat__modal .vat__modal-content .content-input");
  const acceptButton = $(".vat__modal .vat__modal-content .content-button");
  const closeButton = $(".vat__modal .vat__modal-content .content-close");

  $(vatButton).unbind("click");
  $(vatButton).on("click", async () => {
    try {
      showCustomModal("vat");
      // const text = loadingFullScreen({
      //   event: "show",
      //   background: "rgb(42,107,176, .9)",
      //   icon: '<span class="material-icons-outlined">storage</span>',
      //   text: "trwa pobieranie danych z rejestru podatników VAT...",
      // });
      // // window.location.href = response;
      //
      // //do VAT potrzebny jest albo NIP albo REGON albo numer konta bankowego
      //
      // const response = await GET(`/vat/data?params...to...be...done`);
      // // console.log({ response });
    } catch (err) {
      // console.log({ err });
    }
  });

  $(acceptButton).unbind("click");
  $(acceptButton).on("click", async () => {
    const lang = $("nav.navbar .language__active").data("active-language");
    let radioOption;
    for (const radio of radiosButtons) {
      if ($(radio).is(":checked")) {
        radioOption = $(radio).val();
      }
    }
    const inputValue = $(vatInput).val();
    // console.log({ radioOption }, { inputValue });
    if (inputValue && radioOption) {
      closeCustomModal("vat");

      mainBackgroundView({ event: "show" });
      const text = loadingFullScreen({
        event: "show",
        background: "transparent",
        icon: '<span class="material-icons-outlined">storage</span>',
        text: language[lang].get_data_vat,
      });

      loadingFullScreen({ event: "hide", object: "close_button" });
      let data;
      try {
        const response = await GET(`/vat/data?${radioOption}=${inputValue}`);
        // console.log({ response });
        data = JSON.parse(response).data;
        // console.log({ data });
      } catch (err) {
        $(text).html(language[lang].get_data_error);
        // mainBackgroundView({ event: "close" });
        loadingFullScreen({ event: "visible", object: "close_button" });
        return;
      }

      loadingFullScreen({
        event: "close",
      });

      const [acceptButton, rejectButton] = userDataView({
        event: "show",
        background: "transparent",
        // source: source,
        // loa: loa,
        data: data,
      });

      acceptButton && $(acceptButton).unbind("click");
      acceptButton &&
        $(acceptButton).on("click", async () => {
          // dataView({ event: "close" });
          // console.log("{user accepted data}");
          userDataView({ event: "close" });
          const text = loadingFullScreen({
            event: "show",
            background: "transparent",
            text: language[lang].updating,
          });
          loadingFullScreen({ event: "hide", object: "close_button" });
          const response = await POST("/publishDataToTopic", data);
          // console.log({ response });
          setTimeout(async () => {
            try {
              await update();
            } catch (err) {
              // console.log({ err });
              $(text).html(language[lang].updating_error);
            }
            mainBackgroundView({ event: "close" });
            loadingFullScreen({ event: "close" });
          }, 5000);
        });

      rejectButton && $(rejectButton).unbind("click");
      rejectButton &&
        $(rejectButton).on("click", () => {
          // dataView({ event: "close" });
          // console.log("{user rejected data}");
          mainBackgroundView({ event: "close" });
          userDataView({ event: "close" });
        });

      // const text = loadingFullScreen({
      //   event: "show",
      //   background: "rgb(42,107,176, .9)",
      //   icon: '<span class="material-icons-outlined">storage</span>',
      //   text: "trwa pobieranie danych z rejestru podatników VAT...",
      // });
      // const response = await GET(`/vat/data?${radioOption}=${inputValue}`);
      // // console.log({ response });
      // loadingFullScreen({
      //   event: "close",
      // });
    } else {
      // console.log("error: no input value or radio option passed...");
    }
  });

  $(closeButton).unbind("click");
  $(closeButton).on("click", () => {
    closeCustomModal("vat");
  });
})();

(() => {
  const krsButton = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='krs']"
  );
  const acceptButton = $(".krs__modal .krs__modal-content .content-button");
  const nameInput = $(
    ".krs__modal .krs__modal-content .content-input[name='krs_name']"
  );
  const surnameInput = $(
    ".krs__modal .krs__modal-content .content-input[name='krs_surname']"
  );
  const nipInput = $(
    ".krs__modal .krs__modal-content .content-input[name='krs_nip']"
  );
  const krsPersonIdInput = $(
    ".krs__modal .krs__modal-content .content-input[name='krs_number']"
  );
  const closeButton = $(".krs__modal .krs__modal-content .content-close");

  // console.log({ nipInput }, { krsPersonIdInput });

  $(krsButton).unbind("click");
  $(krsButton).on("click", async () => {
    try {
      showCustomModal("krs");
    } catch (err) {
      // console.log({ err });
    }
  });

  $(acceptButton).unbind("click");
  $(acceptButton).on("click", async () => {
    const lang = $("nav.navbar .language__active").data("active-language");
    const nip = $(nipInput).val();
    const krsPersonId = $(krsPersonIdInput).val();
    const name = $(nameInput).val();
    const surname = $(surnameInput).val();

    // console.log({ nip }, { krsPersonId });
    if (name && surname && (krsPersonId || nip)) {
      closeCustomModal("krs");
      mainBackgroundView({ event: "show" });
      const text = loadingFullScreen({
        event: "show",
        background: "transparent",
        icon: '<span class="material-icons-outlined">storage</span>',
        text: language[lang].get_data_krs,
      });

      loadingFullScreen({ event: "hide", object: "close_button" });
      let data;
      const checkedData = getCheckedDataLabels_Array();
      try {
        const response = await POST("/krs/data", {
          nip: nip,
          krsPersonId: krsPersonId,
          name: name,
          surname: surname,
          checkedData: checkedData,
        });
        // const response = await GET(
        //   `/krs/data?nip=${nip}&krsPersonId=${krsPersonId}&name=${name}&surname=${surname}`
        // );
        // console.log({ response });
        data = JSON.parse(response).data;
        console.log({ data });
      } catch (err) {
        $(text).html(language[lang].get_data_error);
        // mainBackgroundView({ event: "close" });
        loadingFullScreen({ event: "visible", object: "close_button" });
        return;
      }

      loadingFullScreen({
        event: "close",
      });

      const [acceptButton, rejectButton] = userDataView({
        event: "show",
        background: "transparent",
        // source: source,
        // loa: loa,
        data: data,
      });

      $(acceptButton).unbind("click");
      $(acceptButton).on("click", async () => {
        // dataView({ event: "close" });
        // console.log("{user accepted data}");
        userDataView({ event: "close" });
        const text = loadingFullScreen({
          event: "show",
          background: "transparent",
          text: language[lang].updating,
        });
        loadingFullScreen({ event: "hide", object: "close_button" });
        const response = await POST("/publishDataToTopic", data);
        // console.log({ response });
        setTimeout(async () => {
          try {
            await update();
          } catch (err) {
            // console.log({ err });
            $(text).html(language[lang].updating_error);
          }
          mainBackgroundView({ event: "close" });
          loadingFullScreen({ event: "close" });
        }, 5000);
      });

      $(rejectButton).unbind("click");
      $(rejectButton).on("click", () => {
        // dataView({ event: "close" });
        // console.log("{user rejected data}");
        mainBackgroundView({ event: "close" });
        userDataView({ event: "close" });
      });
    } else {
      // pole było puste trzeba obsluzyc blad ze nie ma nipu
    }
  });

  $(closeButton).unbind("click");
  $(closeButton).on("click", () => {
    closeCustomModal("krs");
  });
})();

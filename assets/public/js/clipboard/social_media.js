(() => {
  const facebookButton = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='facebook']"
  );
  // const linkedInButton = $(
  //   ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='linkedin']"
  // );
  const closeButton = $(".loading__transparent .loading__transparent-close");

  // // console.log({ facebookButton });
  const getLoginResponse = () => {
    return new Promise((resolve) => {
      deleteCookie("fblo_876070029684187");
      FB.getLoginStatus((response) => {
        resolve(response);
      });
    });
  };

  const deleteCookie = (name) => {
    document.cookie =
      name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
  };
  //
  const logInToFacebook = () => {
    return new Promise((resolve, reject) => {
      FB.login(
        (response) => {
          console.log({ response });
          if (response.authResponse) {
            resolve(response);
          } else {
            reject({
              error: "err",
              message: "User cancelled login or did not fully authorize.",
            });
          }
        },
        {
          scope:
            "public_profile,email,user_link,user_gender,user_location,user_hometown,user_birthday",
        }
      );
    });
  };
  //
  const getFacebookData = async (authResponse) => {
    const lang = $("nav.navbar .language__active").data("active-language");
    const text = loadingFullScreen({
      event: "show",
      background: "var(--main-background)",
      icon: '<i class="fab fa-facebook-square fa-2x"></i>',
      text: "pobieranie danych z facebook...",
    });
    $(closeButton).unbind("click");
    $(closeButton).on("click", () => {
      loadingFullScreen({ event: "close" });
    });
    console.log("getting facebook data... ", { authResponse });
    try {
      const checkedData = getCheckedDataLabels_Array();
      authResponse.checkedData = checkedData;
      const response = await POST("/api/facebook", authResponse);
      const data = JSON.parse(response).data;
      console.log({ data });
      loadingFullScreen({ event: "close" });
      showData(data);
    } catch (err) {
      console.log(err);
      $(text).html(language[lang].get_data_error);
      // mainBackgroundView({ event: "close" });
      loadingFullScreen({ event: "visible", object: "close_button" });
      return;
    }

    // FB.api("/me", (userData) => {
    //   if (userData.error && userData.error.type === "OAuthException") {
    //     $(text).html("wygasła sesja logowania, proszę zalogować się ponownie");
    //     loadingFullScreen({ event: "close" });
    //     $(facebookButton).click();
    //     return;
    //   }
    //   console.log({ userData });
    //   console.log("Welcome: " + userData.name);
    //   loadingFullScreen({
    //     event: "close",
    //   });
    // });
  };

  $(facebookButton).on("click", async () => {
    try {
      // // console.log("button clicked, getting data");
      // const response = await GET("/path/facebook");
      // // console.log({ response });
      // // window.location.href = response;
      // let socket = socketConnect();
      //
      // const child = appCustomWindow({
      //   event: "show",
      //   name: "cyber-facebook",
      //   url: response,
      // });
      //
      // console.log({ child });
      // var timer = setInterval(() => {
      //   if (child.closed) {
      //     clearInterval(timer);
      //     socket.close(1000);
      //   }
      // }, 500);
      //
      // socket.onclose = (event) => {
      //   if (event.wasClean) {
      //     console.log(
      //       `[close] Websocket connection closed cleanly, code=${event.code} reason=${event.reason}`
      //     );
      //   } else if (event.code === 1006) {
      //     console.log(`websocket connection closed with event code 1006`);
      //     alert(`websocket connection closed with event code 1006`);
      //     socket = socketConnect();
      //     getWSMessage(socket);
      //   }
      // };
      //
      // const getWSMessage = (socket) => {
      //   socket.onmessage = async (event) => {
      //     const message = JSON.parse(event.data);
      //     if (message.event === "get_data") {
      //       if (message.checked) {
      //         // console.log({ message });
      //         const checkedData = getCheckedDataLabels_Array();
      //         // console.log({ checkedData });
      //         socket.send(
      //           JSON.stringify({
      //             event: "response_data",
      //             checked: true,
      //             data: checkedData,
      //           })
      //         );
      //       }
      //     }
      //     if (message.event && message.event === "accept") {
      //       console.log({ message });
      //     }
      //     if (message.event === "facebook_update") {
      //       // console.log({ message });
      //       if (message.status === "done") {
      //         const data = message.data;
      //         appCustomWindow({
      //           event: "close",
      //           name: "cyber-facebook",
      //         });
      //         // wywolac obsluge danych
      //         showData(data);
      //       } else {
      //         appCustomWindow({
      //           event: "close",
      //           name: "cyber-facebook",
      //         });
      //       }
      //       socket.close(1000);
      //     }
      //   };
      // };
      // getWSMessage(socket);

      let response = await getLoginResponse();
      console.log({ response });
      if (response.status === "connected") {
        console.log("user logged in");
        getFacebookData(response.authResponse);
      } else {
        console.log("user not logged, please log in");
        response = await logInToFacebook();
        console.log({ response });
        console.log("user logged in");
        getFacebookData(response.authResponse);
      }
    } catch (err) {
      console.log({ err });
    }
  });

  // $(linkedInButton).on("click", async () => {
  //   try {
  //     // console.log("linkedIn button clicked, getting data");
  //     const response = await GET("/path/linkedin");
  //     // console.log({ response });
  //     // window.location.href = response;
  //     appCustomWindow({
  //       event: "show",
  //       name: "cyber-linkedin",
  //       url: response,
  //     });
  //
  //     socket.onmessage = async (event) => {
  //       const message = JSON.parse(event.data);
  //       if (message.event === "linkedin_update") {
  //         // console.log({ message });
  //         if (message.status === "done") {
  //           const data = message.data;
  //           appCustomWindow({
  //             event: "close",
  //             name: "cyber-linkedin",
  //           });
  //           showData(data);
  //         } else {
  //           appCustomWindow({
  //             event: "close",
  //             name: "cyber-linkedin",
  //           });
  //         }
  //       }
  //     };
  //   } catch (err) {
  //     // console.log({ err });
  //   }
  // });

  const showData = (data) => {
    mainBackgroundView({ event: "show" });
    const [acceptButton, rejectButton] = userDataView({
      event: "show",
      background: "transparent",
      // source: source,
      // loa: loa,
      data: data,
    });

    $(acceptButton).unbind("click");
    $(acceptButton).on("click", async () => {
      // dataView({ event: "close" });
      // console.log("{user accepted data}");
      userDataView({ event: "close" });
      const text = loadingFullScreen({
        event: "show",
        background: "transparent",
        text: "jeszcze tylko chwila, trwa aktualizacja Twoich danych...",
      });
      loadingFullScreen({ event: "hide", object: "close_button" });
      const response = await POST("/publishDataToTopic", data);
      // console.log({ response });
      setTimeout(async () => {
        await update();
        mainBackgroundView({ event: "close" });
        loadingFullScreen({ event: "close" });
      }, 5000);
    });

    $(rejectButton).unbind("click");
    $(rejectButton).on("click", () => {
      // dataView({ event: "close" });
      // console.log("{user rejected data}");
      mainBackgroundView({ event: "close" });
      userDataView({ event: "close" });
    });
  };
})();

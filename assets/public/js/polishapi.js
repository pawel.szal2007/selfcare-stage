var queryString = window.location.search;
// console.log(queryString);

var getPolishApiData = (e) => {
  const lang = $("nav.navbar .language__active").data("active-language");
  // const loadingContent = await POST("/loading", "");
  let text;
  if (lang === "pl") {
    text = "proszę czekać, zbieramy dane z Twojego rachunku bankowego...";
  } else {
    text = "please wait, we are processing your bank account data...";
  }

  $(".page .page-section").html(`<div class="container-fluid h-100">
<div class="d-flex justify-content-center align-items-center" style="height: 100%">
  <div class="spinner-border" style="color: #003574; width: 3rem; height: 3rem;" role="status">
  </div>
  <span class="text-center ms-3">${text}</span>
</div>`);

  const code = $(e).data("code") ? $(e).data("code") : e.code;
  const state = $(e).data("state") ? $(e).data("state") : e.state;
  (async () => {
    try {
      const data = await POST("/polishapi/accountsInformation", {
        code: code,
        state: state,
      });
      const res = JSON.parse(data);
      // console.log({
      //   res,
      // });

      res.data.forEach((account, index) => {
        const accountExistOnPage = $("#account__0");
        if (accountExistOnPage.length > 0) {
          $(".page .page-section")
            .append(`<div id="account__${index}" class="row mt-5" >
        <div class="col-12 col-md-5">
          <div class="bank__account-container mdc-card px-4">
          </div>
        </div>
            <div class="mdc-card transaction__container col-12 col-md-7">
              <div class="history__data px-5 py-2">
                <h4>Historia transakcji</h4>
              </div>
            </div>
          </div>`);
        } else {
          $(".page .page-section")
            .html(`<div id="account__${index}" class="row">
        <div class="col-12 col-md-5">
          <div class="bank__account-container mdc-card px-4">
          </div>
        </div>
            <div class="mdc-card transaction__container col-12 col-md-7">
              <div class="history__data px-5 py-2">
                <h4>Historia transakcji</h4>
              </div>
            </div>
          </div>`);
        }

        // bank account
        // const bankAccountData = res.data.bank_account;
        if (
          account.hasOwnProperty("accountNameClient") &&
          account.hasOwnProperty("accountType") &&
          account.hasOwnProperty("accountNumber") &&
          account.hasOwnProperty("availableBalance") &&
          account.hasOwnProperty("currency") &&
          account.hasOwnProperty("historyTransactions")
        ) {
          // for (const property in account) {
          $(`#account__${index} .bank__account-container`)
            .append(`<div class="py-2">
          <h4>Konto: ${account.accountNameClient}</h4>
          <p>typ konta: ${account.accountType.description}</p>
          <p>iban: ${account.accountNumber}</p>
          <p>dostępne środki: ${account.availableBalance}</p>
          <p>waluta: ${account.currency}</p></div>
          <hr/>`);
          // }
        } else {
          $(`#account__${index} .bank__account-container`)
            .append(`<div class="py-2">
        <h4>Brak danych dotyczących wskazanego konta bankowego</h4></div>
        <hr/>`);
        }

        // transactions
        const transactions = account.historyTransactions;
        let incomes = 0;
        let expenses = 0;
        for (const transaction of transactions) {
          let color;
          let amount = parseFloat(transaction.amount);
          if (amount < 0) {
            expenses += amount;
            color = "var(--bs-danger)";
          } else {
            incomes += amount;
            color = "var(--bs-success)";
          }

          $(`#account__${index} .transaction__container .history__data`)
            .append(`<div class="ccontainer py-2">
                  <h6 class="transaction__date">${
                    transaction.tradeDate.split("T")[0]
                  }</h6>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="transaction__left d-flex flex-column">
                      <div class="transaction__recipient">
                        <span class="fw-bold">${
                          transaction.recipient.nameAddress.value === undefined
                            ? "b.d."
                            : transaction.recipient.nameAddress.value
                        }</span>
                      </div>
                      <div class="transaction__name">
                        <span>${transaction.description}</span>
                      </div>
                    </div>
                    <div class="transaction__right" style="color: ${color}; font-weight: bold">
                      ${transaction.amount} ${transaction.currency}
                    </div>
                  </div>
                </div><hr/>`);
        }

        incomes = Math.floor(incomes);
        expenses = Math.floor(expenses) * -1;

        $(`#account__${index} .bank__account-container`)
          .append(`<h6 class="incomes">przychody: <div class="fw-bold" style="display: inline; color: var(--bs-success)">${incomes} ${account.currency}</div></h6>
          <h6 class="other_expenses">pozostałe wydatki: <div class="fw-bold" style="display: inline; color: var(--bs-danger)">${expenses} ${account.currency}</div><h6>`);

        $(`#account__${index} .bank__account-container`).append(
          `<span class="refresh material-icons-outlined" data-code=${code} data-state=${state} onClick="getPolishApiData(this)">refresh</span>
          <button id="credit__btn-save-${index}" type="button" data-index=${index} class="mt-2 btn btn-secondary btn-sm credit__profile-data-btn-save">zapisz dane</button>`
        );

        // save the data
        $(`#credit__btn-save-${index}.credit__profile-data-btn-save`).unbind(
          "click"
        );
        $(`#credit__btn-save-${index}.credit__profile-data-btn-save`).on(
          "click",
          (e) => {
            $(".page .page-section").html(`<div class="container-fluid h-100">
        <div class="d-flex justify-content-center align-items-center" style="height: 100%">
          <div class="spinner-border" style="color: #003574; width: 3rem; height: 3rem;" role="status">
          </div>
          <span class="text-center ms-3">zapisywanie danych profilu kredytowego...</span>
        </div>`);

            (async () => {
              try {
                const index = $(e.currentTarget).data("index");
                const response_ = await POST("/profiles/credit/save", {
                  data: res.data[index],
                });
                console.log({ response_ });
                const response = await POST("/updateManually", {
                  incomes: incomes,
                  other_expenses: expenses,
                  loa: "high",
                  source: "PKOBP API/Polish API",
                });
                console.log({ response });
                // not found data in clipboard
                let cookieHash = readCookie("clipboard");
                console.log(cookieHash);
                if (cookieHash && cookieHash.length > 0) {
                  const cookiesData = JSON.parse(
                    decodeURIComponent(
                      b64DecodeUnicode(decodeURIComponent(cookieHash[1]))
                    )
                  );

                  if (cookiesData.length > 0) {
                    await update();
                    showToast(
                      "_add-success",
                      '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
                      "aktualizacja danych",
                      `profil kredytowy został zaktualizowany`
                    );
                  } else {
                    setTimeout(() => {
                      $(
                        "nav.sidebar .list-item a[data-name='profiles/credit']"
                      ).click();
                    }, 3000);
                  }
                } else {
                  setTimeout(() => {
                    $(
                      "nav.sidebar .list-item a[data-name='profiles/credit']"
                    ).click();
                  }, 3000);
                }

                // found data in clipboard like: incomes, other_expenses
              } catch (err) {
                $(".page .page-section")
                  .html(`<div class="container-fluid h-100">
        <div class="d-flex justify-content-center align-items-center" style="height: 100%">
          </div>
          <span class="text-center ms-3">błąd zapisywania danych, spróbuj ponownie później...</span>
        </div>`);
              }
            })();
          }
        );
      });
    } catch (err) {
      console.log({
        err,
      });
      $(".page .page-section").html(`<div class="container-fluid h-100">
        <div class="d-flex justify-content-center align-items-center" style="height: 100%">
          <span class="text-center ms-3">${language[lang].banqup_data_error}</span>
        </div>`);
    }
  })();
};

if (queryString && queryString != undefined) {
  const urlParams = new URLSearchParams(queryString);
  // const clientId = urlParams.get("clientId");
  const code = urlParams.get("code");
  const state = urlParams.get("state");
  if (urlParams.has("code") && urlParams.has("state")) {
    // console.log(`polishapi callback... we can download data`);
    // console.log(consentId);
    const lang = $("nav.navbar .language__active").data("active-language");
    let newUrl =
      window.location.protocol +
      "//" +
      window.location.host +
      window.location.pathname;

    window.history.pushState(
      {
        path: newUrl,
      },
      "",
      newUrl
    );

    getPolishApiData({ code: code, state: state });
  } else {
    console.log(`no polishapi callback...`);
  }
}

// listeners
var banqp_imgs = $(".banqup__image-bank-logo");

banqp_imgs &&
  $(banqp_imgs).on("click", async (e) => {
    // console.log("clicked ", e.currentTarget);
    const bank_id = $(e.currentTarget).data("bank-id");
    // console.log("bank id: ", bank_id);
    const lang = $("nav.navbar .language__active").data("active-language");
    try {
      // const loadingContent = await POST("/loading", "");
      $(".page .page-section").html(`<div class="container-fluid h-100">
        <div class="d-flex justify-content-center align-items-center" style="height: 100%">
          <div class="spinner-border" style="color: #003574; width: 3rem; height: 3rem;" role="status">
          </div>
          <span class="text-center ms-3">${language[lang].banqup_loading}</span>
        </div>`);

      const response = await POST("/polishapi/request/authorize", {
        bank_id: bank_id,
      });
      const json = JSON.parse(response);
      // console.log({
      //   json,
      // });
      window.location.href = json.data.aspspRedirectUri;
    } catch (err) {
      console.log({
        err,
      });
      $(".page .page-section").html(`<div class="container-fluid h-100">
        <div class="d-flex justify-content-center align-items-center" style="height: 100%">
          <span class="text-center ms-3">${language[lang].banqup_error}</span>
        </div>`);
    }
  });

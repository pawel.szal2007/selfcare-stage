$(document).ready(() => {
  const tables = $("table.table");
  const lang = $("nav.navbar .language__active").data("active-language");
  for (const table of tables) {
    const options = getTableOptions(
      lang,
      $(table).hasClass("table__simplified")
    );
    // console.log({ options });

    $(table).DataTable(options);

    // ustawienie filtrów
    const filter = $("#dataTablesScript").data("filter");
    console.log({ filter });
    // if ($(table).attr("id") === "history" || $(table).attr("id") === "data") {
    $(table).DataTable().search(filter).draw();
    // }

    if ($(table).hasClass("table__simplified")) {
      // if (lang === 'pl') {
      //   const options = getTableOptions(lang, $(table).hasClass('table__simplified'))
      //   $(table).DataTable(options)
      // } else {
      //   $(table).DataTable({ responsive: true })
      // }
      $($(".dataTables_wrapper").children()[0]).css("margin", "20px 30px");
      $($($(".dataTables_wrapper").children()[0]).children()[0]).css(
        "display",
        "none"
      );
      $($($(".dataTables_wrapper").children()[0]).children()[1]).css(
        "display",
        "flex"
      );
      $($(".dataTables_wrapper").children()[2]).css("margin", "20px 30px");
      $($($(".dataTables_wrapper").children()[2]).children()[0]).css(
        "visibility",
        "hidden"
      );
    } else {
      table.parentElement.parentElement.style.overflowX = "scroll";
      table.parentElement.parentElement.style.marginBottom = "20px";
    }
  }
  //
  // const filter = $("#dataTablesScript").data("filter");
  // console.log({ filter });
  // if (filter) {
  //   $("#history_filter input").val(filter);
  //   $("#history_filter input").focus();
  //   $("#history_filter input").click();
  // }
});

var getTableOptions = (language, simplified) => {
  let options = {};
  if (language === "pl") {
    if (simplified) {
      options = {
        pageLength: 3,
        responsive: true,
        order: [[0, "desc"]],
        language: {
          lengthMenu: "wyświetl _MENU_ wyników na stronie",
          zeroRecords: "nie znaleziono żadnych wyników",
          info: "strona _PAGE_ z _PAGES_",
          infoEmpty: "brak wyników",
          search: "szukaj:",
          infoFiltered: "(przefiltrowano _MAX_)",
          paginate: {
            first: "pierwszy",
            last: "ostatni",
            next: "następna",
            previous: "poprzednia",
          },
        },
      };
    } else {
      options = {
        responsive: true,
        order: [[0, "desc"]],
        language: {
          lengthMenu: "wyświetl _MENU_ wyników na stronie",
          zeroRecords: "nie znaleziono żadnych wyników",
          info: "strona _PAGE_ z _PAGES_",
          infoEmpty: "brak wyników",
          search: "szukaj:",
          infoFiltered: "(przefiltrowano _MAX_)",
          paginate: {
            first: "pierwszy",
            last: "ostatni",
            next: "następna",
            previous: "poprzednia",
          },
        },
      };
    }
  } else {
    if (simplified) {
      options = {
        responsive: true,
        order: [[0, "desc"]],
        pageLength: 1,
      };
    } else {
      options = {
        order: [[0, "desc"]],
        responsive: true,
      };
    }
  }
  return options;
};

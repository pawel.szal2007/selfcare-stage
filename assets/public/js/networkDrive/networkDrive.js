console.log("hello from network drive login content... :)");

(async () => {
  try {
    const driveCode = await GET("/networkDrive/uniqueCode");
    // const data = parseJwt(token);
    const url = `networkdrivepw:code=${driveCode}/service=null`;
    console.log({ driveCode });
    var sUsrAg = navigator.userAgent;

    let isExecuted = confirm(
      "czy chcesz zalogować się do aplikacji dysku sieciowego?"
    );

    console.log(isExecuted);
    if (isExecuted) {
      if (sUsrAg.indexOf("Firefox") > -1) {
        openNewWindow({
          event: "show",
          name: "cyber-networkdrive",
          url: url,
          height: 330,
        });
      } else window.location.href = url;
    }
  } catch (err) {
    console.log(err);
    // alert(`couldn't open network drive, websocket connection close...`);
  }
})();

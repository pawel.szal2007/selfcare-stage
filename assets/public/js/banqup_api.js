var getBanqwareData = (e) => {
  console.log({
    e,
  });
  const lang = $("nav.navbar .language__active").data("active-language");
  // const loadingContent = await POST("/loading", "");
  let text;
  if (lang === "pl") {
    text = "proszę czekać, zbieramy dane z Twojego rachunku bankowego...";
  } else {
    text = "please wait, we are processing your bank account data...";
  }

  $(".page .page-section").html(`<div class="container-fluid h-100">
<div class="d-flex justify-content-center align-items-center" style="height: 100%">
  <div class="spinner-border" style="color: #003574; width: 3rem; height: 3rem;" role="status">
  </div>
  <span class="text-center ms-3">${text}</span>
</div>`);

  const consentId = $(e).data("consentid") ? $(e).data("consentid") : e;
  console.log({
    consentId,
  });
  (async () => {
    try {
      const data = await POST("/api/banqup/bankAccount", {
        consentId: consentId,
      });
      const res = JSON.parse(data);
      console.log({
        res,
      });
      $(".page .page-section").html(`<div class="row">
      <div class="col-12 col-md-5">
        <div class="bank__account-container mdc-card px-4">
        </div>
      </div>
          <div class="mdc-card transaction__container col-12 col-md-7">
            <div class="history__data px-5 py-2">
              <h4>Historia transakcji</h4>
            </div>
          </div>
        </div>`);

      // bank account
      const bankAccountData = res.data.bank_account;
      if (bankAccountData.length > 0) {
        for (const account of bankAccountData) {
          $(".bank__account-container").append(`<div class="py-2">
          <h4>Konto: ${account.holderInformation.nameAddress}</h4>
          <p>iban: ${account.ids.iban}</p>
          <p>waluta: ${account.currencyCode}</p></div>
          <hr/>`);
        }
      } else {
        $(".bank__account-container").append(`<div class="py-2">
        <h4>Brak danych dotyczących wskazanego konta bankowego</h4></div>
        <hr/>`);
      }

      // transactions
      const transactions = res.data.transactions;
      let incomes = 0;
      let expenses = 0;
      if (transactions.length > 0) {
        for (const transaction of transactions) {
          const items = transaction.items;
          for (const item of items) {
            let color;
            if (item.amount < 0) {
              expenses += item.amount;
              color = "var(--bs-danger)";
            } else {
              incomes += item.amount;
              color = "var(--bs-success)";
            }

            $(".transaction__container .history__data")
              .append(`<div class="ccontainer py-2">
                <h6 class="transaction__date">${
                  item.effectiveDate.split("T")[0]
                }</h6>
                <div class="d-flex justify-content-between align-items-center">
                  <div class="transaction__left d-flex flex-column">
                    <div class="transaction__recipient">
                      <span class="fw-bold">${item.recipient}</span>
                    </div>
                    <div class="transaction__name">
                      <span>${item.title}</span>
                    </div>
                  </div>
                  <div class="transaction__right" style="color: ${color}; font-weight: bold">
                    ${item.amount} ${item.currencyCode}
                  </div>
                </div>
              </div><hr/>`);
          }
        }
      }

      incomes = Math.floor(incomes);
      expenses = Math.floor(expenses) * -1;

      $(".bank__account-container")
        .append(`<h6 class="incomes">przychody: <div class="fw-bold" style="display: inline; color: var(--bs-success)">${incomes} PLN</div></h6>
        <h6 class="other_expenses">pozostałe wydatki: <div class="fw-bold" style="display: inline; color: var(--bs-danger)">${expenses} PLN</div><h6>`);

      $(".bank__account-container").append(
        `<span class="refresh material-icons-outlined" data-consentId=${consentId} onClick="getBanqwareData(this)">refresh</span>
        <button type="button" class="mt-2 btn btn-secondary btn-sm credit__profile-data-btn-save">zapisz dane</button>`
      );

      $(".credit__profile-data-btn-save").unbind("click");
      $(".credit__profile-data-btn-save").on("click", () => {
        $(".page .page-section").html(`<div class="container-fluid h-100">
      <div class="d-flex justify-content-center align-items-center" style="height: 100%">
        <div class="spinner-border" style="color: #003574; width: 3rem; height: 3rem;" role="status">
        </div>
        <span class="text-center ms-3">zapisywanie danych profilu kredytowego...</span>
      </div>`);

        (async () => {
          try {
            const response_ = await POST("/profiles/credit/save", {
              data: res,
            });
            const response = await POST("/updateManually", {
              incomes: incomes,
              other_expenses: expenses,
              loa: "high",
              source: "banqware",
            });

            // let cookieHash = readCookie("clipboard");
            // // console.log(cookieHash);
            // if (cookieHash && cookieHash.length > 0) {
            //   const cookiesData = JSON.parse(
            //     decodeURIComponent(
            //       b64DecodeUnicode(decodeURIComponent(cookieHash[1]))
            //     )
            //   );
            //
            //   console.log({ cookiesData });
            // }

            // not found data in clipboard
            let cookieHash = readCookie("clipboard");
            // console.log(cookieHash);
            if (cookieHash && cookieHash.length > 0) {
              const cookiesData = JSON.parse(
                decodeURIComponent(
                  b64DecodeUnicode(decodeURIComponent(cookieHash[1]))
                )
              );

              if (cookiesData.length > 0) {
                await update();
                showToast(
                  "_add-success",
                  '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
                  "aktualizacja danych",
                  `profil kredytowy został zaktualizowany`
                );
              } else {
                setTimeout(() => {
                  $(
                    "nav.sidebar .list-item a[data-name='profiles/credit']"
                  ).click();
                }, 3000);
              }
            } else {
              setTimeout(() => {
                $(
                  "nav.sidebar .list-item a[data-name='profiles/credit']"
                ).click();
              }, 3000);
            }

            // found data in clipboard like: incomes, other_expenses
          } catch (err) {
            $(".page .page-section").html(`<div class="container-fluid h-100">
      <div class="d-flex justify-content-center align-items-center" style="height: 100%">
        </div>
        <span class="text-center ms-3">błąd zapisywania danych, spróbuj ponownie później...</span>
      </div>`);
          }
        })();
      });
    } catch (err) {
      console.log({
        err,
      });
      $(".page .page-section").html(`<div class="container-fluid h-100">
        <div class="d-flex justify-content-center align-items-center" style="height: 100%">
          <span class="text-center ms-3">${language[lang].banqup_data_error}</span>
        </div>`);
    }
  })();
};

var queryString = window.location.search;
console.log(queryString);

if (queryString && queryString != undefined) {
  const urlParams = new URLSearchParams(queryString);
  // const clientId = urlParams.get("clientId");

  if (
    urlParams.has("consentId") &&
    urlParams.has("scope") &&
    urlParams.has("clientId") &&
    urlParams.has("userId")
  ) {
    console.log(`banqware callback... we can download data`);
    const consentId = urlParams.get("consentId");
    console.log(consentId);
    const lang = $("nav.navbar .language__active").data("active-language");
    let newUrl =
      window.location.protocol +
      "//" +
      window.location.host +
      window.location.pathname;

    window.history.pushState(
      {
        path: newUrl,
      },
      "",
      newUrl
    );

    getBanqwareData(consentId);
  } else {
    console.log(`no banqware callback...`);
  }
}

// listeners
var banqp_imgs = $(".banqup__image-bank-logo");

banqp_imgs &&
  $(banqp_imgs).on("click", async (e) => {
    // console.log("clicked ", e.currentTarget);
    const bank_id = $(e.currentTarget).data("bank-id");
    console.log("bank id: ", bank_id);
    const lang = $("nav.navbar .language__active").data("active-language");
    try {
      // const loadingContent = await POST("/loading", "");
      $(".page .page-section").html(`<div class="container-fluid h-100">
        <div class="d-flex justify-content-center align-items-center" style="height: 100%">
          <div class="spinner-border" style="color: #003574; width: 3rem; height: 3rem;" role="status">
          </div>
          <span class="text-center ms-3">${language[lang].banqup_loading}</span>
        </div>`);

      const response = await POST("/api/banqup", {
        bank_id: bank_id,
      });
      const json = JSON.parse(response);
      console.log({
        json,
      });
      window.location.href = json.consent.url;
    } catch (err) {
      console.log({
        err,
      });
      $(".page .page-section").html(`<div class="container-fluid h-100">
        <div class="d-flex justify-content-center align-items-center" style="height: 100%">
          <span class="text-center ms-3">${language[lang].banqup_error}</span>
        </div>`);
    }
  });

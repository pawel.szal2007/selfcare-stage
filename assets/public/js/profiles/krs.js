(() => {
  // const krsButton = $(
  //   ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='krs']"
  // );
  const acceptButton = $(".krs__modal .krs__modal-content .content-button");
  const nameInput = $(
    ".krs__modal .krs__modal-content .content-input[name='krs_name']"
  );
  const surnameInput = $(
    ".krs__modal .krs__modal-content .content-input[name='krs_surname']"
  );
  const numberInput = $(
    ".krs__modal .krs__modal-content .content-input[name='krs_nip']"
  );
  // const krsPersonIdInput = $(
  //   ".krs__modal .krs__modal-content .content-input[name='krs_number']"
  // );
  const closeButton = $(".krs__modal .krs__modal-content .content-close");

  // console.log({ nipInput }, { krsPersonIdInput });

  // $(krsButton).unbind("click");
  // $(krsButton).on("click", async () => {
  //   try {
  //     showCustomModal("krs");
  //   } catch (err) {
  //     // console.log({ err });
  //   }
  // });

  const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
  };

  // console.log(capitalizeFirstLetter('foo')); // Foo

  $(acceptButton).unbind("click");
  $(acceptButton).on("click", async () => {
    const lang = $("nav.navbar .language__active").data("active-language");
    const service = $(".krs__modal-content select option:selected").val();
    let nip, krsPersonId;
    if (service === "nip") {
      nip = $(numberInput).val();
    } else if (service === "krs") {
      krsPersonId = $(numberInput).val();
    }

    let name = $(nameInput).val();
    let surname = $(surnameInput).val();

    console.log({ nip }, { krsPersonId });
    if (name && surname && (krsPersonId || nip)) {
      console.log(
        {
          name,
        },
        {
          surname,
        },
        { service }
      );
      closeCustomModal("krs");
      mainBackgroundView({
        event: "show",
      });
      const text = loadingFullScreen({
        event: "show",
        background: "transparent",
        icon: '<span class="material-icons-outlined">storage</span>',
        text: language[lang].get_data_krs,
      });

      loadingFullScreen({
        event: "hide",
        object: "close_button",
      });
      let data;
      const checkedData = getCheckedDataLabels_Array();
      try {
        const response = await POST("/krs/data", {
          nip: nip,
          krsPersonId: krsPersonId,
          name: capitalizeFirstLetter(name),
          surname: capitalizeFirstLetter(surname),
          checkedData: checkedData,
        });
        // const response = await GET(
        //   `/krs/data?nip=${nip}&krsPersonId=${krsPersonId}&name=${name}&surname=${surname}`
        // );
        // console.log({ response });
        data = JSON.parse(response).data;
        console.log({
          data,
        });
      } catch (err) {
        $(text).html(language[lang].get_data_error);
        // mainBackgroundView({ event: "close" });
        loadingFullScreen({
          event: "visible",
          object: "close_button",
        });
        return;
      }

      loadingFullScreen({
        event: "close",
      });
      mainBackgroundView({
        event: "close",
      });

      dataResultSection(data);
      // const [acceptButton, rejectButton] = userDataView({
      //   event: "show",
      //   background: "transparent",
      //   // source: source,
      //   // loa: loa,
      //   data: data,
      // });
      //
      // $(acceptButton).unbind("click");
      // $(acceptButton).on("click", async () => {
      //   // dataView({ event: "close" });
      //   // console.log("{user accepted data}");
      //   userDataView({ event: "close" });
      //   const text = loadingFullScreen({
      //     event: "show",
      //     background: "transparent",
      //     text: language[lang].updating,
      //   });
      //   loadingFullScreen({ event: "hide", object: "close_button" });
      //   const response = await POST("/publishDataToTopic", data);
      //   // console.log({ response });
      //   setTimeout(async () => {
      //     try {
      //       await update();
      //     } catch (err) {
      //       // console.log({ err });
      //       $(text).html(language[lang].updating_error);
      //     }
      //     mainBackgroundView({ event: "close" });
      //     loadingFullScreen({ event: "close" });
      //   }, 5000);
      // });
      //
      // $(rejectButton).unbind("click");
      // $(rejectButton).on("click", () => {
      //   // dataView({ event: "close" });
      //   // console.log("{user rejected data}");
      //   mainBackgroundView({ event: "close" });
      //   userDataView({ event: "close" });
      // });
    } else {
      // pole było puste trzeba obsluzyc blad ze nie ma nipu
    }
  });

  const dataResultSection = (data) => {
    $(".profile__data-result").html("");
    $(".profile__data-result").append(`<nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">pobrane dane</li>
      </ol>
    </nav>
    <div class="row mb-5">
      <div class="col col-12">
        <div class="mdc-card px-4">
          <div class="mx-2 my-3 accordion accordion-flush" id="accordionDataResult">
          </div>
          <input class="ms-2 mb-3 settings__save" type="button" onClick="krsBusinessProfileSave(this)" value="zapisz">
  </div>
</div>
</div>
    `);

    if (data && data != undefined && data.Organizations.length > 0) {
      $(".settings__save").data("dt", data);
      for (const [index, organization] of data.Organizations.entries()) {
        $("#accordionDataResult").append(`
          <span class="user fw-bold">${data.Profile.given_name}</span>
          <div class="d-flex align-items-center mb-4">
          <input data-organizationid=${organization.Organization.id} class="organization__input-checkbox form-check-input m-0 me-3" type="checkbox" value="" id="flexCheckDefault_${index}">
          <div class="accordion-item w-100">
          <h2 class="accordion-header" id="flush-heading_${index}">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse_${index}" aria-expanded="false" aria-controls="flush-collapse_${index}">
              ${organization.Organization.nazwy.pelna}
            </button>
          </h2>
          <div id="flush-collapse_${index}" class="accordion-collapse collapse" aria-labelledby="flush-heading_${index}" data-bs-parent="#accordionDataResult">
            <div class="accordion-body">
              <ul class="list-group border-0">
                <li class="list-group-item border-0"><span class="fw-bold">adres: </span><span class="value">ul. ${organization.Organization.adres.ulica} ${organization.Organization.adres.kod} ${organization.Organization.adres.poczta}</span></li>
                <li class="list-group-item border-0"><span class="fw-bold">forma prawna: </span><span class="value">${organization.Organization.stan.forma_prawna}</span></li>
                <li class="list-group-item border-0"><span class="fw-bold">dział: </span><span class="value">${organization.Organization.stan.pkd_przewazajace_dzial}</span></li>
              </ul>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb ms-3 mb-0 mt-4">
                  <li class="breadcrumb-item active" aria-current="page">powiązane organizacje</li>
                </ol>
              </nav>
              <div class="ms-3 accordion accordion-flush" id="accordionAssociatedOrganizations">
              </div>
            </div>
          </div>
        </div>
          </div>
          `);

        for (const [i, associatedOrganization] of organization[
          "Associated Organizations"
        ].entries()) {
          $("#accordionAssociatedOrganizations")
            .append(`<div class="accordion-item">
            <h2 class="accordion-header" id="flush-heading_${index}_${i}">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse_${index}_${i}" aria-expanded="false" aria-controls="flush-collapse_${index}_${i}">
                ${associatedOrganization.nazwy.pelna}
              </button>
            </h2>
            <div id="flush-collapse_${index}_${i}" class="accordion-collapse collapse" aria-labelledby="flush-heading_${index}_${i}" data-bs-parent="#accordionAssociatedOrganizations">
              <div class="accordion-body">
                <ul class="list-group border-0">
                  <li class="list-group-item border-0"><span class="fw-bold">adres: </span><span class="value">ul. ${associatedOrganization.adres.ulica} ${associatedOrganization.adres.kod} ${associatedOrganization.adres.poczta}</span></li>
                  <li class="list-group-item border-0"><span class="fw-bold">forma prawna: </span><span class="value">${associatedOrganization.stan.forma_prawna}</span></li>
                  <li class="list-group-item border-0"><span class="fw-bold">dział: </span><span class="value">${associatedOrganization.stan.pkd_przewazajace_dzial}</span></li>
                </ul>
              </div>
            </div>
          </div>`);
        }
      }
    } else {
      $(".settings__save").css("display", "none");
      $("#accordionDataResult").append(
        `<div class="d-flex justify-content-center align-items-center"><span class="coscos">brak danych w rejestrze KRS</span></div>`
      );
    }
  };

  $(closeButton).unbind("click");
  $(closeButton).on("click", () => {
    closeCustomModal("krs");
  });
})();

var krsBusinessProfileSave = async (e) => {
  const data = $(e).data("dt");
  // console.log({ data });

  // filter from checkboxes
  const checkboxes = $(".organization__input-checkbox");
  const idsToRemove = [];
  for (const box of checkboxes) {
    if (!$(box).is(":checked")) {
      idsToRemove.push($(box).data("organizationid"));
    }
  }

  // console.log(`IDS TO REMOVE: ${idsToRemove}`);
  // remove data
  for (const id of idsToRemove) {
    for (const row of data.Organizations) {
      if (idsToRemove.includes(row.Organization.id)) {
        // console.log(`ids found - removing`);
        const index = data.Organizations.indexOf(row);
        if (index > -1) {
          data.Organizations.splice(index, 1); // 2nd parameter means remove one item only
        }
      }
    }
  }

  // console.log(`NEW PARSED DATA`);
  console.log({ data });

  try {
    const response = await POST("/profiles/business/krs/save", { data: data });
    const jsonRes = JSON.parse(response);
    console.log({ jsonRes });

    if (jsonRes.status === "Success") {
      showToast(
        "_add-success",
        '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
        "profil biznesowy KRS",
        `profil biznesowy KRS został zapisany!`
      );
    } else {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "profil biznesowy KRS",
        "wystąpił błąd przy zapisywaniu danych, dane KRS nie zostały zapisane"
      );
    }
  } catch (err) {
    console.log(err);
    showToast(
      "_danger-no-data",
      '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
      "profil biznesowy KRS",
      "wystąpił błąd przy zapisywaniu danych, dane KRS nie zostały zapisane"
    );
  }
  $(".profile__data-result").html("");
};

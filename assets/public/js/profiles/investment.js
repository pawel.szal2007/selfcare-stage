(() => {
  $(document).ready(() => {
    const investmentDataRows = $(
      ".page .page-content .survey__data-row .data-row"
    );
    let currentElement;
    for (const invRow of investmentDataRows) {
      $(invRow).unbind("click");
      $(invRow).on("click", (e) => {
        // console.log("on click.... jest");
        const collapsibleElements = $(
          ".page .page-content .investment__data-collapse"
        );
        for (const element of collapsibleElements) {
          // // console.log($(element).parent().find(".settings__data-label"));
          $(element)
            .parent()
            .parent()
            .find(".investment__data-label")
            .removeClass("settings-active");
          $(element).collapse("hide");
        }
        // // currentElement != e.currentTarget && $(e.currentTarget).parent().addClass('main-background')
        const investmentLabel = $(e.currentTarget).find(
          ".investment__data-label"
        );
        currentElement != e.currentTarget &&
          $(investmentLabel).addClass("settings-active");
        // console.log(e.currentTarget);
        $(e.currentTarget)
          .parent()
          .find(".investment__data-collapse")
          .collapse("show");
        if (currentElement != e.currentTarget) {
          currentElement = e.currentTarget;
        } else {
          currentElement = "";
        }
      });
    }
  });

  const surveyObjects = object;

  let i = 0;
  for (const survey of surveyObjects) {
    const surveyContainer = $(`#${survey.category} .survey_container`);
    // // console.log({surveyContainer});
    if (!surveyContainer) {
      // console.log(`category ${survey.category} not correct. NOT FOUND`);
      continue;
    }

    $(surveyContainer).append(`
      <div class="question__box my-4">
        <p class="question">${survey.question}</p>
        `);

    const questionBox = $(surveyContainer).find(".question__box").last();
    // // console.log({questionBox});
    if (survey.type === "form_check") {
      for (const answer of survey.answers) {
        i++;
        $(questionBox).append(`
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="${i}">
            <label class="form-check-label" for="${i}">${answer}</label>
          </div>
          `);
      }
    } else if (survey.type === "form_select") {
      $(questionBox)
        .append(`<div class="ms-3"><select class="form-select" aria-label="default">
      </select></div>`);
      for (const answer of survey.answers) {
        i++;
        const formSelect = $(questionBox).find(".form-select").last();
        // console.log({ formSelect });
        $(formSelect).append(`<option value="${i}">${answer}</option>`);
      }
    } else {
      // console.log(`type: ${survey.type} is not recognized`);
    }
  }
})();

var saveInvestmentProfile = async () => {
  const obj = {};

  // ankieta
  const question_boxes = $(".question__box");
  for (let box of question_boxes) {
    const question = $(box).find(".question").text();
    if ($(box).find(".form-check-input").length > 0) {
      // robimy pętle po form checkach i zapisujemy w tabeli odpowiedzi
      obj[question] = [];
      const answers = $(box).find(".form-check");
      for (let answer of answers) {
        if ($(answer).find(".form-check-input").is(":checked")) {
          obj[question].push($(answer).find(".form-check-label").html());
        }
      }
    } else if ($(box).find(".form-select").length > 0) {
      // to znaczy ze mamy selecty i dajemy konkretna odpowiedz
      const answer = $(box).find(".form-select option:selected").text();
      obj[question] = answer;
    }
  }

  // dodatkowe pytania
  const assets = $(".asset__container");
  for (let asset of assets) {
    const question = $(asset).find(".asset-label").text();
    const answer = $(asset).find("input").val();
    obj[question] = answer ? answer : "";
  }
  console.log({ obj });

  try {
    const response = await POST("/profiles/investment/save", { data: obj });
    const jsonRes = JSON.parse(response);

    if (jsonRes.status === "Success") {
      showToast(
        "_add-success",
        '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
        "profil inwestycyjny",
        `profil inwestycyjny został zapisany!`
      );
    } else {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "profil inwestycyjny",
        "wystąpił błąd przy zapisywaniu danych, profil inwestycyjny nie został zapisany"
      );
    }
  } catch (err) {
    console.log(err);
    showToast(
      "_danger-no-data",
      '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
      "profil inwestycyjny",
      "wystąpił błąd przy zapisywaniu danych, profil inwestycyjny nie został zapisany"
    );
  }
};

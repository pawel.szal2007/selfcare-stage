$(document).ready(() => {
  // console.log("======== jestes w profilu biznesowym ! ==============");

  $('input[type="checkbox"]').change((e) => {
    console.log($(e.currentTarget).is(":checked"));
    console.log($(e.currentTarget).data("service"));
    const service = $(e.currentTarget).data("service");
    const checked = $(e.currentTarget).is(":checked");
    if (checked) {
      try {
        showCustomModal(service.toLowerCase());
      } catch (err) {
        console.log({ err });
      }
    }
  });
});

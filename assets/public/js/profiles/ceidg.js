(() => {
  // const ceidgButton = $(
  //   ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='ceidg']"
  // );
  const radiosButtons = $(".ceidg__modal .ceidg__modal-content .content-radio");
  const ceidgInput = $(".ceidg__modal .ceidg__modal-content .content-input");
  const acceptButton = $(".ceidg__modal .ceidg__modal-content .content-button");
  const closeButton = $(".ceidg__modal .ceidg__modal-content .content-close");

  $(acceptButton).unbind("click");
  $(acceptButton).on("click", async () => {
    const lang = $("nav.navbar .language__active").data("active-language");
    let radioOption;
    for (const radio of radiosButtons) {
      if ($(radio).is(":checked")) {
        radioOption = $(radio).val();
      }
    }
    const inputValue = $(ceidgInput).val();
    // console.log({ radioOption }, { inputValue });
    if (inputValue && radioOption) {
      closeCustomModal("ceidg");

      mainBackgroundView({ event: "show" });

      const text = loadingFullScreen({
        event: "show",
        background: "transparent",
        icon: '<span class="material-icons-outlined">storage</span>',
        text: language[lang].get_data_ceidg,
      });
      loadingFullScreen({ event: "hide", object: "close_button" });
      let data;
      const checkedData = getCheckedDataLabels_Array();
      try {
        let response;
        if (radioOption === "nip") {
          response = await POST(`/ceidg/data`, {
            checkedData: checkedData,
            nip: inputValue,
          });
        } else if (radioOption === "regon") {
          response = await POST(`/ceidg/data`, {
            checkedData: checkedData,
            regon: inputValue,
          });
        } else {
          console.log("radio option ma wartosc: ", { radioOption });
        }

        // const response = await GET(`/ceidg/data?${radioOption}=${inputValue}`);
        // console.log({ response });
        data = JSON.parse(response).data;
        console.log({ data });
      } catch (err) {
        $(text).html(language[lang].get_data_error);
        // mainBackgroundView({ event: "close" });
        loadingFullScreen({ event: "visible", object: "close_button" });
        return;
      }
      loadingFullScreen({ event: "close" });
      mainBackgroundView({ event: "close" });
      accordionDataResultCEIDG(data);
    } else {
      // console.log("error: no input value or no radio option passed...");
    }
  });

  const accordionDataResultCEIDG = (data) => {
    $(".profile__data-result").html("");
    $(".profile__data-result").append(`<nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">pobrane dane</li>
      </ol>
    </nav>
    <div class="row mb-5">
      <div class="col col-12">
        <div class="mdc-card px-4">
          <div class="mx-2 my-3 accordion accordion-flush" id="accordionDataResult">
    </div>
    <input class="ms-2 mb-3 settings__save" type="button" onClick="ceidgBusinessProfileSave(this)" value="zapisz">
  </div>
</div>
</div>
    `);

    if (data && data != undefined) {
      $(".settings__save").data("dt", data);
      // for (const [
      //   index,
      //   row,
      // ] of data.wynikWyszukiwania.informacjaOWpisie.entries()) {
      for (let [i, [key, value]] of Object.entries(data).entries()) {
        if (
          typeof value === "object" &&
          value != null &&
          !Array.isArray(value)
        ) {
          console.log(`jest obiekt dla wartosci ${key}`);
          // trzeba utworzyc accordion i go przeleciec
          $("#accordionDataResult").append(`
              <div class="ms-4 accordion-item">
              <h2 class="accordion-header" id="flush-heading__${i}">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse__${i}" aria-expanded="false" aria-controls="flush-collapse__${i}">
                  ${parseCEIDGKeyName(key)}
                </button>
              </h2>
              <div id="flush-collapse__${i}" class="accordion-collapse collapse" aria-labelledby="flush-heading__${i}" data-bs-parent="#accordionDataResult">
                <div class="accordion-body">
                </div>
              </div>
            </div>`);

          // teraz w accordion body listujemy elementy ale jak trafimy na obiekt to znowu tworzymy nowy accordion
          for (let [i_, [key_, value_]] of Object.entries(value).entries()) {
            if (
              typeof value_ === "object" &&
              value_ != null &&
              !Array.isArray(value_)
            ) {
              // inny parent accordion? jesli nie ma
              if (
                $(`#flush-collapse__${i} .accordion-body #accordionDataResult_`)
                  .length === 0
              ) {
                $(`#flush-collapse__${i} .accordion-body`)
                  .append(`<div class="ms-4 accordion accordion-flush" id="accordionDataResult_">
              </div>`);
              }

              $(`#flush-collapse__${i} .accordion-body #accordionDataResult_`)
                .append(`<div class="ms-4 accordion-item">
                <h2 class="accordion-header" id="flush-heading__${i}_${i_}">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse__${i}_${i_}" aria-expanded="false" aria-controls="flush-collapse__${i}_${i_}">
                    ${parseCEIDGKeyName(key_)}
                  </button>
                </h2>
                <div id="flush-collapse__${i}_${i_}" class="accordion-collapse collapse" aria-labelledby="flush-heading__${i}_${i_}" data-bs-parent="#accordionDataResult_">
                  <div class="accordion-body">
                  </div>
                </div>
              </div>`);

              // zagnieżdżenie
              for (let [i__, [key__, value__]] of Object.entries(
                value_
              ).entries()) {
                if (
                  typeof value__ === "object" &&
                  value__ != null &&
                  !Array.isArray(value__)
                ) {
                  if (
                    $(
                      `#flush-collapse__${i}_${i_} .accordion-body #accordionDataResult__`
                    ).length === 0
                  ) {
                    $(`#flush-collapse__${i}_${i_} .accordion-body`)
                      .append(`<div class="ms-4 accordion accordion-flush" id="accordionDataResult__">
                  </div>`);
                  }

                  $(`#flush-collapse__${i}_${i_} .accordion-body`)
                    .append(`<div class="ms-4 accordion-item">
                    <h2 class="accordion-header" id="flush-heading__${i}_${i_}_${i__}">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse__${i}_${i_}_${i__}" aria-expanded="false" aria-controls="flush-collapse__${i}_${i_}_${i__}">
                        ${parseCEIDGKeyName(key__)}
                      </button>
                    </h2>
                    <div id="flush-collapse__${i}_${i_}_${i__}" class="accordion-collapse collapse" aria-labelledby="flush-heading__${i}_${i_}_${i__}" data-bs-parent="#accordionDataResult__">
                      <div class="accordion-body">
                      </div>
                    </div>
                  </div>`);

                  // dodajemy item bez zagniezdzen
                  for (let [i___, [key___, value___]] of Object.entries(
                    value__
                  ).entries()) {
                    // const attr = parseCEIDGKeyName(key___);
                    $(`#flush-collapse__${i}_${i_}_${i__} .accordion-body`)
                      .append(`<ul class="list-group border-0">
                      <li class="list-group-item border-0"><span class="fw-bold">${parseCEIDGKeyName(
                        key___
                      )}: </span><span class="value">${value___}</span></li>
                  </ul>`);
                  }
                } else {
                  // dodajemy item
                  if (value__ != null && !Array.isArray(value__)) {
                    // const attr = parseCEIDGKeyName(key__);
                    $(`#flush-collapse_${index}_${i}_${i_} .accordion-body`)
                      .append(`<ul class="ms-4 list-group border-0">
                          <li class="list-group-item border-0"><span class="fw-bold">${parseCEIDGKeyName(
                            key__
                          )}: </span><span class="value">${value__}</span></li>
                      </ul>`);
                  }
                }
              }
            } else {
              // dodajmy normalne pole z wartoscia
              if (value_ != null && !Array.isArray(value_)) {
                // const attr = parseCEIDGKeyName(key_);
                $(`#flush-collapse__${i} .accordion-body`)
                  .append(`<ul class="ms-4 list-group border-0">
                      <li class="list-group-item border-0"><span class="fw-bold">${parseCEIDGKeyName(
                        key_
                      )}: </span><span class="value">${value_}</span></li>
                  </ul>`);
              }
            }
          }
        } else {
          // dodajemy normalne pole z wartoscia
          if (value != null && !Array.isArray(value)) {
            // const attr = parseCEIDGKeyName(key);
            // console.log({ attr });
            $("#accordionDataResult").append(`<ul class="list-group border-0">
                <li class="list-group-item border-0"><span class="fw-bold">${parseCEIDGKeyName(
                  key
                )}: </span><span class="value">${value}</span></li>
            </ul>`);
          }
        }
      }
      // }
    } else {
      $(".settings__save").css("display", "none");
      $("#accordionDataResult").append(
        `<div class="d-flex justify-content-center align-items-center"><span class="coscos">brak danych w rejestrze CEIDG</span></div>`
      );
    }

    //   <ul class="list-group border-0">
    //     <li class="list-group-item border-0"><span class="fw-bold">adres: </span><span class="value">ul. ${organization.Organization.adres.ulica} ${organization.Organization.adres.kod} ${organization.Organization.adres.poczta}</span></li>
    //     <li class="list-group-item border-0"><span class="fw-bold">forma prawna: </span><span class="value">${organization.Organization.stan.forma_prawna}</span></li>
    //     <li class="list-group-item border-0"><span class="fw-bold">dział: </span><span class="value">${organization.Organization.stan.pkd_przewazajace_dzial}</span></li>
    //   </ul>
    //
    // $("#accordionDataResult").append(`
    //   <span class="user fw-bold">${data.Profile.given_name}</span>
    //   <div class="accordion-item">
    //   <h2 class="accordion-header" id="flush-heading_${index}">
    //     <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse_${index}" aria-expanded="false" aria-controls="flush-collapse_${index}">
    //       ${organization.Organization.nazwy.pelna}
    //     </button>
    //   </h2>
    //   <div id="flush-collapse_${index}" class="accordion-collapse collapse" aria-labelledby="flush-heading_${index}" data-bs-parent="#accordionDataResult">
    //     <div class="accordion-body">
    //       <ul class="list-group border-0">
    //         <li class="list-group-item border-0"><span class="fw-bold">adres: </span><span class="value">ul. ${organization.Organization.adres.ulica} ${organization.Organization.adres.kod} ${organization.Organization.adres.poczta}</span></li>
    //         <li class="list-group-item border-0"><span class="fw-bold">forma prawna: </span><span class="value">${organization.Organization.stan.forma_prawna}</span></li>
    //         <li class="list-group-item border-0"><span class="fw-bold">dział: </span><span class="value">${organization.Organization.stan.pkd_przewazajace_dzial}</span></li>
    //       </ul>
    //       <nav aria-label="breadcrumb">
    //         <ol class="breadcrumb ms-3 mb-0 mt-4">
    //           <li class="breadcrumb-item active" aria-current="page">powiązane organizacje</li>
    //         </ol>
    //       </nav>
    //       <div class="ms-3 accordion accordion-flush" id="accordionAssociatedOrganizations">
    //       </div>
    //     </div>
    //   </div>
    // </div>`);
  };

  $(closeButton).unbind("click");
  $(closeButton).on("click", () => {
    closeCustomModal("ceidg");
  });
})();

var ceidgBusinessProfileSave = async (e) => {
  const data = $(e).data("dt");
  console.log({ data });

  try {
    const response = await POST("/profiles/business/ceidg/save", {
      data: data,
    });
    const jsonRes = JSON.parse(response);
    console.log({ jsonRes });

    if (jsonRes.status === "Success") {
      showToast(
        "_add-success",
        '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
        "profil biznesowy CEIDG",
        `profil biznesowy CEIDG został zapisany!`
      );
    } else {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "profil biznesowy CEIDG",
        "wystąpił błąd przy zapisywaniu danych, dane CEIDG nie zostały zapisane"
      );
    }
  } catch (err) {
    console.log(err);
    showToast(
      "_danger-no-data",
      '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
      "profil biznesowy CEIDG",
      "wystąpił błąd przy zapisywaniu danych, dane CEIDG nie zostały zapisane"
    );
  }
  $(".profile__data-result").html("");
};

var parseCEIDGKeyName = (word) => {
  let word_new = [];
  let pointer = 0;
  for (let i = 0; i < word.length; i++) {
    if (word.charAt(i) === word.charAt(i).toUpperCase()) {
      word_new.push(word.substring(pointer, i).toLowerCase());
      pointer = i;
    } else if (i === word.length - 1) {
      word_new.push(word.substring(pointer, i + 1).toLowerCase());
    }
  }
  return word_new.length === 0 ? word : word_new.join(" ");
};

exports.map = (topicData, userCheckedData) => {
  const o = {
    topicName: topicData.topicName,
    data: {}
  }
  // o.data['timestamp'] = topicData.data.timestamp
  // o.data["user_uid"] = topicData.data.user_uid
  // o.data['method'] = topicData.data.method
  // o.data['loa'] = topicData.data.loa

  o.data = topicData.data

  if(!userCheckedData.includes('identity_card_number')) { o.data['identity_card_number'] = ' ' }
  if(!userCheckedData.includes('date_of_issue')) { o.data['date_of_issue'] = ' ' }
  if(!userCheckedData.includes('expiry_date')) { o.data['expiry_date'] = ' ' }
  if(!userCheckedData.includes('issuing_authority')) { o.data['issuing_authority'] = ' ' }
  if(!userCheckedData.includes('user_img')) { o.data['user_img'] = ' ' }
  if(!userCheckedData.includes('personal_number')) { o.data['personal_number'] = ' ' }
  if(!userCheckedData.includes('surname')) { o.data['surname'] = ' ' }
  if(!userCheckedData.includes('family_name')) { o.data['family_name'] = ' ' }
  if(!userCheckedData.includes('given_names')) { o.data['given_names'] = ' ' }
  if(!userCheckedData.includes('sex')) { o.data['sex'] = ' ' }  o.data['sex']
  if(!userCheckedData.includes('date_of_birth')) { o.data['date_of_birth'] = ' ' }

  return o
}

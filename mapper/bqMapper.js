/*
 * function to convert data from some source (eg facebook, linkedIn, KRS etc...) to bigquery table schema
 *  input: obj with attributes -> obj.userID, obj.data, obj.checkedData, obj.source, obj.loa
 * checkedData -> data that user choose on frontend clipboard data manager
 */
exports.convertToBigQuerySchema = (obj) => {
  // console.log("convert to bigquery data...");
  const userId = obj.userId;
  const data = obj.data;
  const checkedData = obj.checkedData;
  const source = obj.data.source || "";
  const loa = obj.data.loa || obj.data.Loa || "";

  const o = {};

  const scopes = [
    {
      topicName: "topic-user-profile",
      name: "profile",
      label: ["locale", "zoneinfo", "name", "given_name", "preferred_username"],
    },
    {
      topicName: "topic-user-credit-profile",
      name: "credit_profile",
      label: ["incomes", "monthly_income", "fixed_charges", "other_expenses"],
    },
    {
      topicName: "topic-user-profile-extended",
      name: "profile_extended",
      label: [
        "picture",
        "gender",
        "date_of_birth",
        "education",
        "job_title",
        "website",
        "profile_url",
        "nickname",
      ],
    },
    {
      topicName: "topic-user-identity",
      name: "identity",
      label: [
        "nationality",
        "document_name",
        "issued_country",
        "street",
        "city",
        "zip",
        "country",
        "document_number",
        "issued_date",
        "valid_date",
        "issuer_name",
        "national_id",
        "family_name",
        "middle_name",
      ],
    },
    {
      topicName: "topic-user-shopping",
      name: "shopping",
      label: [
        "height",
        "weight",
        "clothing_size",
        "shoe_size",
        "delivery_method",
        "address",
        "pickup_address",
        "payment_type",
      ],
    },
  ];

  for (const scope of scopes) {
    const scopeName = scope.name;
    const topicName = scope.topicName;
    const scopeLabels = scope.label;
    o[scopeName] = {
      topicName: topicName,
      data: {},
    };

    // constant data for all scopes
    o[scopeName].data["timestamp"] = parseInt(
      Math.floor(Date.now() / 1000),
      10
    );
    o[scopeName].data["user_id"] = userId || "unknown";
    o[scopeName].data["source"] = source || "unknown";
    o[scopeName].data["loa"] = loa || "unknown";
    o[scopeName].data["updated_at"] = parseInt(
      Math.floor(Date.now() / 1000),
      10
    );

    for (const label of scopeLabels) {
      // sprawdzenie czy pole to: date_of_birth, issued_date, valid_date -> wtedy konwersja na timestamp
      if (
        typeof data[label] != "object" &&
        data[label] &&
        (checkedData === "all" || checkedData.includes(label))
      ) {
        if (
          label === "date_of_birth" ||
          label === "issued_date" ||
          label === "valid_date"
        ) {
          // format DD/MM/YYY or DD.MM.YYYY
          const dateString = data[label];

          const dateParts = dateString.includes("/")
            ? dateString.split("/")
            : dateString.split(".");

          // month is 0-based, that's why we need dataParts[1] - 1
          const timestamp =
            parseInt(
              new Date(
                +dateParts[2],
                dateParts[1] - 1,
                +dateParts[0]
              ).getTime() / 1000
            ) || null;

          o[scopeName].data[label] = timestamp;
        } else {
          o[scopeName].data[label] = data[label];
        }
      } else {
        o[scopeName].data[label] = null;
      }
    }
  }

  let i = 0;
  for (const scope in o) {
    for (const key in o[scope].data) {
      if (
        key != "timestamp" &&
        key != "user_id" &&
        key != "source" &&
        key != "loa" &&
        key != "updated_at"
      ) {
        if (
          o[scope].data[key] != "" &&
          o[scope].data[key] != " " &&
          o[scope].data[key] != null
        ) {
          i++;
          break;
        }
      }
    }
    if (i === 0) delete o[scope];
    i = 0;
  }

  return o;
};

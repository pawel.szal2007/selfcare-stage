const Storage = require("@google-cloud/storage");
const storage = new Storage.Storage();
const path = require("path");
let env = require("dotenv").config();
const dotenvParseVariables = require("dotenv-parse-variables");
env = dotenvParseVariables(env.parsed);
const Logger = require("../winston-config");
// {keyFileName: "key.json"}

const bucketDestinationName = env.GCP_STORAGE_BUCKET;

// exports.getGOVRDOData = (userID) => {
//   return new Promise(async (resolve, reject) => {
//     try {
//       let upToDateObject;
//
//       const [files] = await storage.bucket(`gs://${bucketDestinationName}/`).getFiles({
//         prefix: `${userID}/scraping/rdo` //tu moze jakeis ID uzytkownika zamiast pawel_s, najlepiej przekazywane z tokenu jakies id
//       });
//
//       files.length > 0 ? console.log("Pobieram dane...") : resolve(upToDateObject);
//       let objects = [];
//
//       for await (const file of files) {
//         const fileName = file.name;
//         if (fileName.split('.').pop() === "json") {
//           console.log('Downloading file ' + fileName);
//           const response = await downloadStorageFile(fileName);
//           objects.push(response);
//         }
//       }
//
//       const govRDOData = joinRowsData(objects);
//
//       // let miliseconds = 0;
//       // console.log({
//       //   objects
//       // });
//       // if (objects) {
//       //   for (const object of objects) {
//       //     const date = new Date(object.timestamp).getTime();
//       //     date > miliseconds && ((upToDateObject = object) && (miliseconds = date));
//       //   }
//       // }
//       // console.log({
//       //   upToDateObject
//       // });
//       resolve(govRDOData);
//     } catch (err) {
//       reject(err);
//     }
//   });
// }
//
// const downloadStorageFile = (fileName) => {
//   return new Promise(async (resolve) => {
//     var download = await storage.bucket(`gs://${bucketDestinationName}/`).file(fileName).createReadStream();
//     var buf = '';
//     await download.on('data', (d) => {
//       buf += d;
//     }).on('end', async () => {
//       console.log('Done downloading file....');
//       const response = JSON.parse(buf);
//       resolve(response);
//     });
//   });
// }

exports.getUserFiles = (userID) => {
  return new Promise(async (resolve, reject) => {
    try {
      const obj = [];
      const [files] = await storage
        .bucket(`gs://${bucketDestinationName}/`)
        .getFiles({
          prefix: `${userID}/json_data/`, //tu moze jakeis ID uzytkownika zamiast pawel_s, najlepiej przekazywane z tokenu jakies id
        });
      // console.log({ files });
      if (files && files.length > 0) {
        for (const file of files) {
          let loa, file_url, source;

          // source of data
          source = file.name.split("/")[2];

          // loa of data based on source
          if (
            source === "gov_rdo" ||
            source === "gov_nl" ||
            source === "gov_pesel"
          ) {
            loa = "high";
          } else if (
            source === "investment_profile" ||
            source === "business_profile" ||
            source === "credit_profile"
          ) {
            loa = "-";
          } else loa = "medium";

          // add object
          obj.push({
            loa: loa,
            path: file.name,
            source: source,
            updated_at: file.metadata.timeCreated,
          });
        }
      }
      resolve(obj);
    } catch (err) {
      reject(err);
    }
  });
};

exports.uploadJSONData = (userId, service, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const today = new Date();
      const ms = today.getTime();
      let buff = new Buffer(`${service}_${today}_${ms}`);
      let name = buff.toString("base64");
      const path = `${userId}/json_data/${service}`;

      await storage
        .bucket(`gs://${bucketDestinationName}/`, {
          // user id a potem sciezka z jsonami i źródłem odpowiednim
          destination: path,
        })
        .file(`${path}/${name}.json`)
        .save(JSON.stringify(data), {
          timestamp: ms,
        });
      resolve(encodeURI(`${path}/${name}.json`));
    } catch (err) {
      reject(err);
    }
  });
};

exports.uploadFileToBucket = (userID, service, file) => {
  return new Promise(async (resolve, reject) => {
    Logger.info("[UPLOAD FILE TO STORAGE BUCKET]", {
      file,
    });
    await storage
      .bucket(`gs://${bucketDestinationName}/`)
      .upload(file.tempFilePath, {
        destination: `${userID}/${service}/${file.type}/${file.name}`,
      })
      .then(() => {
        Logger.info(
          `${file.tempFilePath} uploaded to gs://${bucketDestinationName}/`
        );
        resolve({
          bucketPath: `${userID}/${service}/${file.type}/${file.name}`,
          bucketName: bucketDestinationName,
        });
      });
  });
};

exports.uploadUserImage = (userID, file) => {
  return new Promise(async (resolve, reject) => {
    Logger.info("[UPLOAD FILE TO STORAGE BUCKET]", {
      file,
    });
    await storage
      .bucket(`gs://${bucketDestinationName}/`)
      .upload(file.tempFilePath, {
        destination: `${userID}/images/${file.name}`,
      })
      .then(() => {
        Logger.info(
          `${file.tempFilePath} uploaded to gs://${bucketDestinationName}/`
        );
        resolve({
          bucketPath: `${userID}/images/${file.name}`,
          bucketName: bucketDestinationName,
        });
      });
  });
};

exports.downloadFile = (filePath) => {
  return new Promise(async (resolve, reject) => {
    const filePathSplit = filePath?.split("/");
    let destFileFolder;
    if (filePathSplit) {
      destFileFolder = path.join(
        __dirname,
        "..",
        "assets",
        "public",
        "gcs_files",
        filePathSplit[0]
      );
      const fs = require("fs");
      // check if directory exists
      if (!fs.existsSync(destFileFolder)) {
        fs.mkdirSync(destFileFolder);
      }
    }

    try {
      const destFileName = `${destFileFolder}/${
        filePathSplit[filePathSplit.length - 1]
      }`;
      const options = {
        destination: destFileName,
      };

      await storage
        .bucket(bucketDestinationName)
        .file(filePath)
        .download(options);
      // console.log(`gs://${bucket}/${filePath} downloaded to ${destFileName}.`);
      resolve(destFileName);
    } catch (err) {
      // console.log(err);
      reject(err);
    }
  });
};

exports.deleteFile = (path) => {
  return new Promise(async (resolve, reject) => {
    try {
      await storage.bucket(bucketDestinationName).file(path).delete();
      Logger.info(`gs://${bucketDestinationName}/${path} deleted`);
      resolve();
    } catch (err) {
      reject(err);
    }
  });
};

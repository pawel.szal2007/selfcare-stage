// =====================================================================================

const { BigQuery } = require("@google-cloud/bigquery");
const bigquery = new BigQuery();

let env = require("dotenv").config();
const dotenvParseVariables = require("dotenv-parse-variables");
env = dotenvParseVariables(env.parsed);
const Logger = require("../winston-config");

exports.getTableColumns = (scope) => {
  return new Promise(async (resolve, reject) => {
    const query = `SELECT * FROM users.INFORMATION_SCHEMA.COLUMNS WHERE table_name="user_${scope}"`;
    Logger.info("getting table columns from bigquery");
    const options = {
      query: query,
    };
    const [job] = await bigquery.createQueryJob(options);
    Logger.info(`Job ${job.id} started.`);
    // Wait for the query to finish
    const [rows] = await job.getQueryResults();

    let col_arr = [];
    if (rows) {
      for (const row of rows) {
        if (
          row.column_name != "timestamp" &&
          row.column_name != "user_id" &&
          row.column_name != "updated_at" &&
          row.column_name != "loa" &&
          row.column_name != "source"
        ) {
          col_arr.push(row.column_name);
        }
      }
    }

    col_arr.length > 0
      ? resolve(col_arr)
      : reject({
          error: 404,
          details: "scope table not found, no data found",
        });
  });
};

// if we have all columns name (array) for scope and data that user already has, we can create all scope data with empty values
// exports.createScopeData = (scope_names_arr, data) => {
//   const obj = {};
//   for (const name of scope_names_arr) {
//     obj[name] = {
//       updated_at: data && data[name] ? data[name].updated_at : "",
//       value: data && data[name] ? data[name].value : "",
//       loa: data && data[name] ? data[name].loa : "",
//       source: data && data[name] ? data[name].source : "",
//     };
//   }
//   return obj;
// };

// get up to date one label record from table... or tables
exports.getLabel = (userId, label, table = "all") => {
  return new Promise(async (resolve, reject) => {
    const scopes = env.BIGQUERY_SCOPES;
    const tables = env.BIGQUERY_SCOPE_TABLES;
    const td = tables[scopes.indexOf(table)];
    if (table != "all") {
      if (td) {
        const query = `SELECT ${label} FROM (SELECT timestamp, ${label}
      , RANK() OVER(ORDER BY timestamp DESC) rank
 FROM ${td} WHERE user_id="${userId}") WHERE ${label}!="" ORDER BY rank`;
        Logger.info("getting label from bigquery");
        const options = {
          query: query,
        };
        // Run the query as a job
        const [job] = await bigquery.createQueryJob(options);
        Logger.info(`Job ${job.id} started.`);
        // Wait for the query to finish
        const [row] = await job.getQueryResults();
        // console.log({
        //   row,
        // });
        resolve(row);
      } else
        reject(
          `table name: '${table}' doesn't exist in configuration file....`
        );
    } else {
      // searching all tables to find the proper label...
      const scopes = env.BIGQUERY_SCOPES;
      let obj = [];
      for await (let scope of scopes) {
        const query = `SELECT ${label} FROM (SELECT timestamp, ${label}
      , RANK() OVER(ORDER BY timestamp DESC) rank
 FROM ${td} WHERE user_id="${userId}") WHERE ${label}!="" ORDER BY rank`;
        const options = {
          query: query,
        };
        const [job] = await bigquery.createQueryJob(options);
        Logger.info(`Job ${job.id} started.`);
        // Wait for the query to finish
        const [rows] = await job.getQueryResults();
        rows && (obj = obj.concat(rows));
      }
      // console.log({
      //   obj,
      // });
      resolve(obj);
    }
  });
};

// limit do 1000 wyników ostatnich... zrobic sortowanie po dacie
exports.getAllData = (userId) => {
  return new Promise(async (resolve) => {
    const scopes = env.BIGQUERY_SCOPES;
    const tables = env.BIGQUERY_SCOPE_TABLES;
    let data_obj = [];
    for await (const scope of scopes) {
      const query = `SELECT * FROM ${
        tables[scopes.indexOf(scope)]
      } WHERE user_id='${userId}' ORDER BY timestamp DESC LIMIT 500`;
      Logger.info("getting all data user data from bigquery...");
      const options = {
        query: query,
      };
      // Run the query as a job
      const [job] = await bigquery.createQueryJob(options);
      Logger.info(`Job ${job.id} started.`);
      // Wait for the query to finish
      const [rows] = await job.getQueryResults();
      // console.log("ALL DATA FOR SCOPE: ", `${scope}`);
      // console.log({ rows });
      data_obj = [...data_obj, ...rows];
    }
    // console.log({ data_obj });
    resolve(data_obj);
  });
};

exports.selectUserScope = (userId, scope) => {
  return new Promise(async (resolve, reject) => {
    try {
      const scopes = env.BIGQUERY_SCOPES;
      const tables = env.BIGQUERY_SCOPE_TABLES;
      let data_obj = [];
      // for await (const scope of scopes) {
      let query = `SELECT * FROM ${
        tables[scopes.indexOf(scope.toUpperCase())]
      } WHERE user_id='${userId}'`;

      const scope_labels = env[scope.toUpperCase()];
      // Logger.info({ scope_labels });
      for (let [index, label] of scope_labels.entries()) {
        if (index === 0) {
          query += ` AND `;
        }

        query += `timestamp = (SELECT MAX(timestamp) FROM ${
          tables[scopes.indexOf(scope.toUpperCase())]
        } WHERE user_id='${userId}' AND ${label} IS NOT NULL)`;

        if (index != scope_labels.length - 1) {
          query += ` OR `;
        } else {
          query += `;`;
        }
      }

      // Logger.info({ query });
      Logger.info("selecting user scope data from bigquery...");
      const options = {
        query: query,
      };
      // Run the query as a job
      const [job] = await bigquery.createQueryJob(options);
      Logger.info(`Job ${job.id} started.`);
      // Wait for the query to finish
      const [rows] = await job.getQueryResults();
      // console.log("ALL DATA FOR SCOPE: ", `${scope}`);
      // console.log({ rows });
      let bq_data = [];
      rows.length > 0 && bq_data.push(joinRowsData(rows));
      data_obj = [...data_obj, ...bq_data];
      // }
      // console.log({ data_obj });
      resolve(data_obj);
    } catch (err) {
      reject(err);
    }
  });
};

exports.selectUpToDateAllData = (userID) => {
  return new Promise(async (resolve) => {
    const scopes = env.BIGQUERY_SCOPES;
    // const tables = require("../configuration").projectVariable
    //   .BIGQUERY_SCOPE_TABLES;
    let data_obj = {};
    for await (const scope of scopes) {
      const upToDateRow = await selectUserScopeBigQueryData(userID, scope);
      if (upToDateRow)
        data_obj = {
          ...data_obj,
          ...upToDateRow,
        };

      // const query = `SELECT * FROM ${tables[scope]} WHERE user_id='${userID}'`;
      // console.log({ query });
      // const options = {
      //   query: query,
      // };
      // // Run the query as a job
      // const [job] = await bigquery.createQueryJob(options);
      // console.log(`Job ${job.id} started.`);
      // // Wait for the query to finish
      // const [rows] = await job.getQueryResults();
      // if (rows.length > 0) {
      //   const upToDateRow = joinRowsData(rows);
      //   data_obj = { ...data_obj, ...upToDateRow };
      // }
    }
    resolve(data_obj);
  });
};

// cyber-id-prod.users.user_id_ocr
const selectUserScopeBigQueryData = (userId, scope_table) => {
  return new Promise(async (resolve, reject) => {
    // console.log("selectUserScopeBigQueryData...");
    // const object_arr = [];
    const scopes = env.BIGQUERY_SCOPES;
    const tables = env.BIGQUERY_SCOPE_TABLES;

    // for await (const table of table_arr) {
    let query = `SELECT * FROM ${
      tables[scopes.indexOf(scope_table.toUpperCase())]
    } WHERE user_id='${userId}'`;

    const scope_labels = env[scope_table.toUpperCase()];
    // Logger.info({ scope_labels });
    for (let [index, label] of scope_labels.entries()) {
      if (index === 0) {
        query += ` AND `;
      }

      query += `timestamp = (SELECT MAX(timestamp) FROM ${
        tables[scopes.indexOf(scope_table.toUpperCase())]
      } WHERE user_id='${userId}' AND ${label} IS NOT NULL)`;

      if (index != scope_labels.length - 1) {
        query += ` OR `;
      } else {
        query += `;`;
      }
    }

    // Logger.info({ query });
    Logger.info("selecting user scope data from bigquery...");

    const options = {
      query: query,
    };
    // Run the query as a job
    try {
      const [job] = await bigquery.createQueryJob(options);
      Logger.info(`Job ${job.id} started.`);
      // Wait for the query to finish
      const [rows] = await job.getQueryResults();
      // we have few rows and need to parse to one row with the most actual data
      let bq_data;
      rows.length > 0 && (bq_data = joinRowsData(rows));
      // object_arr.push(joinedRows);
      // }
      // const bq_data = await joinData(object_arr);
      resolve(bq_data);
    } catch (err) {
      reject(err);
    }
  });
};

const joinRowsData = (data) => {
  const o = {};
  const firstItem = data[0];
  for (const property in firstItem) {
    if (
      property != "timestamp" &&
      property != "user_id" &&
      property != "loa" &&
      property != "source" &&
      property != "updated_at" &&
      firstItem[property] != " " &&
      firstItem[property] != "" &&
      firstItem[property] != null
    ) {
      o[property] = {};
      // if (typeof firstItem.timestamp === 'object') {
      // } else o[property]['updated_at'] = firstItem.timestamp;
      o[property]["updated_at"] = new Date(firstItem.timestamp.value).getTime();
      o[property]["value"] = firstItem[property];
      o[property]["loa"] = firstItem.loa;
      o[property]["source"] = firstItem.source;
      // DO PORPAWY BO NA RAZIE W BAZIE NIE MA JAKIE JEST LOA DLA TEJ DANEJ
      // if (firstItem.loa != undefined) {
      //   o[property]['loa'] = firstItem.loa;
      // } else o[property]['loa'] = 'medium';
      //
      // if (firstItem.source != undefined) {
      //   o[property]['source'] = firstItem.source;
      // } else o[property]['source'] = 'skan dokumentu';
    }
  }

  for (let i = 1; i < data.length; i++) {
    if (data[i] != "undefined") {
      const rowData = data[i];
      for (const property in rowData) {
        if (
          property != "timestamp" &&
          property != "user_id" &&
          property != "loa" &&
          property != "source" &&
          property != "updated_at" &&
          rowData[property] != " " &&
          rowData[property] != "" &&
          rowData[property] != null
        ) {
          const rowTimestamp = new Date(rowData.timestamp.value).getTime();
          // if (typeof rowData.timestamp === 'object') {
          // rowTimestamp =
          // } else rowTimestamp = rowData.timestamp;
          if (o[property] != undefined) {
            if (o[property].updated_at < rowTimestamp) {
              o[property].updated_at = new Date(
                rowData.timestamp.value
              ).getTime();
              o[property].value = rowData[property];
              o[property].loa = rowData.loa;
              o[property].source = rowData.source;
              // if (typeof rowData.timestamp === 'object') {
              // } else o[property]['updated_at'] = rowData.timestamp;
              // if (rowData.loa != undefined) {
              // } else o[property]['loa'] = 'medium';
              // if (rowData.source != undefined) {
              // } else o[property]['source'] = 'skan dokumentu';
            }
          } else {
            o[property] = {};
            // if (typeof rowData.timestamp === 'object') {
            o[property].updated_at = new Date(
              rowData.timestamp.value
            ).getTime();
            o[property].value = rowData[property];
            o[property].loa = rowData.loa;
            o[property]["source"] = rowData.source;
            // } else o[property]['updated_at'] = rowData.timestamp;
            // DO PORPAWY BO NA RAZIE W BAZIE NIE MA JAKIE JEST LOA DLA TEJ DANEJ
            // if (rowData.loa != undefined) {
            // } else o[property]['loa'] = 'medium';
            // if (rowData.source != undefined) {
            // } else o[property]['source'] = 'skan dokumentu';
          }
        }
      }
    }
  }
  // console.log("========= NEW JOINED OBJECT ===========");
  // console.log({
  //   o,
  // });
  return o;
};

const joinData = async (data_arr) => {
  return new Promise((resolve) => {
    // console.log({
    //   data_arr,
    // });
    let obj = {};
    for (let i = 0; i < data_arr.length; i++) {
      const data = data_arr[i];
      for (const property in data) {
        if (obj[property] != undefined) {
          if (obj[property].updated_at < data[property].updated_at) {
            obj[property] = data[property];
          }
        } else {
          obj[property] = data[property];
        }
      }
    }
    // console.log("============= FINAL OBJECT ===============");
    // console.log({
    //   obj,
    // });
    resolve(obj);
  });
};

// Imports the Google Cloud client library
const { PubSub } = require("@google-cloud/pubsub");
const pubSubClient = new PubSub();

exports.publishToTopic = (userID, topic) => {
  return new Promise(async (resolve, reject) => {
    const topicName = topic.topicName;
    const dataBuffer = Buffer.from(JSON.stringify(topic.data));
    // console.log(topic.data);
    try {
      const messageId = await pubSubClient.topic(topicName).publish(dataBuffer);
      Logger.info(`Message ${messageId} published.`);
      resolve();
    } catch (error) {
      Logger.error(`Received error while publishing: ${error.message}`);
      process.exitCode = 1;
      reject(error);
    }
  });
};

// topics - array with obj
exports.publishMultipleToTopics = (userID, scopesObj) => {
  return new Promise(async (resolve) => {
    Logger.info("publishing multiple topics to pubsub");
    // console.log("[publish multiple to topics]");
    for await (const scope of Object.keys(scopesObj)) {
      const topicName = scopesObj[scope].topicName;
      const dataBuffer = Buffer.from(JSON.stringify(scopesObj[scope].data));
      // console.log({ topicName }, scopesObj[scope].data);
      try {
        const messageId = await pubSubClient
          .topic(topicName)
          .publish(dataBuffer);
        Logger.info(`Message ${messageId} published.`);
      } catch (error) {
        Logger.error(`Received error while publishing: ${error.message}`);
        process.exitCode = 1;
      }
    }
    resolve();
  });
};

exports.selectUserScopeBigQueryData = selectUserScopeBigQueryData;

let env = require("dotenv").config();
const dotenvParseVariables = require("dotenv-parse-variables");
env = dotenvParseVariables(env.parsed);
const Logger = require("../winston-config");
// ============ CLOUD SQL POSTGRES ============
// let knex;
const uuid_generator = require("uuid");

const createTcpPool = () => {
  Logger.info("<< POSTGRES >> INITIALIZING NEW CONNECTION TO POSTGRES DB");
  // if (knex && knex != undefined) {
  //   try {
  //     closeConnection();
  //   } catch (err) {
  //     Logger.error(err);
  //   }
  // }
  // Extract host and port from socket address
  const dbSocketAddr = env.DB_HOST.split(":"); // e.g. '127.0.0.1:5432'

  // Establish a connection to the database
  try {
    const knex = require("knex")({
      client: "pg",
      connection: {
        user: env.DB_USER, // e.g. 'my-user'
        password: env.DB_PASS, // e.g. 'my-user-password'
        database: env.DB_NAME, // e.g. 'my-database'
        host: dbSocketAddr[0], // e.g. '127.0.0.1'
        port: dbSocketAddr[1], // e.g. '5432'
      },
      pool: {
        min: 0,
        max: 7,
        // createRetryIntervalMillis: 200,
        // acquireTimeoutMillis: 60000,
        // createTimeoutMillis: 30000,
        // idleTimeoutMillis: 600000,
      },
    });
    Logger.info("<< POSTGRES >> POSTGRESS CONNECTION OPENED");
    return knex;
  } catch (err) {
    throw err;
  }
};

const SELECT = (knex, obj) => {
  return obj?.attr
    ? knex.select(obj.attr).from(obj.table)
    : knex.select().from(obj.table);
};

const selectUserPhoneNumbers = (knex, obj) => {
  Logger.info("<< POSTGRES >> SELECTING USER PHONE NUMBERS");
  return knex
    .select("phone_number")
    .from("phones")
    .where("user_id", obj.user_id);
};

const selectAllPhoneNumbers = (knex) => {
  Logger.info("<< POSTGRES >> SELECTING ALL USER PHONE NUMBERS");
  return knex.select("phone_number").from("phones");
};

const selectUserEmails = (knex, obj) => {
  Logger.info("<< POSTGRES >> SELECTING USER EMAIL ADDRESS");
  return knex
    .select("email_address", "is_verified")
    .from("emails")
    .where("user_id", obj.user_id);
};

const selectUserPassword = (knex, obj) => {
  Logger.info("<< POSTGRES >> SELECTING USER PASSWORD");
  return knex.select("password").from("users").where("user_id", obj.user_id);
};

const selectUserQuestion = (knex, obj) => {
  Logger.info("<< POSTGRES >> SELECTING USER SECURITY QUESTION");
  return knex
    .select("answer")
    .from("security_questions")
    .where({ user_id: obj.user_id, question: obj.question });
};

const selectUserPIN = (knex, obj) => {
  Logger.info("<< POSTGRES >> SELECTING USER PIN");
  return knex.select("pin").from("users").where("user_id", obj.user_id);
};

const isBiometryEnrolled = (knex, obj) => {
  Logger.info("<< POSTGRES >> CHECKING USER BIOMETRY STATUS");
  return knex
    .select("is_biometry_enrolled")
    .from("users")
    .where("user_id", obj.user_id);
};

const selectUserSecurityQuestions = (knex, obj) => {
  Logger.info("<< POSTGRES >> SELECTING USER ALL SECURITY QUESTIONS");
  return knex
    .select("question")
    .from("security_questions")
    .where("user_id", obj.user_id);
};

const addUserPhoneNumber = (knex, obj) => {
  Logger.info("<< POSTGRES >> ADDING USER PHONE NUMBER");
  return knex("phones").insert({
    phone_id: uuid_generator.v1(),
    user_id: obj.user_id,
    phone_number: obj.phone_number,
  });
};

const addUserSecurityQuestions = (knex, obj) => {
  Logger.info("<< POSTGRES >> ADDING USER SECURITY QUESTIONS");
  const user_id = obj.user_id;
  const data = [];
  for (const qn of obj.questions) {
    data.push({
      question_id: uuid_generator.v1(),
      user_id: user_id,
      question: qn.question,
      answer: qn.answer,
    });
  }
  return knex("security_questions").insert(data);
};

const deleteUserEmail = (knex, obj) => {
  Logger.info("<< POSTGRES >> DELETING USER EMAIL ADDRESS");
  return knex("emails")
    .where("user_id", obj.user_id)
    .where("email_address", obj.email)
    .del();
};

const deleteUserPhone = (knex, obj) => {
  Logger.info("<< POSTGRES >> DELETING USER PHONE NUMBER");
  return knex("phones")
    .where("user_id", obj.user_id)
    .where("phone_number", obj.phone_number)
    .del();
};

const setUserPIN = (knex, obj) => {
  Logger.info("<< POSTGRES >> SETTING USER PIN");
  return knex("users").where("user_id", obj.user_id).update("pin", obj.pin);
};

const deletePIN = (knex, obj) => {
  Logger.info("<< POSTGRES >> DELETING USER PIN");
  return knex("users").update({ pin: "" }).where("user_id", obj.user_id);
};

const deletePassword = (knex, obj) => {
  Logger.info("<< POSTGRES >> DELETING USER PASSWORD");
  return knex("users").update({ password: "" }).where("user_id", obj.user_id);
};

const deleteSecurityQuestion = (knex, obj) => {
  Logger.info("<< POSTGRES >> DELETING USER SECURITY QUESTIONS");
  return knex("security_questions")
    .delete()
    .where({ user_id: obj.user_id, question: obj.question });
};

const updateUserQuestion = (knex, obj) => {
  Logger.info("<< POSTGRES >> UPDATING USER SECURITY QUESTION");
  return knex("security_questions")
    .update({ question: obj.question, answer: obj.answer })
    .where("user_id", obj.user_id)
    .where("question", obj.replaced);
};

const setUserPassword = (knex, obj) => {
  Logger.info("<< POSTGRES >> SETTING USER PASSWORD");
  return knex("users")
    .where("user_id", obj.user_id)
    .update("password", obj.password);
};

const setUserBiometryStatus = (knex, obj) => {
  Logger.info("<< POSTGRES >> SETTING USER BIOMETRY STATUS");
  return knex("users")
    .where("user_id", obj.user_id)
    .update("is_biometry_enrolled", obj.isBiometryEnrolled);
};

const selectDriveCode = (knex, obj) => {
  Logger.info("<< POSTGRES >> SELECTING USER DRIVE CODE APP");
  return knex
    .select("drive_code", "drive_code_valid_to")
    .from("users")
    .where("user_id", obj.user_id);
};

const selectAllDriveCodes = (knex) => {
  Logger.info("<< POSTGRES >> SELECTING ALL DRIVES CODES");
  return knex.select("drive_code").from("users");
};

const selectUserByDriveCode = (knex, obj) => {
  Logger.info("<< POSTGRES >> SELECTING USER BY DRIVE CODE");
  return knex
    .select("user_id", "drive_code", "drive_code_valid_to")
    .from("users")
    .where("drive_code", obj.drive_code);
};

const setDriveCode = (knex, obj) => {
  Logger.info("<< POSTGRES >> SETTING DRIVE CODE APP");
  // var exp = 3600; //3600 - 60 minut
  const moment = require("moment");
  const valid_to = new Date(obj.drive_code_valid_to);
  const validTo = moment(valid_to).format("YYYY-MM-DD HH:mm:ss");
  // console.log({ validTo });

  return knex("users").where("user_id", obj.user_id).update({
    drive_code: obj.drive_code,
    drive_code_valid_to: validTo,
  });
};

const closeConnection = (knex) => {
  Logger.info("<< POSTGRES >> CLOSING CONNECTION");
  knex.destroy();
  knex = undefined;
  return;
};

exports.connect = createTcpPool;
exports.select = SELECT;
exports.userPhones = selectUserPhoneNumbers;
exports.selectAllPhoneNumbers = selectAllPhoneNumbers;
exports.userEmails = selectUserEmails;
exports.userPassword = selectUserPassword;
exports.userQuestion = selectUserQuestion;
exports.selectDriveCode = selectDriveCode;
exports.selectUserByDriveCode = selectUserByDriveCode;
exports.selectAllDriveCodes = selectAllDriveCodes;
exports.setDriveCode = setDriveCode;
exports.userPIN = selectUserPIN;
exports.isBiometryEnrolled = isBiometryEnrolled;
exports.userSecurityQuestions = selectUserSecurityQuestions;
exports.addPhone = addUserPhoneNumber;
exports.addSecurityQuestions = addUserSecurityQuestions;
exports.deleteEmail = deleteUserEmail;
exports.deletePhone = deleteUserPhone;
exports.setPIN = setUserPIN;
exports.setPassword = setUserPassword;
exports.deletePIN = deletePIN;
exports.deletePassword = deletePassword;
exports.deleteSecurityQuestion = deleteSecurityQuestion;
exports.updateUserQuestion = updateUserQuestion;
exports.setUserBiometryStatus = setUserBiometryStatus;
exports.close = closeConnection;
// exports.test = test;

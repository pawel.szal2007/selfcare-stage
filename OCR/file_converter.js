exports.convertToTiff = (file) => {
  return new Promise((resolve, reject) => {
    const Jimp = require("jimp")

    Jimp.read(file.tempFilePath, (err, newFile) => {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        const path = `${__dirname}/tiff_images/${file.name}.tiff`;
        newFile.write(path);
        resolve(path);
      }
    })
  })
}

exports.formatData = (data) => {
  const values = ["1.", "2.", "3.", "4.", "5.", "6.", "7.", "8.", "9.", "10."];

  let formattedData = {};

  for (const page of data.pages) {
    for (const block of page.words) {
      if (values.indexOf(block.word) > -1) {
        if (typeof formattedData[block.word] != "undefined") {
          continue;
        }

        const blocks = require("./middleware/blocks");
        const valueBlock = blocks.findBlockBelow(block, page.words);

        if (valueBlock) {
          formattedData[block.word] = "";
          formattedData[block.word] = valueBlock.word;
          // if(["1.", "2.", "4.", "8.", "10."].includes(block.word)){
          let additionalBlocks = blocks.getAdditionalBlocks(
            valueBlock,
            page.words,
            page.X_WIDTH
          );
          for (const additionalBlock of additionalBlocks) {
            formattedData[block.word] += " " + additionalBlock.word;
          }
          formattedData[block.word] = formattedData[block.word].trim();
          // }
        }
      }
    }
  }
  const mappedData = mapData(formattedData);
  if (mappedData.given_name && mappedData.given_name.split(" ").length > 1) {
    const names = mappedData.given_name.split(" ");
    mappedData.given_name = names[0];
    mappedData.middle_name = names[1];
  }
  mappedData.source = "passport_document_scan";
  mappedData.loa = "medium";
  // const dataObj = convertToBigQuerySchema(userId, mappedData, userDataChecked);
  return mappedData;
};

const mapData = (obj) => {
  const mappedObj = {};
  obj["1."] && (mappedObj["name"] = obj["1."]);
  obj["2."] && (mappedObj["given_name"] = obj["2."]);
  obj["3."] && (mappedObj["nationality"] = obj["3."]);
  obj["4."] && (mappedObj["date_of_birth"] = obj["4."]);
  obj["5."] && (mappedObj["national_id"] = obj["5."]);
  obj["6."] && (mappedObj["gender"] = obj["6."]);
  obj["7."] && (mappedObj["place_of_birth"] = obj["7."]);
  obj["8."] && (mappedObj["issued_date"] = obj["8."]);
  obj["9."] && (mappedObj["issuer_name"] = obj["9."]);
  obj["10."] && (mappedObj["valid_date"] = obj["10."]);

  return mappedObj;
};

// const convertToBigQuerySchema = (userId, passport_data, userData) => {
//   // console.log("convert to bigquery data...");
//   const o = {};
//
//   o.profile = {
//     topicName: "topic-user-profile",
//     data: {},
//   };
//   o.profile.data["timestamp"] = parseInt(Math.floor(Date.now() / 1000), 10);
//   o.profile.data["user_id"] = userId;
//   o.profile.data["source"] = "passport_document_scan";
//   o.profile.data["loa"] = "medium";
//   o.profile.data["locale"] = "";
//   o.profile.data["zoneinfo"] = "";
//   o.profile.data["updated_at"] = parseInt(Math.floor(Date.now() / 1000), 10);
//   o.profile.data.preferred_username = "";
//   passport_data.name && userData.includes("name")
//     ? (o.profile.data["name"] = passport_data["name"])
//     : (o.profile.data["name"] = "");
//   passport_data.given_name && userData.includes("given_name")
//     ? (o.profile.data["given_name"] = passport_data.given_name.split(" ")[0])
//     : (o.profile.data["given_name"] = "");
//
//   // IDENTITY ---> identity_card_number -> document_number, date_of_issue -> issued date, expiry_date --> valid date
//   o.identity = {
//     topicName: "topic-user-identity",
//     data: {},
//   };
//   o.identity.data["timestamp"] = parseInt(Math.floor(Date.now() / 1000), 10);
//   o.identity.data["user_id"] = userId;
//   o.identity.data["source"] = "id_document_scan";
//   o.identity.data["loa"] = "medium";
//   o.identity.data["updated_at"] = parseInt(Math.floor(Date.now() / 1000), 10);
//   o.identity.data.nationality =
//     passport_data.nationality && userData.includes("nationality")
//       ? passport_data.nationality
//       : "";
//   o.identity.data.document_name =
//     passport_data.document_name && userData.includes("document_name")
//       ? passport_data.document_name
//       : "";
//   o.identity.data.issued_country =
//     passport_data.issued_country && userData.includes("issued_country")
//       ? passport_data.issued_country
//       : "";
//   o.identity.data.street =
//     passport_data.street && userData.includes("street")
//       ? passport_data.street
//       : "";
//   o.identity.data.city =
//     passport_data.city && userData.includes("city") ? passport_data.city : "";
//   o.identity.data.zip =
//     passport_data.zip && userData.includes("zip") ? passport_data.zip : "";
//   o.identity.data.country =
//     passport_data.country && userData.includes("country")
//       ? passport_data.country
//       : "";
//   o.identity.data.document_number =
//     passport_data.document_number && userData.includes("document_number")
//       ? passport_data.document_number
//       : "";
//   o.identity.data.issued_date =
//     passport_data.issued_date && userData.includes("issued_date")
//       ? passport_data.issued_date
//       : "";
//   o.identity.data.valid_date =
//     passport_data.valid_date && userData.includes("valid_date")
//       ? passport_data.valid_date
//       : "";
//   o.identity.data.issuer_name =
//     passport_data.issuer_name && userData.includes("issuer_name")
//       ? passport_data.issuer_name
//       : "";
//   o.identity.data.national_id =
//     passport_data.national_id && userData.includes("national_id")
//       ? passport_data.national_id
//       : "";
//   o.identity.data.family_name =
//     passport_data.family_name && userData.includes("family_name")
//       ? passport_data.family_name
//       : "";
//   passport_data["given_name"] &&
//   passport_data["given_name"].split(" ").length > 1 &&
//   userData.includes("middle_name")
//     ? (o.identity.data["middle_name"] = passport_data["given_name"].split(
//         " "
//       )[1])
//     : (o.identity.data["middle_name"] = "");
//
//   o.profile_extended = {
//     topicName: "topic-user-profile-extended",
//     data: {},
//   };
//   o.profile_extended.data["timestamp"] = parseInt(
//     Math.floor(Date.now() / 1000),
//     10
//   );
//   o.profile_extended.data["user_id"] = userId;
//   o.profile_extended.data["source"] = "id_document_scan";
//   o.profile_extended.data["loa"] = "medium";
//   o.profile_extended.data["updated_at"] = parseInt(
//     Math.floor(Date.now() / 1000),
//     10
//   );
//   o.profile_extended.data.nickname =
//     passport_data.nickname && userData.includes("nickname")
//       ? passport_data.nickname
//       : "";
//   o.profile_extended.data.profile_url =
//     passport_data.profile_url && userData.includes("profile_url")
//       ? passport_data.profile_url
//       : "";
//   o.profile_extended.data.website =
//     passport_data.website && userData.includes("website")
//       ? passport_data.website
//       : "";
//   o.profile_extended.data.education =
//     passport_data.education && userData.includes("education")
//       ? passport_data.education
//       : "";
//   o.profile_extended.data.date_of_birth =
//     passport_data.date_of_birth && userData.includes("date_of_birth")
//       ? passport_data.date_of_birth
//       : "";
//   o.profile_extended.data.gender =
//     passport_data.gender && userData.includes("gender")
//       ? passport_data.gender
//       : "";
//   o.profile_extended.data.picture =
//     passport_data.picture && userData.includes("picture")
//       ? passport_data.picture
//       : "";
//
//   let i = 0;
//   for (const scope in o) {
//     for (const key in o[scope].data) {
//       if (
//         key != "timestamp" &&
//         key != "user_id" &&
//         key != "source" &&
//         key != "loa" &&
//         key != "updated_at"
//       ) {
//         if (o[scope].data[key] != "" && o[scope].data[key] != " ") {
//           i++;
//           break;
//         }
//       }
//     }
//     if (i === 0) delete o[scope];
//     i = 0;
//   }
//
//   return o;
// };

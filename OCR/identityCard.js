exports.formatData = (data) => {
  const values = [
    "NAZWISKO",
    "IMIONA",
    "DATA",
    "IDENTITY",
    "EXPIRY",
    "SEX",
    "NUMER",
    "ISSUE",
    "ORGAN",
    "OBYWATELSTWO",
    "MIEJSCE",
  ];

  let formattedData = {};

  for (const page of data.pages) {
    for (const block of page.words) {
      if (values.indexOf(block.word) > -1) {
        let suffix = "";
        if (["IMIONA", "NAZWISKO", "DATA", "NUMER"].includes(block.word)) {
          if (
            [
              "RODOWE",
              "RODZICÓW",
              "WYDANIA",
              "URODZENIA",
              "WAŻNOŚCI",
              "PESEL",
            ].includes(page.words[page.words.indexOf(block) + 1].word)
          ) {
            suffix = " " + page.words[page.words.indexOf(block) + 1].word;
          }
        }

        if (typeof formattedData[block.word + suffix] != "undefined") {
          continue;
        }

        const blocks = require("./middleware/blocks");
        const valueBlock = blocks.findBlockBelow(block, page.words);

        if (
          [
            "NAZWISKO",
            "IMIONA",
            "SEX",
            "ORGAN",
            "OBYWATELSTWO",
            "MIEJSCE",
          ].includes(block.word)
        ) {
          if (!/^[a-zA-ZłŁąĄćĆęĘńŃóÓśŚ]+$/.test(valueBlock.word)) {
            continue;
          }
        }

        if (valueBlock) {
          let field;
          if (block.word === "OBYWATELSTWO") {
            field = "nationality";
          } else if (block.word === "IDENTITY") {
            field = "document_number";
          } else if (block.word === "EXPIRY") {
            field = "valid_date";
          } else if (block.word === "SEX") {
            field = "gender";
          } else if (block.word === "ORGAN") {
            field = "issuer_name";
          } else if (block.word === "ISSUE") {
            field = "issued_date";
          } else if (block.word === "MIEJSCE") {
            field = "place_of_birth";
          } else field = block.word + suffix;

          if (field === "NUMER PESEL") {
            field = "national_id";
          } else if (field === "IMIONA") {
            field = "given_name";
          } else if (field === "IMIONA RODZICÓW") {
            field = "parents_given_names";
          } else if (field === "NAZWISKO") {
            field = "name";
          } else if (field === "NAZWISKO RODOWE") {
            field = "family_name";
          } else if (field === "DATA URODZENIA") {
            field = "date_of_birth";
          } else if (field === "DATA WYDANIA") {
            field = "issued_date";
          }

          formattedData[field] = "";
          formattedData[field] = valueBlock.word;

          if (
            ["IMIONA", "NAZWISKO", "ORGAN", "IDENTITY", "MIEJSCE"].includes(
              block.word
            )
          ) {
            let additionalBlocks = blocks.getAdditionalBlocks(
              valueBlock,
              page.words,
              page.X_WIDTH
            );
            for (const additionalBlock of additionalBlocks) {
              formattedData[field] += " " + additionalBlock.word;
            }
            formattedData[field] = formattedData[field].trim();
          }

          if (block.word === "IDENTITY") {
            if (
              formattedData[field].length != 9 &&
              formattedData[field].length != 10
            ) {
              delete formattedData[field];
              continue;
            }
          }
          if (block.word === "NUMER") {
            if (formattedData[field].length != 11) {
              delete formattedData[field];
              continue;
            }
          }
          if (block.word === "DATA" || block.word === "DATE") {
            if (formattedData[field].length != 10) {
              delete formattedData[field];
              continue;
            }
          }
        }
      }
    }
  }
  formattedData.document_name = "DOWÓD OSOBISTY / IDENTITY CARD";
  if (
    formattedData.given_name &&
    formattedData.given_name.split(" ").length > 1
  ) {
    const names = formattedData.given_name.split(" ");
    formattedData.given_name = names[0];
    formattedData.middle_name = names[1];
  }
  formattedData.source = "id_document_scan";
  formattedData.loa = "medium";
  // const objData = convertToBigQuerySchema(
  //   userID,
  //   formattedData,
  //   userDataChecked
  // );
  // console.log({ objData });
  return formattedData;
};

// const convertToBigQuerySchema = (userId, id_data, userData) => {
//   // console.log("convert to bigquery data...");
//   const o = {};
//
//   o.profile = {
//     topicName: "topic-user-profile",
//     data: {},
//   };
//   o.profile.data["timestamp"] = parseInt(Math.floor(Date.now() / 1000), 10);
//   o.profile.data["user_id"] = userId;
//   o.profile.data["source"] = "id_document_scan";
//   o.profile.data["loa"] = "medium";
//   o.profile.data["locale"] = "";
//   o.profile.data["zoneinfo"] = "";
//   o.profile.data["updated_at"] = parseInt(Math.floor(Date.now() / 1000), 10);
//   o.profile.data.preferred_username = "";
//   id_data.name && userData.includes("name")
//     ? (o.profile.data["name"] = id_data["name"])
//     : (o.profile.data["name"] = "");
//   id_data.given_name && userData.includes("given_name")
//     ? (o.profile.data["given_name"] = id_data.given_name.split(" ")[0])
//     : (o.profile.data["given_name"] = "");
//
//   // IDENTITY ---> identity_card_number -> document_number, date_of_issue -> issued date, expiry_date --> valid date
//   o.identity = {
//     topicName: "topic-user-identity",
//     data: {},
//   };
//   o.identity.data["timestamp"] = parseInt(Math.floor(Date.now() / 1000), 10);
//   o.identity.data["user_id"] = userId;
//   o.identity.data["source"] = "id_document_scan";
//   o.identity.data["loa"] = "medium";
//   o.identity.data["updated_at"] = parseInt(Math.floor(Date.now() / 1000), 10);
//   o.identity.data.nationality =
//     id_data.nationality && userData.includes("nationality")
//       ? id_data.nationality
//       : "";
//   o.identity.data.document_name =
//     id_data.document_name && userData.includes("document_name")
//       ? id_data.document_name
//       : "";
//   o.identity.data.issued_country =
//     id_data.issued_country && userData.includes("issued_country")
//       ? id_data.issued_country
//       : "";
//   o.identity.data.street =
//     id_data.street && userData.includes("street") ? id_data.street : "";
//   o.identity.data.city =
//     id_data.city && userData.includes("city") ? id_data.city : "";
//   o.identity.data.zip =
//     id_data.zip && userData.includes("zip") ? id_data.zip : "";
//   o.identity.data.country =
//     id_data.country && userData.includes("country") ? id_data.country : "";
//   o.identity.data.document_number =
//     id_data.document_number && userData.includes("document_number")
//       ? id_data.document_number
//       : "";
//   o.identity.data.issued_date =
//     id_data.issued_date && userData.includes("issued_date")
//       ? id_data.issued_date
//       : "";
//   o.identity.data.valid_date =
//     id_data.valid_date && userData.includes("valid_date")
//       ? id_data.valid_date
//       : "";
//   o.identity.data.issuer_name =
//     id_data.issuer_name && userData.includes("issuer_name")
//       ? id_data.issuer_name
//       : "";
//   o.identity.data.national_id =
//     id_data.national_id && userData.includes("national_id")
//       ? id_data.national_id
//       : "";
//   o.identity.data.family_name =
//     id_data.family_name && userData.includes("family_name")
//       ? id_data.family_name
//       : "";
//   id_data["given_name"] &&
//   id_data["given_name"].split(" ").length > 1 &&
//   userData.includes("middle_name")
//     ? (o.identity.data["middle_name"] = id_data["given_name"].split(" ")[1])
//     : (o.identity.data["middle_name"] = "");
//
//   o.profile_extended = {
//     topicName: "topic-user-profile-extended",
//     data: {},
//   };
//   o.profile_extended.data["timestamp"] = parseInt(
//     Math.floor(Date.now() / 1000),
//     10
//   );
//   o.profile_extended.data["user_id"] = userId;
//   o.profile_extended.data["source"] = "id_document_scan";
//   o.profile_extended.data["loa"] = "medium";
//   o.profile_extended.data["updated_at"] = parseInt(
//     Math.floor(Date.now() / 1000),
//     10
//   );
//   o.profile_extended.data.nickname =
//     id_data.nickname && userData.includes("nickname") ? id_data.nickname : "";
//   o.profile_extended.data.profile_url =
//     id_data.profile_url && userData.includes("profile_url")
//       ? id_data.profile_url
//       : "";
//   o.profile_extended.data.website =
//     id_data.website && userData.includes("website") ? id_data.website : "";
//   o.profile_extended.data.education =
//     id_data.education && userData.includes("education")
//       ? id_data.education
//       : "";
//   o.profile_extended.data.date_of_birth =
//     id_data.date_of_birth && userData.includes("date_of_birth")
//       ? id_data.date_of_birth
//       : "";
//   o.profile_extended.data.gender =
//     id_data.gender && userData.includes("gender") ? id_data.gender : "";
//   o.profile_extended.data.picture =
//     id_data.picture && userData.includes("picture") ? id_data.picture : "";
//
//   // if (userData != undefined && userData.length > 0) {
//   //   if (!userData.includes("name")) {
//   //     o.profile.data["name"] = " ";
//   //   }
//   //   if (!userData.includes("given_name")) {
//   //     o.profile.data["given_name"] = " ";
//   //   }
//   //
//   //   if (!userData.includes("document_number")) {
//   //     o.identity.data["document_number"] = " ";
//   //   }
//   //   if (!userData.includes("issued_date")) {
//   //     o.identity.data["issued_date"] = " ";
//   //   }
//   //   if (!userData.includes("valid_date")) {
//   //     o.identity.data["valid_date"] = " ";
//   //   }
//   //   if (!userData.includes("issuer_name")) {
//   //     o.identity.data["issuer_name"] = " ";
//   //   }
//   //   if (!userData.includes("national_id")) {
//   //     o.identity.data["national_id"] = " ";
//   //   }
//   //   if (!userData.includes("family_name")) {
//   //     o.identity.data["family_name"] = " ";
//   //   }
//   //
//   //   if (!userData.includes("gender")) {
//   //     o.profile_extended.data["gender"] = " ";
//   //   }
//   //   if (!userData.includes("date_of_birth")) {
//   //     o.profile_extended.data["date_of_birth"] = " ";
//   //   }
//   //   if (!userData.includes("user_img")) {
//   //     o.profile_extended.data["user_img"] = " ";
//   //   }
//   // }
//
//   let i = 0;
//   for (const scope in o) {
//     for (const key in o[scope].data) {
//       if (
//         key != "timestamp" &&
//         key != "user_id" &&
//         key != "source" &&
//         key != "loa" &&
//         key != "updated_at"
//       ) {
//         if (o[scope].data[key] != "" && o[scope].data[key] != " ") {
//           i++;
//           break;
//         }
//       }
//     }
//     if (i === 0) delete o[scope];
//     i = 0;
//   }
//
//   return o;
// };

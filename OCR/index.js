// exports.extractData = async (file, context) => {
//   // (async () => {
//   try {
//     console.log("///////////   OCR - TEXT DETECTION  ///////////");
//     console.log(`  Bucket: ${file.bucket}`);
//     console.log(`  File: ${file.name}`);
//     if (file.name.includes("users/") && file.name.includes("identity_card/") && !file.name.includes("web_scraper/")) {
//       const OCR = require("./OCR");
//       // file.name
//       const data = await OCR.process(file.name);
//       // console.log(data.pages[0].words);
//       const folderName = "identity_card";
//
//       if (folderName === "identity_card") {
//         const identityCard = require("./identityCard");
//         const formattedData = identityCard.formatData(data);
//         console.log(formattedData);
//         await OCR.saveData(file.name.replace("users/", "data/"), formattedData);
//       } else if (folderName === "passport") {
//         const passport = require("./passport");
//         console.log(passport.format(data));
//       } else {
//         console.log("Nieprawidlowa nazwa folderu: " + folderName + ". Dozwolone: identity_card lub passport");
//       }
//     }
//
//     if (file.name.includes("users/") && file.name.includes("web_scraper/") && (file.name.split(".").pop() === "mhtml" || file.name.split(".").pop() === "html")) {
//       let govPath;
//       file.name.split('/')[3] === "gov_identity_card" ? govPath = "identity-card" : console.log();
//       file.name.split('/')[3] === "gov_personal_number" ? govPath = "pesel" : console.log();
//       if (govPath) {
//         const axios = require('axios');
//         const qs = require("qs");
//         axios({
//           method: 'post',
//           url: `https://selfcare.topid.org/:8080/scraper/gov/${govPath}`,
//           headers: {
//             'Content-Type': 'application/x-www-form-urlencoded'
//           },
//           data: qs.stringify({
//             bucket: file.bucket,
//             filePath: file.name
//           })
//         }).then((response) => {
//           console.log(response.data);
//         }).catch((error) => {
//           console.log(error);
//         });
//       }
//     }
//   } catch (err) {
//     console.log(err);
//   }
//   // })();
// }
let env = require("dotenv").config();
const dotenvParseVariables = require("dotenv-parse-variables");
env = dotenvParseVariables(env.parsed);

(async () => {
  // console.log('Wywolalem OCR!');
  let filePath;
  let fileType;
  let userId;
  // let userDataChecked;

  for (const parameter of process.argv) {
    parameter.includes("filePath") && (filePath = parameter.split("=")[1]);
    parameter.includes("fileType") && (fileType = parameter.split("=")[1]);
    parameter.includes("user_id") && (userId = parameter.split("=")[1]);
    // parameter.includes("data") && (userDataChecked = parameter.split("=")[1]);
  }

  if (!filePath) throw new Error("no filePath parameter");
  if (!fileType) throw new Error("no fileType parameter");
  if (!userId) throw new Error("no user_id parameter");

  // console.log({ filePath }, { fileType }, { userId });
  try {
    const OCR = require("./OCR");
    const data = await OCR.process(filePath);
    let formattedData;
    if (fileType === "identity_card") {
      const identityCard = require("./identityCard");
      formattedData = identityCard.formatData(data);
    } else if (fileType === "passport") {
      const passport = require("./passport");
      formattedData = passport.formatData(data);
    } else if (fileType === "gov_id_pdf") {
      const gov_id_pdf = require("./gov_id_pdf");
      formattedData = gov_id_pdf.formatData(data);
    } else throw new Error(`fileType ${fileType} not recognized`);

    // save to bucket storage
    await OCR.uploadData(
      env.GCP_STORAGE_BUCKET,
      userId,
      fileType,
      formattedData
    );

    if (formattedData) {
      console.log(JSON.stringify(formattedData));
    } else {
      throw new Error(
        "formatted data error - data was not formatted correctly"
      );
    }
  } catch (err) {
    console.log(err);
  }
  // run biquery script to get all results from COLLECTION->TABLE
  // exec(`node ${__dirname}/bigquery.js collection=${collection} table=${table}`, (error, stdout, stderr) => {
  //   if (error !== null) {
  //     console.log({error});
  //   } else {
  //     console.log(stdout);
  //   }
  // });
})();

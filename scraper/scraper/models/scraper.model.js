const HTMLParser = require("node-html-parser");

exports.getGOVIdentityCardObjectData = async (page) => {
  try {
    const dataObject = await page.evaluate(() => {
      try {
        let userData = Array.from(document.querySelectorAll(".p_data-row"));
        let identityCards = Array.from(document.querySelectorAll(".accordion-heading__button-text span"));
        let jsObject = {};
        const labels = ["Numer PESEL", "Imię (imiona)", "Nazwisko", "Nazwisko rodowe", "Imię ojca", "Imię matki", "Nazwisko rodowe matki", "Data i miejsce urodzenia", "Płeć"]
        const identityCardLabels = ["Data wydania (personalizacji)", "Data ważności dowodu", "Organ, który wydał dowód", "Przyczyna wydania", "Data i godzina zawieszenia", "Data unieważnienia"];
        const IDENTITY_CARD_ALIAS = "Aktualny dowód osobisty";
        const HISTORICAL_IDENTITY_CARD_ALIAS = "Poprzednie numery dowodów osobistych";

        identityCards.map((identityCard, i) => {
          if (identityCard.innerText.includes("Dowód osobisty")) {
            if (jsObject[HISTORICAL_IDENTITY_CARD_ALIAS]) {
              jsObject[HISTORICAL_IDENTITY_CARD_ALIAS].push(identityCards[i + 1].innerText);
              return;
            }
            jsObject[HISTORICAL_IDENTITY_CARD_ALIAS] = [];
            jsObject[IDENTITY_CARD_ALIAS] = {};
            jsObject[IDENTITY_CARD_ALIAS]["Numer dowodu"] = {};
            jsObject[IDENTITY_CARD_ALIAS]["Numer dowodu"] = identityCards[i + 1].innerText;
          }
        });

        userData.map(data => {
          if (jsObject[data.children[0].innerText] || jsObject[IDENTITY_CARD_ALIAS][data.children[0].innerText]) {
            return;
          }

          if (identityCardLabels.indexOf(data.children[0].innerText) > -1) {
            jsObject[IDENTITY_CARD_ALIAS][data.children[0].innerText] = {};
            jsObject[IDENTITY_CARD_ALIAS][data.children[0].innerText] = data.children[1].innerText;
            return;
          }

          if (labels.indexOf(data.children[0].innerText) > -1) {
            jsObject[data.children[0].innerText] = {};
            jsObject[data.children[0].innerText] = data.children[1].innerText;
          }
        });
        return jsObject;
      } catch (err) {
        return err;
      }
    });
    return dataObject;
  } catch (err) {
    throw new Error("Nie udało się wydobyć danych. " + err);
  }
}

exports.getGOVPESELObjectData = async (page) => {
  try {
    const dataObject = await page.evaluate(() => {
      let userPESELData = Array.from(document.querySelectorAll(".user-data__label, .user-data__value"));
      let jsPESELObject = {};
      const PESELlabels = ["Numer PESEL", "Imię (imiona)", "Nazwisko", "Nazwisko rodowe", "Imię ojca", "Imię matki", "Nazwisko rodowe matki", "Data urodzenia", "Miejsce urodzenia", "Płeć", "Stan cywilny", "Obywatelstwo albo status bezpaństwowca", "Adres zameldowania na pobyt stały", "Data zameldowania na pobyt stały", "Data wymeldowania z miejsca pobytu stałego", "Adres zameldowania na pobyt czasowy", "Data zameldowania na pobyt czasowy", "Data wymeldowania z miejsca pobytu czasowego"];
      userPESELData.map((data, i) => {
        if (jsPESELObject[data.innerText]) {
          return;
        }
        if (PESELlabels.includes(data.innerText)) {
          jsPESELObject[data.innerText] = {};
          jsPESELObject[data.innerText] = userPESELData[i + 1].innerText;
        }
      });
      return jsPESELObject;
    });
    return dataObject;
  } catch (err) {
    throw new Error("Nie udało się wydobyć danych. " + err);
  }
}

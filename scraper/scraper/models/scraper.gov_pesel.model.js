const HTMLParser = require("node-html-parser");

exports.scrape = async (page) => {
  try {
    return await page
      .evaluate(() => {
        let userPESELData = Array.from(
          document.querySelectorAll(".user-data__label, .user-data__value")
        );
        let jsPESELObject = {};
        const PESELlabels = [
          "Numer PESEL",
          "Imię (imiona)",
          "Nazwisko",
          "Nazwisko rodowe",
          "Imię ojca",
          "Imię matki",
          "Nazwisko rodowe matki",
          "Data urodzenia",
          "Miejsce urodzenia",
          "Płeć",
          "Stan cywilny",
          "Obywatelstwo albo status bezpaństwowca",
          "Adres zameldowania na pobyt stały",
          "Data zameldowania na pobyt stały",
          "Data wymeldowania z miejsca pobytu stałego",
          "Adres zameldowania na pobyt czasowy",
          "Obecny kraj miejsca zamieszkania",
          "Data zameldowania na pobyt czasowy",
          "Data wymeldowania z miejsca pobytu czasowego",
        ];
        userPESELData.map((data, i) => {
          if (jsPESELObject[data.innerText]) {
            return;
          }
          if (PESELlabels.includes(data.innerText)) {
            jsPESELObject[data.innerText] = {};
            jsPESELObject[data.innerText] = userPESELData[i + 1].innerText;
          }
        });
        return jsPESELObject;
      })
      .catch((err) => {
        throw new Error("Error occured while scraping data: ", { err });
      });
  } catch (err) {
    throw new Error("Error occured while scraping data: ", { err });
  }
};

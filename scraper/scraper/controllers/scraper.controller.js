exports.getData = (filePath, website) => {
  return new Promise((resolve, reject) => {
    const ScraperModel = require("../models/scraper.model");
    (async () => {
      try {
        const puppeteer = require("puppeteer");
        // console.log(`tworze obiekt przeglądarki...`);
        const browser = await puppeteer.launch({
          args: ["--no-sandbox", "--disable-setuid-sandbox"],
          headless: true,
          dumpio: true,
        });
        // console.log(`otwieram nowa strone w przegladarce...`);
        const page = await browser.newPage();
        // console.log(`przechodze na strone do skrapowania pliku html...`);
        await page.goto("file://" + filePath);
        // console.log(`oczekuje na załadowanie strony...`);
        // { waitUntil: "networkidle0" }
        // await page.waitForNavigation({ waitUntil: "networkidle0" });
        // console.log(`dodaje opóźnienie 5s...`);
        const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
        await delay(5000);
        let objectData;

        // services to scrape, to be changed in future in case of next services
        switch (website) {
          case "gov_rdo":
            // console.log('==== IDENTITY CARD SCRAPING (RDO) ===');
            const rdo_scraper = require("../models/scraper.gov_rdo.model");
            objectData = await rdo_scraper.scrape(page);
            break;
          case "gov_pesel":
            // console.log('==== PESEL SCRAPING ===');
            const pesel_scraper = require("../models/scraper.gov_pesel.model");
            objectData = await pesel_scraper.scrape(page);
            break;
          case "gov_nl":
            const nl_scraper = require("../models/scraper.gov_nl.model");
            objectData = await nl_scraper.scrape(page);
            break;
          case "linkedin":
            // console.log(`linkedin scraper controller getting data...`);
            const linkedin_scraper = require("../models/scraper.linkedin.model");
            objectData = await linkedin_scraper.scrape(page);
            break;
          default:
            reject(
              `Application doesn't support '${website}' scraping. Possible services: "gov_rdo" or "gov_pesel"`
            );
            break;
        }

        // console.log({ objectData });
        await browser.close().catch((err) => {
          // console.log("The browser hasn't closed: " + err)
        });
        // console.log("[scraper] Done");
        // console.log(JSON.stringify(objectData));
        resolve(objectData);
      } catch (err) {
        reject(err);
      }
    })();
  });
};

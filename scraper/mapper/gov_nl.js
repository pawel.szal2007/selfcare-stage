exports.map = (data) => {
  const o = {};

  o["source"] = "gov_nl";
  o["loa"] = "high";
  data["Identiteitsgegevens"]["Voornamen"]
    ? (o["given_name"] = data["Identiteitsgegevens"]["Voornamen"].split(" ")[0])
    : (o["given_name"] = "");
  data["Identiteitsgegevens"]["Voornamen"] &&
  data["Identiteitsgegevens"]["Voornamen"].split(" ").length > 1
    ? (o["middle_name"] = data["Identiteitsgegevens"]["Voornamen"].split(
        " "
      )[1])
    : (o["middle_name"] = "");
  data["Identiteitsgegevens"]["Geslachtsnaam"]
    ? (o["name"] = data["Identiteitsgegevens"]["Geslachtsnaam"])
    : (o["name"] = "");
  data["Identiteitsgegevens"]["Geslacht"]
    ? (o["gender"] = data["Identiteitsgegevens"]["Geslacht"])
    : (o["gender"] = "");
  data["Identiteitsgegevens"]["Burgerservicenummer"]
    ? (o["national_id"] = data["Identiteitsgegevens"]["Burgerservicenummer"])
    : (o["national_id"] = "");
  data["Identiteitsgegevens"]["Geboortedatum"]
    ? (o["date_of_birth"] = data["Identiteitsgegevens"]["Geboortedatum"])
    : (o["date_of_birth"] = "");
  data["Identiteitsgegevens"]["Geboorteplaats"]
    ? (o["place_of_birth"] = data["Identiteitsgegevens"]["Geboorteplaats"])
    : (o["place_of_birth"] = "");
  data["Identiteitsgegevens"]["Geboorteland"]
    ? (o["nationality"] = data["Identiteitsgegevens"]["Geboorteland"])
    : (o["nationality"] = "");
  data["Identiteitsgegevens"]["Gemeente document"]
    ? (o["document_name"] = data["Identiteitsgegevens"]["Gemeente document"])
    : (o["document_name"] = "");
  data["Identiteitsgegevens"]["Datum document"]
    ? (o["issued_date"] = data["Identiteitsgegevens"]["Datum document"])
    : (o["issued_date"] = "");
  data["Identiteitsgegevens"]["Ingangsdatum geldigheid"]
    ? (o["valid_date"] = data["Identiteitsgegevens"]["Ingangsdatum geldigheid"])
    : (o["valid_date"] = "");

  if (data["Adresgegevens"][0]) {
    data["Adresgegevens"][0]["Straat"]
      ? (o[
          "street"
        ] = `${data["Adresgegevens"][0]["Straat"]} ${data["Adresgegevens"][0]["Huisnummer"]}`)
      : (o["street"] = "");
    data["Adresgegevens"][0]["Postcode"]
      ? (o["zip"] = data["Adresgegevens"][0]["Postcode"])
      : (o["zip"] = "");
    data["Adresgegevens"][0]["Woonplaatsnaam"]
      ? (o["city"] = data["Adresgegevens"][0]["Woonplaatsnaam"])
      : (o["city"] = "");
  }

  return o;
};

exports.map = (data) => {
  const o = {};

  o["source"] = "gov_pesel";
  o["loa"] = "high";
  data["Numer PESEL"]
    ? (o["national_id"] = data["Numer PESEL"])
    : (o["national_id"] = "");
  data["Imię (imiona)"] && data["Imię (imiona)"].split(" ").length > 1
    ? (o["middle_name"] = data["Imię (imiona)"].split(" ")[1])
    : (o["middle_name"] = "");
  data["Imię (imiona)"]
    ? (o["given_name"] = data["Imię (imiona)"].split(" ")[0])
    : (o["given_name"] = "");
  data["Nazwisko"] ? (o["name"] = data["Nazwisko"]) : (o["name"] = "");
  data["Nazwisko rodowe"]
    ? (o["family_name"] = data["Nazwisko rodowe"])
    : (o["family_name"] = "");
  data["Data urodzenia"]
    ? (o["date_of_birth"] = data["Data urodzenia"])
    : (o["date_of_birth"] = "");
  data["Płeć"] ? (o["gender"] = data["Płeć"]) : (o["gender"] = "");
  data["Adres zameldowania na pobyt stały"]
    ? (o["street"] = data["Adres zameldowania na pobyt stały"])
    : (o["street"] = "");
  data["Obecny kraj miejsca zamieszkania"]
    ? (o["country"] = data["Obecny kraj miejsca zamieszkania"])
    : (o["country"] = "");

  return o;
};

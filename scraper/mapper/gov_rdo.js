exports.map = (data) => {
  const o = {};

  o["source"] = "gov_rdo";
  o["loa"] = "high";
  o["document_name"] = "DOWÓD OSOBISTY";
  data["Data i miejsce urodzenia"]
    ? (o["date_of_birth"] = data["Data i miejsce urodzenia"].split(" ")[0])
    : (o["date_of_birth"] = "");

  // miejsce urodzenia idzie na razie pod miasto
  let city = "";
  if (data["Data i miejsce urodzenia"]) {
    const splitted = data["Data i miejsce urodzenia"].split(" ");
    // ["20.06.2181", "Sandomierz", "Warka"]
    for (let i = 0; i < splitted.length; i++) {
      if (i > 0) {
        city += splitted[i];
      }
      if (i > 0 && i < splitted.length - 1) {
        city += " ";
      }
    }
  }
  data["Data i miejsce urodzenia"] ? (o["city"] = city) : (o["city"] = city);
  data["Płeć"] ? (o["gender"] = data["Płeć"]) : (o["gender"] = "");
  data["Aktualny dowód osobisty"]["Zdjęcie"]
    ? (o["picture"] = data["Aktualny dowód osobisty"]["Zdjęcie"])
    : (o["picture"] = "");
  data["Aktualny dowód osobisty"]["Numer dowodu"]
    ? (o["document_number"] = data["Aktualny dowód osobisty"]["Numer dowodu"])
    : (o["document_number"] = "");
  data["Aktualny dowód osobisty"]["Data wydania (personalizacji)"]
    ? (o["issued_date"] =
        data["Aktualny dowód osobisty"]["Data wydania (personalizacji)"])
    : (o["issued_date"] = "");
  data["Aktualny dowód osobisty"]["Data ważności dowodu"]
    ? (o["valid_date"] =
        data["Aktualny dowód osobisty"]["Data ważności dowodu"])
    : (o["valid_date"] = "");
  data["Aktualny dowód osobisty"]["Organ, który wydał dowód"]
    ? (o["issuer_name"] =
        data["Aktualny dowód osobisty"]["Organ, który wydał dowód"])
    : (o["issuer_name"] = "");
  data["Obywatelstwo"]
    ? (o["nationality"] = data["Obywatelstwo"])
    : (o["nationality"] = "");
  data["Numer PESEL"]
    ? (o["national_id"] = data["Numer PESEL"])
    : (o["national_id"] = "");
  data["Nazwisko rodowe"]
    ? (o["family_name"] = data["Nazwisko rodowe"])
    : (o["family_name"] = "");
  data["Imię (imiona)"] && data["Imię (imiona)"].split(" ").length > 1
    ? (o["middle_name"] = data["Imię (imiona)"].split(" ")[1])
    : (o["middle_name"] = "");
  data["Nazwisko"] ? (o["name"] = data["Nazwisko"]) : (o["name"] = "");
  data["Imię (imiona)"]
    ? (o["given_name"] = data["Imię (imiona)"].split(" ")[0])
    : (o["given_name"] = "");

  // PROFILE ----> zmiana surname na name i given names na given name
  // o.profile = {
  //   topicName: "topic-user-profile",
  //   data: {},
  // };
  // o.profile.data["timestamp"] = parseInt(Math.floor(Date.now() / 1000), 10);
  // o.profile.data["user_id"] = userID;
  // o.profile.data["source"] = "gov_rdo";
  // o.profile.data["loa"] = "high";
  // o.profile.data["locale"] = "";
  // o.profile.data["zoneinfo"] = "";
  // o.profile.data["updated_at"] = parseInt(Math.floor(Date.now() / 1000), 10);
  // o.profile.data.preferred_username = "";
  // data["Nazwisko"]
  //   ? (o.profile.data["name"] = data["Nazwisko"])
  //   : (o.profile.data["name"] = "");
  // data["Imię (imiona)"]
  //   ? (o.profile.data["given_name"] = data["Imię (imiona)"].split(" ")[0])
  //   : (o.profile.data["given_name"] = "");
  //
  // // IDENTITY ---> identity_card_number -> document_number, date_of_issue -> issued date, expiry_date --> valid date
  // o.identity = {
  //   topicName: "topic-user-identity",
  //   data: {},
  // };
  // o.identity.data["timestamp"] = parseInt(Math.floor(Date.now() / 1000), 10);
  // o.identity.data["user_id"] = userID;
  // o.identity.data["source"] = "gov_rdo";
  // o.identity.data["loa"] = "high";
  // o.identity.data["updated_at"] = parseInt(Math.floor(Date.now() / 1000), 10);
  // o.identity.data.nationality = "";
  // o.identity.data.document_name = "";
  // o.identity.data.issued_country = "";
  // o.identity.data.street = "";
  // o.identity.data.city = "";
  // o.identity.data.zip = "";
  // o.identity.data.country = "";
  // data["Aktualny dowód osobisty"]["Numer dowodu"]
  //   ? (o.identity.data["document_number"] =
  //       data["Aktualny dowód osobisty"]["Numer dowodu"])
  //   : (o.identity.data["document_number"] = "");
  // data["Aktualny dowód osobisty"]["Data wydania (personalizacji)"]
  //   ? (o.identity.data["issued_date"] =
  //       data["Aktualny dowód osobisty"]["Data wydania (personalizacji)"])
  //   : (o.identity.data["issued_date"] = "");
  // data["Aktualny dowód osobisty"]["Data ważności dowodu"]
  //   ? (o.identity.data["valid_date"] =
  //       data["Aktualny dowód osobisty"]["Data ważności dowodu"])
  //   : (o.identity.data["valid_date"] = "");
  // data["Aktualny dowód osobisty"]["Organ, który wydał dowód"]
  //   ? (o.identity.data["issuer_name"] =
  //       data["Aktualny dowód osobisty"]["Organ, który wydał dowód"])
  //   : (o.identity.data["issuer_name"] = "");
  // data["Numer PESEL"]
  //   ? (o.identity.data["national_id"] = data["Numer PESEL"])
  //   : (o.identity.data["national_id"] = "");
  // data["Nazwisko rodowe"]
  //   ? (o.identity.data["family_name"] = data["Nazwisko rodowe"])
  //   : (o.identity.data["family_name"] = "");
  // data["Imię (imiona)"] && data["Imię (imiona)"].split(" ").length > 1
  //   ? (o.identity.data["middle_name"] = data["Imię (imiona)"].split(" ")[1])
  //   : (o.identity.data["middle_name"] = "");
  //
  // // EXTENDED
  // o.profile_extended = {
  //   topicName: "topic-user-profile-extended",
  //   data: {},
  // };
  // o.profile_extended.data["timestamp"] = parseInt(
  //   Math.floor(Date.now() / 1000),
  //   10
  // );
  // o.profile_extended.data["user_id"] = userID;
  // o.profile_extended.data["source"] = "gov_rdo";
  // o.profile_extended.data["loa"] = "high";
  // o.profile_extended.data["updated_at"] = parseInt(
  //   Math.floor(Date.now() / 1000),
  //   10
  // );
  // o.profile_extended.data.nickname = "";
  // o.profile_extended.data.profile_url = "";
  // o.profile_extended.data.website = "";
  // o.profile_extended.data.education = "";
  // data["Data i miejsce urodzenia"]
  //   ? (o.profile_extended.data["date_of_birth"] =
  //       data["Data i miejsce urodzenia"])
  //   : (o.profile_extended.data["date_of_birth"] = "");
  // data["Płeć"]
  //   ? (o.profile_extended.data["gender"] = data["Płeć"])
  //   : (o.profile_extended.data["gender"] = "");
  // data["Aktualny dowód osobisty"]["Zdjęcie"]
  //   ? (o.profile_extended.data["picture"] =
  //       data["Aktualny dowód osobisty"]["Zdjęcie"])
  //   : (o.profile_extended.data["picture"] = "");
  //
  // if (userData != undefined && userData.length > 0) {
  //   if (!userData.includes("name")) {
  //     o.profile.data["name"] = " ";
  //   }
  //   if (!userData.includes("given_name")) {
  //     o.profile.data["given_name"] = " ";
  //   }
  //
  //   if (!userData.includes("document_number")) {
  //     o.identity.data["document_number"] = " ";
  //   }
  //   if (!userData.includes("issued_date")) {
  //     o.identity.data["issued_date"] = " ";
  //   }
  //   if (!userData.includes("valid_date")) {
  //     o.identity.data["valid_date"] = " ";
  //   }
  //   if (!userData.includes("issuer_name")) {
  //     o.identity.data["issuer_name"] = " ";
  //   }
  //   if (!userData.includes("national_id")) {
  //     o.identity.data["national_id"] = " ";
  //   }
  //   if (!userData.includes("family_name")) {
  //     o.identity.data["family_name"] = " ";
  //   }
  //
  //   if (!userData.includes("gender")) {
  //     o.profile_extended.data["gender"] = " ";
  //   }
  //   if (!userData.includes("date_of_birth")) {
  //     o.profile_extended.data["date_of_birth"] = " ";
  //   }
  //   if (!userData.includes("user_img")) {
  //     o.profile_extended.data["user_img"] = " ";
  //   }
  // }
  //
  // // console.log("data mapper data: xd:", { o });
  // let i = 0;
  // for (const scope in o) {
  //   for (const key in o[scope].data) {
  //     if (
  //       key != "timestamp" &&
  //       key != "user_id" &&
  //       key != "source" &&
  //       key != "loa" &&
  //       key != "updated_at"
  //     ) {
  //       if (o[scope].data[key] != "" && o[scope].data[key] != " ") {
  //         i++;
  //         break;
  //       }
  //     }
  //   }
  //   if (i === 0) delete o[scope];
  //   i = 0;
  // }

  return o;
};

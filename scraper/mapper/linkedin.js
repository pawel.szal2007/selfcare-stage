exports.map = (data) => {
  const o = data;

  o["source"] = "linkedin";
  o["loa"] = "medium";

  // data["name"] ? (o["gender"] = data["Płeć"]) : (o["gender"] = "");
  if (data["name"]) {
    const name = data["name"];
    const arr = name.split(" ");
    if (arr.length === 2) {
      // pierwsze imie a drugie nazwisko "name", "given_name", "middle_name"
      o["name"] = arr[1];
      o["given_name"] = arr[0];
    } else if (arr.length === 3) {
      // pierwsze imie, drugie imie i nazwisko
      o["name"] = arr[2];
      o["given_name"] = arr[0] + " " + arr[1];
      o["middle_name"] = arr[1];
    }
  }

  data["img"] ? (o["picture"] = data["img"]) : (o["picture"] = "");

  if (data["place_of_residance"]) {
    const place = data["place_of_residance"];
    const arr = place.split(" ");
    o["city"] = arr[0].replace(/[^a-zA-Z ]/g, "");
  }

  return o;
};

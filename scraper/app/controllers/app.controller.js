const Storage = require("@google-cloud/storage");
const storage = new Storage.Storage();
const ScraperController = require("../../scraper/controllers/scraper.controller");
const path = require("path");

exports.webScraping = (filePath, website) => {
  return new Promise(async (resolve, reject) => {
    try {
      const data = await ScraperController.getData(filePath, website);
      if (JSON.stringify(data) === JSON.stringify({})) {
        resolve("Brak danych");
      }
      resolve(data);
    } catch (err) {
      reject(err);
    }
  });
};

exports.downloadFile = (bucket, filePath) => {
  return new Promise(async (resolve, reject) => {
    const filePathSplit = filePath.split("/");
    filePath.split(".").pop() != "mhtml" &&
      filePath.split(".").pop() != "html" &&
      reject("Wskazany plik nie jest plikiem MHTML lub HTML");
    const destFileName = path.join(
      __dirname,
      "..",
      "..",
      "files",
      filePathSplit[filePathSplit.length - 1]
    );
    try {
      const options = {
        destination: destFileName,
      };
      await storage.bucket(bucket).file(filePath).download(options);
      // console.log(`gs://${bucket}/${filePath} downloaded to ${destFileName}.`);
      resolve(destFileName);
    } catch (err) {
      // console.log(err);
      reject(err);
    }
  });
};

exports.uploadData = (bucketName, bucketPath, service, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const today = new Date();
      const ms = today.getTime();
      let buff = new Buffer(`${service}_${today}_${ms}`);
      let name = buff.toString("base64");
      const path = `${bucketPath.split("/")[0]}/json_data/${service}`;

      await storage
        .bucket(`gs://${bucketName}/`, {
          // user id a potem sciezka z jsonami i źródłem odpowiednim
          destination: path,
        })
        .file(`${path}/${name}.json`)
        .save(JSON.stringify(data), {
          timestamp: ms,
        });
      resolve(encodeURI(`${path}/${name}.json`));
    } catch (err) {
      reject(err);
    }
  });
};

exports.publishDataToTopic = (topic) => {
  return new Promise(async (resolve, reject) => {
    const { PubSub } = require("@google-cloud/pubsub");
    const pubSubClient = new PubSub();

    // console.log('[topic] publish data');
    const topicName = topic.topicName;
    const topicData = topic.data;
    // console.log('topic name: ', { topicName });
    // console.log('topic data: ', { topicData });
    const dataBuffer = Buffer.from(JSON.stringify(topicData));
    try {
      const messageId = await pubSubClient.topic(topicName).publish(dataBuffer);
      // console.log(`Message ${messageId} published.`);
      resolve();
    } catch (error) {
      // console.error(`Received error while publishing: ${error.message}`);
      process.exitCode = 1;
      reject(error);
    }
  });
};

exports.saveImage = (userId, bucketName, topicData) => {
  return new Promise(async (resolve, reject) => {
    try {
      const base64Image = topicData.picture.split("base64,")[1];
      const fs = require("fs");
      const buffer = Buffer.from(base64Image, "base64");
      const date = new Date().getTime();
      const filePath = path.join(
        __dirname,
        "..",
        "..",
        "files",
        `user_rdo_img_${date}.jpg`
      );
      // console.log("filepath scraper test moj xd: ", { filePath });
      fs.writeFileSync(filePath, buffer);
      const imagePath = await uploadImage(
        bucketName,
        `${userId}/images`,
        filePath
      );
      topicData.picture = imagePath;
      await fs.unlinkSync(filePath);
      resolve();
    } catch (err) {
      reject(err);
    }
  });
};

const uploadImage = (bucket, bucketPath, filePath) => {
  return new Promise(async (resolve, reject) => {
    await storage
      .bucket(`gs://${bucket}/`)
      .upload(filePath, {
        destination: `${bucketPath}/user-image.jpg`,
      })
      .then(() => {
        // console.log(`${file.tempFilePath} uploaded to gs://${bucket}/`);
        // `https://storage.cloud.google.com/${bucket}/${bucketPath}/user-image.jpg`
        resolve(`${bucketPath}/user-image.jpg`);
      })
      .catch((err) => {
        // console.log({ err });
        reject(err);
      });
  });
};

exports.dataMapper = async (mapperData) => {
  return new Promise((resolve) => {
    switch (mapperData.service) {
      case "gov_rdo":
        resolve(mapper(mapperData));
        break;
      case "gov_pesel":
        resolve(mapper(mapperData));
        break;
      case "gov_nl":
        resolve(mapper(mapperData));
        break;
      case "linkedin":
        resolve(mapper(mapperData));
        break;
      default:
        resolve({});
        // throw new Error(
        //   `service: ${mapperData.service} cannot be mapped to BigQuery data schema`
        // );
        break;
    }
  });
};

const mapper = (mapperData) => {
  // console.log(`[mapper] ${mapperData.service} mapper`);
  const mapper = require(`../../mapper/${mapperData.service}`);
  const data = mapper.map(mapperData.data);
  // console.log(' ===== mapped data ==== ', {data});
  return data;
};

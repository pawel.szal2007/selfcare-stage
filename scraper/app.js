(async () => {
  const AppController = require("./app/controllers/app.controller");
  const fs = require("fs");
  // const { getHash } = require("./crypto/crypto");

  let bucketName;
  let bucketPath;
  let service;
  let userId;

  // data that user choose to update
  // let data;
  // console.log(`jestem w skraperze?????`);
  for (const parameter of process.argv) {
    if (parameter.includes("bucketName")) {
      const value = parameter.split("=")[1];
      bucketName = value;
    }
    if (parameter.includes("bucketPath")) {
      const value = parameter.split("=")[1];
      bucketPath = value;
      userId = value.split("/")[0];
    }
    if (parameter.includes("service")) {
      const value = parameter.split("=")[1];
      service = value;
    }
    // if (parameter.includes("data")) {
    //   const value = parameter.split("=")[1];
    //   data = value;
    // }
  }

  if (!bucketName) throw new Error("no bucketName parameter");
  if (!bucketPath) throw new Error("no bucketPath parameter");
  if (!service) throw new Error("no service parameter");

  try {
    // download file from storage
    const fileToScrape = await AppController.downloadFile(
      bucketName,
      bucketPath
    );

    // console.log("i have file to scrape...", fileToScrape);
    // scrape file according to service like: "gov_rdo", "gov_pesel" and another [...to be done]
    const dataObj = await AppController.webScraping(fileToScrape, service);

    // trzeba wrzucic do storage json z tymi danymi...
    const jsonPath = await AppController.uploadData(
      bucketName,
      bucketPath.substr(0, bucketPath.lastIndexOf("/")),
      service,
      dataObj
    );

    // console.log("i have data object... ", dataObj);
    // map scraped data according to BigQuery table labels/fields with topicName where the data should be published
    let topicData = {};

    // if (service != "linkedin") {
    topicData = await AppController.dataMapper({
      data: dataObj,
      service: service,
    });
    // } else {
    //   topicData.loa = "medium";
    // }

    // if (JSON.stringify(topicData) != JSON.stringify({})) {
    topicData.jsonStoragePath = jsonPath;

    // console.log("i have topicData...", topicData);
    // console.log("scra  per topic data moje xd: ", { topicData });
    // save user img to bucket storage if needed
    if (topicData.picture != undefined && topicData.picture != " ") {
      try {
        await AppController.saveImage(userId, bucketName, topicData);
      } catch (err) {
        // .....
      }
    }
    // }

    // send data to topic on GCP
    // await AppController.publishDataToTopic(topicData)

    // remove file to scrape
    await fs.unlinkSync(fileToScrape);

    // console.log("i have removed a file...");
    // get hash of data and send to app
    // const hash = await getHash(JSON.stringify(topicData.data));

    // log data with hash
    // if (JSON.stringify(topicData) != JSON.stringify({})) {
    console.log(JSON.stringify(topicData));
    // } else console.log(JSON.stringify(dataObj));
  } catch (err) {
    console.log(err);
  }
})();

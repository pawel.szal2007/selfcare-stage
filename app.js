//jshint esversion:6
let env = require("dotenv").config();
const dotenvParseVariables = require("dotenv-parse-variables");
env = dotenvParseVariables(env.parsed);
// Logger.info(env)

// const { projectVariable } = require("./configuration");
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const csrf = require("csurf");
const session = require("express-session");
const Logger = require("./winston-config");
const cors = require("cors");
const ejs = require("ejs");
const bcrypt = require("bcrypt");
const path = require("path");
const requestCountry = require("request-country");
const gcBigquery = require("./google_cloud/google.cloud.bigquery");
const gcPostgresql = require("./google_cloud/google.cloud.postgresql");
const gcStorage = require("./google_cloud/google.cloud.storage");
const morgan = require("morgan");
const jwtDecode = require("jwt-decode");
const _ = require("lodash");
var https = require("https");
const fs = require("fs");
const basicAuth = require("express-basic-auth");
const saltRounds = 10;
const {
  language,
} = require(`${__dirname}/language_dictionary/lang_dictionary.js`);
const fileUpload = require("express-fileupload");
const WebSocket = require("./websocket/websocket");

var logDirectory = path.join(__dirname, "logs");
// ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

const app = express();

var csrfProtect = csrf({
  cookie: true,
});

let ws;

app.set("view engine", "ejs");

app.set("views", __dirname + "/assets/views");

app.use(
  morgan(
    ":method :url HTTP/:http-version :user-agent :status :res[content-length] - :response-time ms",
    {
      stream: Logger.stream,
    }
  )
);

// ////////////////////////////////////////////////////////////////////////

// var allowedOrigins = [
//   "https://sandbox-consent.banqware.com",
//   "https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css",
//   "https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css",
//   "https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css",
//   "https://cdn.datatables.net",
//   "https://unpkg.com",
// ];
//
// app.use(
//   cors({
//     origin: function (origin, callback) {
//       // allow requests with no origin
//       // (like mobile apps or curl requests)
//       if (!origin) return callback(null, true);
//       if (allowedOrigins.indexOf(origin) === -1) {
//         var msg =
//           "The CORS policy for this site does not " +
//           "allow access from the specified Origin.";
//         return callback(new Error(msg), false);
//       }
//       return callback(null, true);
//     },
//   })
// );

// //////////////////////////////////////////////////////////////////////////

app.use(
  cors({
    origin: [
      "https://sandbox-consent.banqware.com",
      "https://www.facebook.com",
    ],
  })
);

// app.use(
//   cors({
//     origin: [
//       "https://sandbox-consent.banqware.com",
//       "https://unpkg.com",
//       " https://cdn.datatables.net",
//       "https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css",
//     ],
//     methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
//     preflightContinue: false,
//     optionsSuccessStatus: 204,
//   })
// );

app.use(
  bodyParser.urlencoded({
    limit: "100mb",
    parameterLimit: 50000,
    extended: true,
  })
);

app.use(
  bodyParser.json({
    limit: "100mb",
  })
);

// app.use(
//   basicAuth({
//     users: {
//       admin: "$2y$10$gxWOeTPktqwiuDs7hrFEjuX4KTI/SgTGX9E0qTfrsDdkNnPc8gV1G",
//     },
//     challenge: true, // <--- needed to actually show the login dialog!
//   })
// );

var cacheControl = require("express-cache-controller");

app.use(
  cacheControl({
    noCache: true,
    noStore: true,
    mustRevalidate: true,
    maxAge: 0,
  })
);

app.use(
  fileUpload({
    useTempFiles: true,
    tempFileDir: "./tmp/",
  })
);

app.use(bodyParser.json());

app.use(cookieParser());

app.use(express.static("assets/public"));

// ###################### OPTION 1 #####################
// function AccessLogger(n, t, blockTime) {
//   this.qty = n;
//   this.time = t;
//   this.blockTime = blockTime;
//   this.requests = {};
//   // schedule cleanup on a regular interval (every 30 minutes)
//   this.interval = setInterval(this.age.bind(this), 30 * 60 * 1000);
// }
//
// AccessLogger.prototype = {
//   check: function (ip) {
//     var info, accessTimes, now, limit, cnt;
//
//     // add this access
//     this.add(ip);
//
//     // should always be an info here because we just added it
//     info = this.requests[ip];
//     accessTimes = info.accessTimes;
//
//     // calc time limits
//     now = Date.now();
//     limit = now - this.time;
//
//     // short circuit if already blocking this ip
//     if (info.blockUntil >= now) {
//       return false;
//     }
//
//     // short circuit an access that has not even had max qty accesses yet
//     if (accessTimes.length < this.qty) {
//       return true;
//     }
//     cnt = 0;
//     for (var i = accessTimes.length - 1; i >= 0; i--) {
//       if (accessTimes[i] > limit) {
//         ++cnt;
//       } else {
//         // assumes cnts are in time order so no need to look any more
//         break;
//       }
//     }
//     if (cnt > this.qty) {
//       // block from now until now + this.blockTime
//       info.blockUntil = now + this.blockTime;
//       return false;
//     } else {
//       return true;
//     }
//   },
//   add: function (ip) {
//     var info = this.requests[ip];
//     if (!info) {
//       info = { accessTimes: [], blockUntil: 0 };
//       this.requests[ip] = info;
//     }
//     // push this access time into the access array for this IP
//     info.accessTimes.push[Date.now()];
//   },
//   age: function () {
//     // clean up any accesses that have not been here within this.time and are not currently blocked
//     var ip,
//       info,
//       accessTimes,
//       now = Date.now(),
//       limit = now - this.time,
//       index;
//     for (ip in this.requests) {
//       if (this.requests.hasOwnProperty(ip)) {
//         info = this.requests[ip];
//         accessTimes = info.accessTimes;
//         // if not currently blocking this one
//         if (info.blockUntil < now) {
//           // if newest access is older than time limit, then nuke the whole item
//           if (
//             !accessTimes.length ||
//             accessTimes[accessTimes.length - 1] < limit
//           ) {
//             delete this.requests[ip];
//           } else {
//             // in case an ip is regularly visiting so its recent access is never old
//             // we must age out older access times to keep them from
//             // accumulating forever
//             if (accessTimes.length > this.qty * 2 && accessTimes[0] < limit) {
//               index = 0;
//               for (var i = 1; i < accessTimes.length; i++) {
//                 if (accessTimes[i] < limit) {
//                   index = i;
//                 } else {
//                   break;
//                 }
//               }
//               // remove index + 1 old access times from the front of the array
//               accessTimes.splice(0, index + 1);
//             }
//           }
//         }
//       }
//     }
//   },
// };
//
// var accesses = new AccessLogger(10, 1000, 15000);
//
// // put this as one of the first middleware so it acts
// // before other middleware spends time processing the request
// app.use((req, res, next) => {
//   if (!accesses.check(req.connection.remoteAddress)) {
//     // cancel the request here
//     return res.end("No data for you!");
//     // or
//     // return res.redirect("/logout");
//   } else {
//     next();
//   }
// });

// ############ OPTION 2 #####################
// const LimitingMiddleware = require("limiting-middleware");
// app.use(
//   new LimitingMiddleware({ limit: 10, resetInterval: 1200000 }).limitByIp()
// );

// ################# OPTION 3 ###################
const rateLimit = require("express-rate-limit");
const limiter = rateLimit({
  windowMs: 60 * 1000, // 1000ms -> 1s
  max: 15, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
  standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
  legacyHeaders: false, // Disable the `X-RateLimit-*` headers
});

// Apply the rate limiting middleware to all requests
// app.use(limiter);

// app.use("assets/public/js", [
//   basicAuth({
//     users: {
//       admin: "$2y$10$gxWOeTPktqwiuDs7hrFEjuX4KTI/SgTGX9E0qTfrsDdkNnPc8gV1G",
//     },
//     challenge: true, // <--- needed to actually show the login dialog!
//   }),
//   express.static("assets/public/js"),
// ]);

const connectRedis = require("connect-redis");
const RedisStore = connectRedis(session);
const redis = require("redis");
let redisClient;

// Logger.info(process.env.NODE_ENV);

if (process.env.NODE_ENV === "development") {
  redisClient = redis.createClient({
    host: env.REDIS_HOST,
    port: env.REDIS_PORT,
  });
} else {
  redisClient = redis.createClient({
    host: env.REDIS_HOST,
    port: parseInt(env.REDIS_PORT),
    auth_pass: env.REDIS_AUTH_PASS,
    tls: {
      ca: [Buffer.from(fs.readFileSync(env.REDIS_CA_PATH), "base64")],
    },
  });
}

redisClient.on("error", (err) => {
  Logger.error(
    `Could not establish a connection with redis app session: ${err}`
  );
});

redisClient.on("connect", () => {
  Logger.info(`Connected to app session redis successfully`);
});

app.use(
  session({
    store: new RedisStore({
      client: redisClient,
    }),
    secret: env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    key: "JSESSIONID",
    cookie: {
      secure: true,
      httpOnly: true,
      sameSite: "lax",
    },
  })
);

// HELMET
const helmet = require("helmet");
// {
//   contentSecurityPolicy: false,
//   crossOriginEmbedderPolicy: false,
//   crossOriginResourcePolicy: { policy: "cross-origin" },
// }
app.use(helmet({ crossOriginEmbedderPolicy: false }));

var ONE_YEAR = 31536000000;
app.use(
  helmet.hsts({
    maxAge: ONE_YEAR,
    includeSubDomains: true,
    force: true,
  })
);

const { expressCspHeader, INLINE, NONE, SELF } = require("express-csp-header");
app.use(
  expressCspHeader({
    directives: {
      "default-src": [
        SELF,
        INLINE,
        "https://cdn.jsdelivr.net",
        "https://cdn.datatables.net",
        "https://cdn.jsdelivr.net",
        "https://cdnjs.cloudflare.com",
        "https://fonts.googleapis.com",
        "https://unpkg.com",
        "https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css",
        "https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js",
        "https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css",
        "https://fonts.gstatic.com",
        "https://cdn.jsdelivr.net",
        "https://unpkg.com/boxicons@2.1.2/dist/boxicons.js",
        "https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css",
        "https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js",
        "https://authstg.phoneid.co/phoneid.js",
        "https://authstg.phoneid.co/js/phoneIdMcIDP/phoneIdAuth/phoneIdAuth.js",
        "https://authstg.phoneid.co/js/phoneIdMcIDP/phoneIdAuth/spinners_6.css",
        "https://connect.facebook.net/pl_PL/sdk.js",
        "https://www.facebook.com",
      ],
      "script-src": [
        "https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js",
        "https://cdn.jsdelivr.net",
        "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js",
        "https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css",
        "https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js",
        "https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css",
        "https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js",
        "https://unpkg.com",
        "https://unpkg.com/boxicons@2.1.2/dist/boxicons.js",
        "https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css",
        "https://authstg.phoneid.co/phoneid.js",
        "https://authstg.phoneid.co/js/phoneIdMcIDP/phoneIdAuth/phoneIdAuth.js",
        "https://connect.facebook.net/pl_PL/sdk.js",
        "https://www.facebook.com",
        SELF,
        INLINE,
      ],
      "style-src": [
        SELF,
        INLINE,
        "https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css",
        "https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css",
        "https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css",
        "https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css",
        "https://cdn.datatables.net",
        "https://cdn.jsdelivr.net",
        "https://cdnjs.cloudflare.com",
        "https://fonts.googleapis.com",
        "https://unpkg.com",
        "https://unpkg.com/boxicons@2.1.2/dist/boxicons.js",
        "https://www.facebook.com",
      ],
      "img-src": [
        SELF,
        INLINE,
        // "https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css",
        "data:",
        "*",
        "unsafe-inline",
        // "https://unpkg.com/boxicons@2.1.2/dist/boxicons.js",
        // "https://upload.wikimedia.org/wikipedia/commons/7/73/Logo_symbol_PW_02.png",
        // "https://www.funduszeeuropejskie.gov.pl/media/76220/FE_barwy_RP_UE_EFSI.png",
        // "https://platform-lookaside.fbsbx.com",
        // "https://www.facebook.com",
        // "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=4273464152688838&height=50&width=50&ext=1655816271&hash=AeREujUF4ijb8gmb46w",
      ],
      "worker-src": [NONE],
      "block-all-mixed-content": true,
    },
  })
);

app.disable("x-powered-by");

// POSTGRESQL
const knex = gcPostgresql.connect();
// Logger.info({ knex });
// is user authenticated
const isAuthenticated = (req, res, next) => {
  if (req.session.tokens) {
    // Logger.info("[user logged in]");
    next();
  } else {
    // Logger.info("[user not logged in]");
    res.redirect("/");
  }
};

const getLanguage = (req) => {
  let lang;
  if (req.query.lang) {
    lang = req.query.lang;
  } else if (req.headers["accept-language"]) {
    lang = req.headers["accept-language"].split(",")[0].split("-")[0];
    if (lang != "pl" && lang != "eng") {
      lang = "eng";
    }
  } else lang = "eng";

  if (!lang) lang = "eng";
  Logger.info("language:", {
    lang,
  });
  return lang;
};

const saveTokens = (response) => {
  Logger.info("saving tokens");
  const token_type = response.data.token_type;
  const access_token = response.data.access_token;
  const expires_in = response.data.expires_in;
  const scope = response.data.scope;
  const refresh_token = response.data.refresh_token;
  const id_token = response.data.id_token.id_token
    ? response.data.id_token.id_token
    : response.data.id_token;

  const data = {
    token_type: token_type,
    access_token: access_token,
    expires_in: expires_in,
    scope: scope,
    refresh_token: refresh_token,
    id_token: id_token,
  };

  const jwt = require("jsonwebtoken");
  const tokens = jwt.sign(data, env.SECRET);
  return tokens;
};

const userIdToken = (userId) => {
  const jwt = require("jsonwebtoken");
  const data = {
    userId: userId,
  };
  const token = jwt.sign(data, env.SECRET);
  return token;
};

const refreshTokenRequest = (req) => {
  return new Promise((resolve, reject) => {
    const refresh_token = jwtDecode(req.session.tokens).refresh_token;
    const axios = require("axios");
    axios
      .post(`${env.OPEN_ID_REFRESH_TOKEN}&refresh_token=${refresh_token}`)
      .then((response) => {
        Logger.info("refresh token response: ", response.data);
        resolve(response);
      })
      .catch((err) => {
        Logger.error("refresh token error: ", err);
        reject(err);
      });
  });
};

// LOGIN
app.get("/", csrfProtect, async (req, res) => {
  // Logger.info(req.connection.remoteAddress.replace(/^.*:/, ""));
  // Logger.info(req.connection.remotePort);
  Logger.info(req.connection.localAddress.replace(/^.*:/, ""));
  Logger.info(req.connection.localPort);
  // const redirect = req.query.redirect;

  // trying to redirect the connection
  // let remoteAddress = "";
  // const localAddress = req.connection.localAddress.replace(/^.*:/, "");
  // const localPort = req.connection.localPort;
  // if (localAddress === "10.205.2.11") remoteAddress = "10.205.2.21";
  // if (localAddress === "10.205.2.21") remoteAddress = "10.205.2.11";
  // const url = `http://${remoteAddress}/proxy/networkDriveProxy`;
  // Logger.info(`Redirecting to second VM...`);
  // Logger.info({ remoteAddress });
  // Logger.info({ url });
  // process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
  // const axios = require("axios");
  // axios
  //   .get(url)
  //   .then((response) => {
  //     const data = response.data;
  //     console.log({ data });
  //     // resolve(data);
  //
  //   })
  //   .catch((err) => {
  //     // console.log({ err });
  //     const errObj = err.data;
  //     console.log({ errObj });
  //     // reject(errObj);
  //     res.render("login/login", { csrfToken: req.csrfToken() });
  //   });
  // res.setHeader(
  //   "Content-Security-Policy-Report-Only",
  //   "default-src 'self'; script-src 'report-sample' 'self' https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js; style-src 'report-sample' 'self' https://cdn.datatables.net https://cdn.jsdelivr.net https://cdnjs.cloudflare.com https://fonts.googleapis.com https://unpkg.com; object-src 'none'; base-uri 'self'; connect-src 'self'; font-src 'self' https://fonts.gstatic.com;frame-src 'self';img-src 'self';manifest-src 'self';media-src 'self';report-uri https://62a644d39bc141b6c536fe54.endpoint.csper.io/?v=2;worker-src 'none';"
  // );
  const lang = getLanguage(req);
  if (req.session.tokens) {
    res.redirect(`/main?lang=${lang}#home`);
  } else {
    res.render("login/login", {
      csrfToken: req.csrfToken(),
    });
  }
});

const userIsAllowed = (req, res, next) => {
  // -----------------------------------------------------------------------
  // authentication middleware

  const auth = {
    login: env.LOGIN,
    password: env.PASSWORD,
  };

  // parse login and password from headers
  const b64auth = (req.headers.authorization || "").split(" ")[1] || "";
  const [login, password] = Buffer.from(b64auth, "base64")
    .toString()
    .split(":");

  // Verify login and password are set and correct
  if (login && password && login === auth.login && password === auth.password) {
    // Access granted...
    return next();
  }

  // Access denied...
  return res.status(401).send("You are not allowed!");

  // -----------------------------------------------------------------------
};

app.all(
  [
    // "/gcs_files/:file",
    "/download/:file",
    "/js/:file",
    "/img/:file",
    "/jquery/:file",
    "/css/:file",
  ],
  userIsAllowed,
  (req, res, next) => {
    next();
  }
);

app.get("/wsEndpoint", csrfProtect, (req, res) => {
  return res.status(200).send(env.WEBSOCKET_ENDPOINT);
});

app.post("/logInTestUserAccount", csrfProtect, (req, res) => {
  const secretCode = req.body?.secret_code;

  if (!secretCode || secretCode === undefined) {
    return res.redirect("/");
  }

  const validTesterCode = env.TESTER_ACCOUNT_SECRET_CODE;
  if (secretCode != validTesterCode) {
    Logger.info("secret code and tester code are different");
    return res.redirect("/");
  }

  const testerUserId = env.TESTER_ACCOUNT_USER_ID;
  // console.log({ testerUserId });

  const idToken = userIdToken(testerUserId);
  // console.log({ idToken });
  req.session.uid = idToken;

  const tokens = saveTokens({
    data: {
      access_token: "",
      expires_in: "",
      scope: "",
      refresh_token: "",
      id_token: "",
    },
  });
  Logger.info("tokens saved");
  req.session.tokens = tokens;

  return res.redirect("/main");
});

// https://ssotest.phoneid.co/auth?client_id=cyber_test&redirect_uri=https://selfcare.topid.org/auth&response_type=code&scope=profile&nonce=12345
app.get("/OpenIDConnect", (req, res) => {
  res.redirect(env.OPEN_ID_CODE_ENDPOINT_LOGIN);
});

app.get("/registerWithOpenID", csrfProtect, (req, res) => {
  res.redirect(env.OPEN_ID_CODE_ENDPOINT_REGISTER);
});

app.get("/auth", csrfProtect, (req, res) => {
  const code = req.query.code;

  Logger.info("AUTHORIZATION GRANT REQUEST");
  const https = require("https");
  const axios = require("axios");
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

  axios
    .post(`${env.OPEN_ID_AUTHORIZATION_GRANT_ENDPOINT}`, {
      client_id: env.OPEN_ID_CLIENT_ID,
      code: code,
      redirect_uri: env.OPEN_ID_REDIRECT_URI_AUTH,
      grant_type: "authorization_code",
      client_secret: env.OPEN_ID_CLIENT_SECRET,
      httpsAgent: new https.Agent({
        rejectUnauthorized: false,
      }),
    })
    .then((response) => {
      Logger.info("AUTHORIZATION GRANT RESPONSE: ", {
        response,
      });
      const tokens = saveTokens(response);
      req.session.tokens = tokens;
      Logger.info("tokens saved");
      // save user id ---> userIdToken(userId) zwróci mi token ktory zapisuje do cookiesa pod "id"
      const userId = jwtDecode(response.data.id_token).user_id;
      const idToken = userIdToken(userId);
      req.session.uid = idToken;
      res.redirect(`/main`);
    })
    .catch((err) => {
      Logger.error("AUTHORIZATION GRANT ERROR: ", {
        err,
      });
      return res.redirect("/");
    });
});

app.get("/nlgov", (req, res) => {
  res.sendFile("assets/views/mijnOverheidIdentiteitBasis.html", {
    root: __dirname,
  });
});

app.get("/zoneinfo", (req, res) => {
  const countryCode = requestCountry(req);
  return res.status(200).send(countryCode);
});

/*
 **  POST w body: userId, sessionId, redirectUri, scope, loa
 **
 **
 */
app.post(
  "/businessClient",
  csrf({
    cookie: true,
    ignoreMethods: ["POST"],
  }),
  async (req, res) => {
    try {
      let scopes = req.body.scope;
      scopes && (scopes = scopes.split(" "));
      const loa = req.body.loa;
      const userId = req.body.userId;
      const sessionId = req.body.sessionId;
      const redirectUri = req.body.redirectUri;
      const lang = getLanguage(req);

      Logger.info(
        {
          scopes,
        },
        {
          loa,
        },
        {
          userId,
        },
        {
          sessionId,
        },
        {
          redirectUri,
        },
        {
          lang,
        }
        // { csrfToken }
      );

      if (!scopes)
        return res.json({
          error: 400,
          info: "body: scope not found",
        });
      if (!sessionId)
        return res.json({
          error: 400,
          info: "body: sessionId not found",
        });
      if (!redirectUri)
        return res.json({
          error: 400,
          info: "body: redirectUri not found",
        });
      if (!loa)
        return res.json({
          error: 400,
          info: "body: loa not found",
        });
      if (!userId)
        return res.json({
          error: 400,
          info: "body: userId not found",
        });
      if (loa != "low" && loa != "medium" && loa != "high")
        return res.json({
          error: 400,
          info: `loa: ${loa} is not recognized. Allowed: low, medium, high`,
        });
      if (
        !scopes.includes("profile") &&
        !scopes.includes("profile_extended") &&
        !scopes.includes("shopping") &&
        !scopes.includes("identity")
      ) {
        return res.json({
          error: 400,
          info: `scope: ${scopes} is not recognized. Allowed: profile, profile_extended, shopping, identity`,
        });
      }

      const newObj = [];
      const cookieData = [];
      for await (let scope of scopes) {
        const userData = await gcBigquery.selectUserScopeBigQueryData(
          userId,
          scope
        );
        const scope_columns = await gcBigquery.getTableColumns(scope);

        const createCookiesData = (scope_names_arr, data) => {
          const cookie_obj = [];
          for (const name of scope_names_arr) {
            cookie_obj.push({
              scope: language.eng.scope[scope],
              dataLabel: name,
              dataValue: data && data[name] ? `${data[name].value}` : "",
              loa: data && data[name] ? data[name].loa : "unknown",
              updated_at: data && data[name] ? `${data[name].updated_at}` : 0,
            });
          }
          return cookie_obj;
        };

        const cookie = createCookiesData(scope_columns, userData);
        cookieData.concat(cookie);

        if (cookie) {
          for (const item of cookie) {
            if (!newObj[item.scope]) newObj[item.scope] = [];
            newObj[item.scope].push(item);
          }
        }
      }

      const data64 = Buffer.from(JSON.stringify(cookieData)).toString("base64");
      Logger.info({
        data64,
      });

      const clientData = {
        sessionId: sessionId,
        redirectUri: redirectUri,
      };
      const clientData64 = Buffer.from(JSON.stringify(clientData)).toString(
        "base64"
      );

      const username = await getUsername(userId);
      // profile img
      let profile_url = await getProfileImage(userId);
      Logger.info({
        profile_url,
      });
      // try {
      //   const url = await gcStorage.downloadFile(profile_url);
      //   // Logger.info({ url });
      //   profile_url = url.split("/public/")[1];
      // } catch (err) {
      //   // ....
      // }

      // Logger.info({ newObj });
      req.session.client = clientData64;
      req.session.clipboard = data64;
      const idToken = userIdToken(userId);
      req.session.uid = idToken;
      return res.status(200).render("businessClient/clipboard-view", {
        data: newObj,
        dictionary: language[lang],
        loa_min: loa,
        username: username,
        profile_image: profile_url,
        csrfToken: req.csrfToken(),
      });
    } catch (err) {
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "server error",
      });
    }
  }
);

app.get("/menu/image", async (req, res) => {
  try {
    const userId = jwtDecode(req.session.uid)?.userId;
    let profile_url = await getProfileImage(userId);
    Logger.info({
      profile_url,
    });
    return res.status(200).send(profile_url);
  } catch (err) {
    Logger.error({
      err,
    });
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/businessClientUpdate", csrfProtect, (req, res) => {
  const lang = req.query.lang;
  const loa_min = req.body.data.loa_min;
  const newObj = [];
  const data = req.body.data.data;

  // Logger.info({ data });

  if (data) {
    for (const item of data) {
      if (!newObj[item.scope]) newObj[item.scope] = [];
      newObj[item.scope].push(item);
    }
  }

  return res.status(200).render("businessClient/content", {
    data: newObj,
    dictionary: language[lang],
    loa_min: loa_min,
  });
});

const getProfileImage = (userId) => {
  return new Promise(async (resolve) => {
    const record = await gcBigquery.getLabel(
      userId,
      "picture",
      "PROFILE_EXTENDED"
    );
    let profile_url = record[0]?.picture;

    try {
      const url = await gcStorage.downloadFile(profile_url);
      Logger.info({
        url,
      });
      profile_url = url.split("/public/")[1];
    } catch (err) {
      Logger.error({
        err,
      });
      // ....
    }

    resolve(profile_url);
  });
};

const getUsername = (userId) => {
  return new Promise(async (resolve) => {
    const record = await gcBigquery.getLabel(
      userId,
      "preferred_username",
      "PROFILE"
    );
    let username = record[0]?.preferred_username;
    // Logger.info({ username });
    if (!username) {
      // sprawdź bigquery z numerem telefonu i adres email
      // try {
      //   await gcPostgresql.connect();

      try {
        // get emails
        const emails = await gcPostgresql.userEmails(knex, {
          user_id: userId,
        });
        Logger.info(
          `there's no preffered username in bigquery, checking emails...`
        );
        if (!emails[0]?.email_address) {
          Logger.info(`there is no email address, checking phones...`);
          // get phones
          const phones = await gcPostgresql.userPhones(knex, {
            user_id: userId,
          });
          if (!phones[0]?.phone_number) {
            Logger.info(`there's no phones, no username founded...`);
          } else username = phones[0].phone_number;
        } else username = emails[0].email_address;
      } catch (err) {
        Logger.error(err);
      }
      // } catch (err) {
      //   Logger.error(err);
      // }

      // try {
      //   await gcPostgresql.close();
      // } catch (err) {
      //   Logger.error(err);
      // }
    }
    resolve(username);
  });
};

// jwtDecode(jwtDecode(req.session.tokens).id_token).given_name + " " + jwtDecode(jwtDecode(req.session.tokens).id_token).family_name,
app.get("/main", isAuthenticated, csrfProtect, async (req, res) => {
  // req.query.lang
  let lang = getLanguage(req);
  // get username from bigquery...
  const userId = jwtDecode(req.session.uid)?.userId;

  const username = await getUsername(userId);
  // await getUsername(userId);

  // profile img
  let profile_url = await getProfileImage(userId);
  Logger.info({
    profile_url,
  });

  // check security questions if there is no one - show a dialaog
  // try {
  //   await gcPostgresql.connect();
  // } catch (err) {
  //   Logger.error(err);
  // }

  const security_questions = await getAuthenticationFactors({
    user_id: userId,
    factors: ["security_questions"],
  });

  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }
  // check cookies clipboard and delete them
  // res.clearCookie("clipboard");
  Logger.info("Rendering main/home page");
  return res.status(200).render("main/home", {
    security_questions: security_questions,
    username: username ? username : "",
    dictionary: language[lang] ? language[lang] : language["eng"],
    profile_image: profile_url,
    csrfToken: req.csrfToken(),
  });
});

app.get("/proxy/networkDriveProxy", (req, res) => {
  Logger.info(`Redirected proxy connection...`);
  Logger.info(req.connection.remoteAddress.replace(/^.*:/, ""));
  Logger.info(req.connection.remotePort);
  Logger.info(req.connection.localAddress.replace(/^.*:/, ""));
  Logger.info(req.connection.localPort);
  return res.status(200).send("ok");
});

app.post("/home", isAuthenticated, csrfProtect, async (req, res) => {
  const lang = req.query.lang;
  // console.log("is it correct?", { lang });
  // return res.redirect(`/main?lang=${lang}`);
  // const givenName = jwtDecode(jwtDecode(req.session.tokens).id_token)
  //   .given_name;
  // const family_name = jwtDecode(jwtDecode(req.session.tokens).id_token)
  //   .family_name;
  const userId = jwtDecode(req.session.uid).userId;
  const username = await getUsername(userId);
  Logger.info("Rendering main/partials/home page");
  res.status(200).render("main/partials/home", {
    username: username ? username : "",
    dictionary: language[lang],
  });
});

// LOADING
app.post("/loading", (req, res) => {
  const lang = req.query.lang;
  Logger.info("[language]", {
    lang,
  });
  Logger.info("Rendering loading/loading-inner page");
  return res.status(200).render("loading/loading-inner", {
    loadingText: language[lang].loading,
  });
});

app.post("/clipboard", isAuthenticated, csrfProtect, (req, res) => {
  const lang = req.query.lang;
  const newObj = [];
  // console.log(req.body.data);
  if (req.body.data) {
    for (const item of req.body.data) {
      if (!newObj[item.scope]) newObj[item.scope] = [];
      newObj[item.scope].push(item);
    }
    // for (const [key, value] of Object.entries(newObj)) {
    //   Logger.info({ key });
    //   Logger.info({ value });
    // }
  }
  Logger.info("Rendering main/partials/clipboard page");
  return res.status(200).render("main/partials/clipboard", {
    data: newObj,
    dictionary: language[lang],
  });
});

app.post("/clipboard/send", csrfProtect, (req, res) => {
  const csvData = req.body.data.csv;
  const sender = req.body.data.sender;
  const recipient = req.body.data.recipient;
  const password = req.body.data.password;
  const base64Title = Buffer.from(jwtDecode(req.session.uid).userId).toString(
    "base64"
  );
  const csvFilePath = `${base64Title}.csv`;
  const zipFilePath = `./zip_files/${base64Title}.zip`;

  const nodemailer = require("nodemailer");
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    auth: {
      user: env.EMAIL_ACCOUNT[0],
      pass: env.EMAIL_ACCOUNT[1],
    },
  });

  // Logger.info({ csvData }, { sender }, { recipient }, { base64Title });

  try {
    const { parse } = require("json2csv");
    const csv = parse(csvData, {
      withBOM: true,
      excelStrings: false,
    });
    // console.log({ csv });
    fs.writeFile(csvFilePath, csv, "utf8", (err) => {
      if (err) {
        Logger.info(
          "Some error occured - file either not saved or corrupted file saved."
        );
        return res.status(500).send({
          status: 500,
          details: "csv file either not saved or corrupted",
        });
      } else {
        Logger.info("csv file saved!");
        const spawn = require("child_process").spawn;
        const zip = spawn("zip", ["-P", password, zipFilePath, csvFilePath]);
        zip.on("exit", (code) => {
          try {
            fs.unlinkSync(csvFilePath);
            Logger.info("csv file removed");
          } catch (err) {
            Logger.error("csv file not removed!");
          }

          if (code === 0) {
            transporter.sendMail(
              {
                from: sender,
                to: recipient,
                subject: `CyberID - dane użytkownika`,
                text: "W załączniku znajdują się gotowe dane.",
                html: `<b>W załączniku znajdują się gotowe dane do pobrania. Plik został zaszyfrowany hasłem podanym na platformie CyberID.</b>
                    <h4>Pozdrawiamy,</h4>
                    <p style="margin-top: 0.5rem">zespół CyberID</p>
                  `,
                attachments: [
                  {
                    path: zipFilePath,
                  },
                ],
              },
              (err, info) => {
                try {
                  fs.unlinkSync(zipFilePath);
                  Logger.info("zip file removed");
                } catch (err) {
                  Logger.error("zip file not removed!");
                }
                if (err) {
                  Logger.error("Error occurred. " + err.message);
                  return res.status(500).send({
                    status: 500,
                    details: "email was not send",
                    // message: err.message,
                  });
                }
                Logger.info("Message sent: %s", info.messageId);
                return res.status(200).send({
                  status: 200,
                  details: "email was sent",
                });
              }
            );
          } else {
            return res.status(500).send({
              status: 500,
              details: "zip file was not saved successfully",
            });
          }
        });
      }
    });
  } catch (err) {
    return res.status(500).send({
      status: 500,
      details: "external server error",
    });
  }
});

app.get("/path/:path", csrfProtect, (req, res) => {
  try {
    const userId = jwtDecode(req.session.uid).userId;
    const path = `${req.params.path.toUpperCase()}_URI`;
    res.send(`${env[path]}?id=${userId}`);
  } catch (err) {
    return res.status(404).send({
      error: 404,
      details: "path not found",
    });
  }
});

app.post("/krs/data", csrfProtect, (req, res) => {
  try {
    const name = req.body.data.name;
    const surname = req.body.data.surname;
    const nip = req.body.data.nip;
    const krsPersonId = req.body.data.krsPersonId;
    const checkedData = req.body.data.checkedData;
    const userId = jwtDecode(req.session.uid).userId;

    if (!name)
      return res.json({
        status: 400,
        details: "no name params found",
      });
    if (!surname)
      return res.json({
        status: 400,
        details: "cannot get user surname",
      });

    const krsURI = `${env.KRS_URI}`;
    let params;
    if (nip) {
      params = {
        firstName: name,
        lastName: surname,
        nip: nip,
      };
    } else if (krsPersonId) {
      params = {
        firstName: name,
        lastName: surname,
        krsNumber: krsPersonId,
      };
    }

    Logger.info("axios GET KRS data request");
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .get(krsURI, {
        params: params,
      })
      .then(async (response) => {
        const response_data = response.data;
        // Logger.info({ response_data });
        return res.status(200).send({
          status: 200,
          details: "ok",
          data: response_data,
        });
      })
      .catch((err) => {
        Logger.error(err);
        return res.status(400).send({
          status: "err",
          error: "server error",
        });
      });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

// app.get("/vat/data", csrfProtect, (req, res) => {
//   try {
//     const regon = req.query.regon;
//     const nip = req.query.nip;
//     const userId = jwtDecode(req.session.uid).userId;
//
//     if (!regon && !nip)
//       return res.json({
//         status: 400,
//         details: "no regon, NIP or bank account params found",
//       });
//
//     let vatObj;
//     if (nip)
//       vatObj = {
//         uri: env.VAT_NIP_URI,
//         params: {
//           nip: nip,
//         },
//       };
//     else if (regon)
//       vatObj = {
//         uri: env.VAT_REGON_URI,
//         params: {
//           regon: regon,
//         },
//       };
//
//     Logger.info("Axios GET VAT data request");
//     const axios = require("axios");
//     axios
//       .get(vatObj.uri, {
//         params: vatObj.params,
//       })
//       .then(async (response) => {
//         const response_data = response.data.result;
//         // Logger.info({ response_data });
//         const { convertToBigQuerySchema } = require("./mapper/bqMapper");
//         const checkedData = await getUserSelectedData(userId);
//         const bq_data = convertToBigQuerySchema({
//           userId: userId,
//           data: response_data,
//           checkedData: checkedData,
//         });
//         // Logger.info({ bq_data });
//         return res.status(200).send({
//           status: 200,
//           details: "okk",
//           data: bq_data,
//         });
//       })
//       .catch((err) => {
//         Logger.error(err);
//         return res.status(400).send({
//           status: "err",
//           error: "server error",
//         });
//       });
//   } catch (err) {
//     Logger.error(err);
//     return res.status(400).send({
//       status: "err",
//       error: "server error",
//     });
//   }
// });

app.post("/tests/krs", (req, res) => {
  try {
    const name = req.body?.name;
    const surname = req.body?.surname;
    const nip = req.body?.nip;
    const krsPersonId = req.body?.krsPersonId;

    if (!name) {
      Logger.error("Name param not found!");
      return res.json({
        status: 400,
        details: "name param not found",
      });
    }
    if (!surname) {
      Logger.error("Surname param not found!");
      return res.json({
        status: 400,
        details: "surname param not found",
      });
    }

    const krsURI = `${env.KRS_URI}`;
    let params;
    if (nip) {
      params = {
        firstName: name,
        lastName: surname,
        nip: nip,
      };
    } else if (krsPersonId) {
      params = {
        firstName: name,
        lastName: surname,
        krsNumber: krsPersonId,
      };
    }

    Logger.info(`GET request ${krsURI}, with params: ${params}`);
    const axios = require("axios");
    axios
      .get(krsURI, {
        params: params,
      })
      .then(async (response) => {
        const response_data = response.data;
        Logger.info(`DataApi server success response, data: ${response_data}`);
        return res.status(200).send(response_data);
      })
      .catch((err) => {
        Logger.error(err);
        return res.status(400).send({
          status: "err",
          error: "DataApi server error",
        });
      });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "Selfcare server error",
    });
  }
});

app.post("/tests/vat", (req, res) => {
  try {
    const regon = req.body?.regon;
    const nip = req.body?.nip;

    if (!regon && !nip) {
      Logger.error("Missing required parameters: regon, nip or bank account");
      return res.json({
        status: 400,
        details:
          "Missing required parameters: regon, NIP or bank account params not found",
      });
    }

    let vatObj;
    if (nip)
      vatObj = {
        uri: env.VAT_NIP_URI,
        params: {
          nip: nip,
        },
      };
    else if (regon)
      vatObj = {
        uri: env.VAT_REGON_URI,
        params: {
          regon: regon,
        },
      };

    // Logger.info({ vatObj });
    Logger.info(`GET request ${vatObj.uri} with parameters: ${vatObj.params}`);
    const axios = require("axios");
    axios
      .get(vatObj.uri, {
        params: vatObj.params,
      })
      .then(async (response) => {
        const response_data = response.data.result;
        Logger.info(`DataAPI server success response, data: ${response_data}`);
        return res.status(200).send(response_data);
      })
      .catch((err) => {
        Logger.error(err);
        return res.status(400).send({
          status: "err",
          error: "DataApi server error",
        });
      });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "Selfcare server error",
    });
  }
});

app.post("/tests/ceidg", (req, res) => {
  try {
    const regon = req.body?.regon;
    const nip = req.body?.nip;
    // Logger.info({ nip }, { regon });
    if (!regon && !nip) {
      Logger.error(
        "Missing required parameters: regon, NIP or bank account parameters not found"
      );
      return res.json({
        status: 400,
        details:
          "Missing required parameters: regon, NIP or bank account params not found",
      });
    }

    let ceidgObj;
    if (nip && regon) {
      ceidgObj = {
        uri: env.CEIDG_URI,
        params: {
          regon: regon,
          nip: nip,
        },
      };
    } else if (nip)
      ceidgObj = {
        uri: env.CEIDG_URI,
        params: {
          nip: nip,
        },
      };
    else if (regon)
      ceidgObj = {
        uri: env.CEIDG_URI,
        params: {
          regon: regon,
        },
      };

    Logger.info(
      `GET request ${ceidgObj.uri} with parameters: ${ceidgObj.params}`
    );
    const axios = require("axios");
    axios
      .get(ceidgObj.uri, {
        params: ceidgObj.params,
      })
      .then(async (response) => {
        const response_data = response.data;
        Logger.info(`DataAPI server success response, data: ${response_data}`);
        return res.status(200).send(response_data);
      })
      .catch((err) => {
        Logger.error(err);
        return res.status(400).send({
          status: "err",
          error: "DataApi server error",
        });
      });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "Selfcare server error",
    });
  }
});

app.post("/ceidg/data", csrfProtect, async (req, res) => {
  try {
    const regon = req.body.data?.regon;
    const nip = req.body.data?.nip;
    const checkedData = req.body.data?.checkedData;
    const userId = jwtDecode(req.session.uid).userId;

    if (!regon && !nip)
      return res.json({
        status: 400,
        details: "no regon, NIP or bank account params found",
      });

    let ceidgObj;
    if (nip && regon) {
      ceidgObj = {
        uri: env.CEIDG_URI,
        params: {
          limit: 1,
          page: 0,
          regon: regon,
          nip: nip,
        },
      };
    } else if (nip)
      ceidgObj = {
        uri: env.CEIDG_URI,
        params: {
          limit: 1,
          page: 0,
          nip: nip,
        },
      };
    else if (regon)
      ceidgObj = {
        uri: env.CEIDG_URI,
        params: {
          limit: 1,
          page: 0,
          regon: regon,
        },
      };

    Logger.info(`Axios GET CEIDG data request::`);
    // Logger.info({ ceidgObj });
    // const sendRequest = (CEIDGObj) => {
    // return new Promise((resolve) => {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .get(ceidgObj.uri, {
        params: ceidgObj.params,
      })
      .then(async (response) => {
        const data = response.data;
        // Logger.info({ data });
        // resolve(response);
        // response_data[0].concat(response[0]);
        const statusCode = data?.statusCode;
        if (statusCode && (statusCode === 204 || statusCode === 400)) {
          Logger.error(`Bad request error from CEIDG API or data not found`);
          // resolve(false);
          return res.status(500).send({
            status: "err",
            error: "external server error",
          });
        } else {
          const response_data = data[0][0];

          // deleting unneccessary data
          if (response_data["id"]) delete response_data["id"];
          if (response_data["adresyDzialalnosciDodatkowe"])
            delete response_data["adresyDzialalnosciDodatkowe"];
          if (response_data["adresKorespondencyjny"])
            delete response_data["adresKorespondencyjny"];
          if (response_data["obywatelstwa"])
            delete response_data["obywatelstwa"];
          if (response_data["pkd"]) delete response_data["pkd"];
          if (response_data["pkdGlowny"]) delete response_data["pkdGlowny"];
          if (response_data["numerStatusu"])
            delete response_data["numerStatusu"];
          if (response_data["zarzadcaSukcesyjny"])
            delete response_data["zarzadcaSukcesyjny"];
          if (response_data["uprawnienia"]) delete response_data["uprawnienia"];
          if (response_data["link"]) delete response_data["link"];

          return res.status(200).send({
            status: 200,
            details: "ok",
            data: response_data,
          });
        }
      })
      .catch((err) => {
        Logger.error(err);
        // resolve(false);
        return res.status(400).send({
          status: "err",
          error: "server error",
        });
      });
    // });
    // };
    //
    // let loop = true;
    // const ceidg_data = [[]];
    // while (loop) {
    //   Logger.info(`CEIDG loop request for page: ${ceidgObj.params.page}`);
    //   const response_data = await sendRequest(ceidgObj);
    //   if (response_data) {
    //     if (response_data?.statusCode && response_data?.statusCode === 204) {
    //       loop = false;
    //     } else {
    //       ceidg_data[0].concat(response_data[0]);
    //       ceidgObj.params.page++;
    //     }
    //   } else {
    //     return res.status(400).send({
    //       status: "err",
    //       error: "server error",
    //     });
    //   }
    // }
    //
    // Logger.info(`CEIDG data from API::`);
    // Logger.info({ ceidg_data });
    // return res.status(200).send({
    //   status: 200,
    //   details: "ok",
    //   data: ceidg_data,
    // });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/api/facebook", csrfProtect, (req, res) => {
  const accessToken = req.body.data.accessToken;
  const userId = jwtDecode(req.session.uid).userId;
  const FBUserId = req.body.data.userID;
  const checkedData = req.body.data?.checkedData;
  // Logger.info({ accessToken }, { FBUserId });
  // wywolanie janka i przekazanie mu tokena do apki...
  const params = {
    accessToken: accessToken,
    userId: FBUserId,
  };
  // Logger.info({ params });
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
  const axios = require("axios");
  axios
    .get(`${env.FACEBOOK_URI}`, {
      params: params,
    })
    .then(async (response) => {
      const response_data = response.data;
      // Logger.info({ response_data });
      const { convertToBigQuerySchema } = require("./mapper/bqMapper");

      try {
        await gcStorage.uploadJSONData(userId, "facebook", response_data);
      } catch (err) {
        // ...
      }

      // const checkedData = await getUserSelectedData(userId);
      // poprawki danych z fb....
      response_data.data.name = response_data.data.surname;
      response_data.data.gender = response_data.data.sex;
      response_data.data.locale = response_data.data.locality;
      response_data.data.picture = response_data.data.user_img;

      const bq_data = convertToBigQuerySchema({
        userId: userId,
        data: response_data.data,
        checkedData: checkedData,
      });
      Logger.info("Data converted to bigquery schema!");
      // console.log("loguje picture...", response_data.data.picture);
      return res.status(200).send({
        status: 200,
        details: "OK",
        data: bq_data,
      });
    })
    .catch((err) => {
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "server error",
      });
    });
  // return res.status(200).send("OK!");
});

app.post("/ocr/:documentType", csrfProtect, async (req, res) => {
  try {
    let file = req.files.file;
    const checkedData = req.body.checkedData;
    const documentType = req.params.documentType;
    const userId = jwtDecode(req.session.uid).userId;
    let tiffPath;
    file.name = file.name.replace(/ /g, "_");

    if (
      path.extname(file.name) != ".pdf" &&
      path.extname(file.name) != ".tiff"
    ) {
      if (
        (file.mimetype != "image/jpg" &&
          file.mimetype != "image/jpeg" &&
          file.mimetype != "image/png") ||
        file.size > 50000000
      ) {
        return res.status(400).send({
          error: "file err",
          info:
            "uploaded file is not correct - the file is too big or mimetype is not an image",
        });
      }
      Logger.info("converting to tiff");
      const { convertToTiff } = require("./OCR/file_converter");
      tiffPath = await convertToTiff(file);
      Logger.info({
        tiffPath,
      });
      file = {
        name: `${file.name}.tiff`,
        type: documentType,
        tempFilePath: tiffPath,
      };
    } else {
      file = {
        name: `${file.name}`,
        type: documentType,
        tempFilePath: file.tempFilePath,
      };
    }

    setTimeout(async () => {
      const { bucketPath } = await gcStorage.uploadFileToBucket(
        userId,
        "ocr",
        file
      );
      const { unlink } = require("fs/promises");
      tiffPath && (await unlink(tiffPath));
      Logger.info("File uploaded to bucket");
      try {
        const data = await runOCRScript(
          bucketPath,
          documentType,
          userId,
          checkedData
        );

        const { convertToBigQuerySchema } = require("./mapper/bqMapper");
        const bq_data = convertToBigQuerySchema({
          userId: userId,
          data: data,
          checkedData: checkedData.split(","),
        });
        return res.status(200).send(bq_data);
      } catch (err) {
        return res.status(400).send({
          error: "error occured during OCR process",
          info: err,
        });
      }
    }, 4000);
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      error: "error occured during OCR process",
      info: err,
    });
  }
});

app.post("/tests/ocr", async (req, res) => {
  try {
    let file = req.files?.file;
    const checkedData = "all";
    const documentType = req.body?.document_type;
    const userId = req.body?.user_id;
    let tiffPath;

    if (!documentType || documentType === undefined) {
      Logger.error(`Missing required parameters: document_type`);
      return res.status(404).send({
        error: 404,
        info: "Missing required parameters: document_type",
      });
    }

    if (!userId || userId === undefined) {
      Logger.error(`Missing required parameters: userId`);
      return res.status(404).send({
        error: 404,
        info: "Missing required parameters: userId",
      });
    }

    if (file && file != undefined) {
      Logger.info(
        `Processing OCR file for userId: ${userId}, document type: ${documentType}`
      );
      file.name = file.name.replace(/ /g, "_");

      if (
        path.extname(file.name) != ".pdf" &&
        path.extname(file.name) != ".tiff"
      ) {
        if (
          (file.mimetype != "image/jpg" &&
            file.mimetype != "image/jpeg" &&
            file.mimetype != "image/png" &&
            file.mimetype != "application/pdf") ||
          file.size > 50000000
        ) {
          Logger.error(
            "uploaded file is not correct - the file is too big or mimetype is not an image"
          );
          return res.status(400).send({
            error: "file err",
            info:
              "uploaded file is not correct - the file is too big or mimetype is not an image",
          });
        }
        Logger.info("Converting file to tiff");
        const { convertToTiff } = require("./OCR/file_converter");
        tiffPath = await convertToTiff(file);
        Logger.info(`File converted: ${tiffPath}`);
        file = {
          name: `${file.name}.tiff`,
          type: documentType,
          tempFilePath: tiffPath,
        };
      } else {
        file = {
          name: `${file.name}`,
          type: documentType,
          tempFilePath: file.tempFilePath,
        };
      }

      setTimeout(async () => {
        Logger.info(`Uploading file to storage`);
        const { bucketPath } = await gcStorage.uploadFileToBucket(
          userId,
          "ocr",
          file
        );
        const { unlink } = require("fs/promises");
        tiffPath && (await unlink(tiffPath));
        Logger.info(`File uploaded to storage bucket: ${bucketPath}`);
        try {
          const data = await runOCRScript(
            bucketPath,
            documentType,
            userId,
            checkedData
          );

          // const { convertToBigQuerySchema } = require("./mapper/bqMapper");
          // const bq_data = convertToBigQuerySchema({
          //   userId: userId,
          //   data: data,
          //   checkedData: checkedData.split(","),
          // });
          Logger.info("Data extracted successfully");
          return res.status(200).send(data);
        } catch (err) {
          Logger.info(err);
          return res.status(400).send({
            error: "error occured during OCR process",
            info: err,
          });
        }
      }, 2000);
    } else {
      Logger.info("File was not found!");
      return res.status(400).send({
        error: 404,
        info: "file was not found",
      });
    }
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      error: "error occured during OCR process",
      info: err,
    });
  }
});

const runOCRScript = (bucketPath, documentType, userId, dataChecked) => {
  return new Promise(async (resolve, reject) => {
    const { exec } = require("child_process");
    await exec(
      `node ${__dirname}/OCR/index.js filePath=${bucketPath} fileType=${documentType} user_id=${userId} data=${dataChecked}`,
      (error, stdout, stderr) => {
        if (error !== null) {
          Logger.error("ocr exec error: ", {
            error,
          });
          reject(err);
        } else {
          Logger.info({
            stdout,
          });
          try {
            const data = JSON.parse(stdout);
            resolve(data);
          } catch (err) {
            reject(err);
          }
        }
      }
    );
  });
};

app.get("/networkDrive/login", isAuthenticated, async (req, res) => {
  const lang = await getLanguage(req);

  return res.render(`login/networkDriveLogin.ejs`, {
    dictionary: language[lang],
  });
});

app.get("/networkDrive/uniqueCode", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  const codeGenerator = require("./generator/code_generator");
  try {
    const code = await codeGenerator.getNetworkDriveCode(knex, userId);
    Logger.info("Network drive code generated");
    return res.status(200).send(code);
  } catch (err) {
    return res.status(400).send(`couldn't get network drive code`);
  }
});

app.get("/upToDateData", csrfProtect, async (req, res) => {
  try {
    const userData = await gcBigquery.selectUpToDateAllData(
      jwtDecode(req.session.uid).userId
    );
    return res.status(200).send(userData);
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/tests/userData", async (req, res) => {
  try {
    Logger.info(`Selecting user data for userId: ${req.body?.user_id}`);
    // Logger.info();
    const userData = await gcBigquery.selectUpToDateAllData(req.body?.user_id);
    Logger.info(`UserId: ${req.body?.user_id} - data downloaded successfully!`);
    return res.status(200).send(userData);
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.get("/settings/phone/add", csrfProtect, async (req, res) => {
  if (req.query.UserTag === undefined) {
    return res.status(400).send("error, no UserTag found");
  } else {
    Logger.info("UserTag: ", req.query.UserTag);
    let lang = getLanguage(req);
    const base64url = require("base64url");
    const buffer = Buffer.from(base64url.decode(req.query.UserTag), "base64");
    const privateKey = fs.readFileSync(
      "certificatesPHONEID/sk_cid_topID_BV_C_1777"
    );
    let tokens;
    const user_id = jwtDecode(req.session.uid).userId;
    // Logger.info({ user_id });

    const crypto = require("crypto");
    let pKeyO = {};
    pKeyO.key = privateKey;
    pKeyO.padding = crypto.constants.RSA_PKCS1_OAEP_PADDING;

    try {
      const decrypted = crypto.privateDecrypt(pKeyO.key, buffer);
      Logger.info("decrypted UserTag: ", decrypted.toString("ascii"));
      const jsonUser = JSON.parse(decrypted);
      // Logger.info("User MSISDN: ", jsonUser.userMSISDN);

      // const { projectVariable } = require("./configuration");
      const phone_number = jsonUser.userMSISDN;
      // wrzucam do bazy numer telefonu...
      // let emails,
      let phones, factors;

      phones = await gcPostgresql.selectAllPhoneNumbers(knex);

      // weryfikacja czy taki numer juz istnieje
      let number_exist = false;
      for (const phone of phones) {
        if (phone.phone_number === phone_number) {
          // istnieje
          number_exist = true;
          break;
        }
      }

      if (!number_exist && phones.length < 10) {
        await gcPostgresql.addPhone(knex, {
          user_id: user_id,
          phone_number: phone_number,
        });
      }
    } catch (err) {
      Logger.error(`PhoneId error: ${err}`);
    }

    try {
      const factors = await getAuthenticationFactors({
        user_id: user_id,
        factors: ["all"],
      });

      // wygenerowanie passphrase
      let passphrase;
      if (factors?.is_biometry_enrolled[0].is_biometry_enrolled) {
        passphrase = await generatePassphrase(user_id);
      } else {
        passphrase = {
          passphrase: language[lang].passphrase,
          currentTimestamp: "",
          passphraseExpirationTime: "",
        };
      }

      // CLOSE CONNECTION TO DB
      // try {
      //   await gcPostgresql.close();
      // } catch (err) {
      //   Logger.error(err);
      // }

      const username = await getUsername(user_id);
      // profile img
      let profile_url = await getProfileImage(user_id);
      Logger.info({
        profile_url,
      });
      // try {
      //   const url = await gcStorage.downloadFile(profile_url);
      //   // Logger.info({ url });
      //   profile_url = url.split("/public/")[1];
      // } catch (err) {
      //   // ....
      // }
      // pin password i biometria bedzie jako true or false czyli jest albo jej nie ma
      return res.status(200).render("phoneid/phoneid_redirect.ejs", {
        dictionary: language[lang],
        username: username ? username : "",
        emails: factors?.emails,
        phones: factors?.phones,
        pin: factors?.pin,
        password: factors?.password,
        isBiometryEnrolled: {
          status: factors?.is_biometry_enrolled[0].is_biometry_enrolled,
          passphrase: passphrase,
        },
        securityQuestions: factors?.security_questions,
        profile_image: profile_url,
        csrfToken: req.csrfToken(),
      });
    } catch (err) {
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "server error",
      });
    }
  }
});

app.post("/settings", isAuthenticated, csrfProtect, async (req, res) => {
  try {
    const lang = req.query.lang;
    !lang && (lang = "eng");

    const e_show = req.query.e_show;
    Logger.info({
      e_show,
    });
    const userId = jwtDecode(req.session.uid).userId;

    // try {
    //   await gcPostgresql.connect();
    // } catch (err) {
    //   Logger.error(err);
    // }

    const factors = await getAuthenticationFactors({
      user_id: userId,
      factors: ["all"],
    });

    // CLOSE CONNECTION TO DB
    // try {
    //   await gcPostgresql.close();
    // } catch (err) {
    //   Logger.error(err);
    // }

    // Logger.info({ factors });

    // wygenerowanie passphrase
    let passphrase;
    if (
      factors &&
      factors.is_biometry_enrolled.length > 0 &&
      factors?.is_biometry_enrolled[0].is_biometry_enrolled
    ) {
      passphrase = await generatePassphrase(userId);
    } else {
      passphrase = {
        passphrase: language[lang].passphrase,
        currentTimestamp: "",
        passphraseExpirationTime: "",
      };
    }

    Logger.info("Generated passphrase: ", {
      passphrase,
    });
    return res.status(200).render("main/partials/settings", {
      dictionary: language[lang],
      emails: factors?.emails,
      phones: factors?.phones,
      pin: factors?.pin,
      password: factors?.password,
      isBiometryEnrolled: {
        status:
          factors.is_biometry_enrolled.length > 0
            ? factors?.is_biometry_enrolled[0].is_biometry_enrolled
            : false,
        passphrase: passphrase,
      },
      securityQuestions: factors?.security_questions,
      e_show: e_show ? [e_show] : [],
    });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/settings/verifyPIN", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  const pin = req.body.data.pin;
  let dbPINHash;
  // console.log({ pin });
  if (!pin)
    return res.status(400).send({
      status: "err",
      error: "pin undefined",
      // more: "please type pin",
    });

  // try {
  //   await gcPostgresql.connect();
  // } catch (err) {
  //   Logger.error(err);
  // }

  try {
    const val = await gcPostgresql.userPIN(knex, {
      user_id: userId,
    });
    dbPINHash = val[0].pin;
  } catch (err) {
    Logger.error(err);
  }

  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }

  Logger.info({
    dbPINHash,
  });
  // console.log("porównuje piny... bcrypt");
  bcrypt.compare(pin, dbPINHash, (err, result) => {
    if (err) {
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "pin is not correct",
      });
    }

    if (result) {
      return res.status(200).send({
        status: "Success",
        info: "pin verification done",
      });
    } else {
      return res.status(400).send({
        status: "err",
        error: "pin is not correct",
      });
    }
  });
});

app.post("/tests/verifyPIN", async (req, res) => {
  const userId = req.body?.user_id;
  const pin = req.body?.pin;
  let dbPINHash;
  Logger.info(`Verifying PIN: ${pin} for userId: ${userId}`);
  if (!pin) {
    Logger.info("PIN was not defined!");
    return res.status(400).send({
      status: "err",
      error: "pin undefined",
    });
  }

  // try {
  //   await gcPostgresql.connect();
  // } catch (err) {
  //   Logger.error(err);
  // }
  Logger.info(`userId: ${userId} - Getting user PIN from postgreSQL`);
  try {
    const val = await gcPostgresql.userPIN(knex, {
      user_id: userId,
    });
    dbPINHash = val[0].pin;
  } catch (err) {
    Logger.error(err);
  }

  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }

  Logger.info(`userId: ${userId} - PIN hash from postgreSQL: ${dbPINHash}`);
  // console.log("porównuje piny... bcrypt");
  Logger.info(`userId: ${userId} -  Comparing PINs`);
  bcrypt.compare(pin, dbPINHash, (err, result) => {
    if (err) {
      Logger.error(`userId: ${userId} - PIN is not correct`);
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "pin is not correct",
        // more: "error with bcrypt compare function...",
      });
    }

    if (result) {
      Logger.info(`userId: ${userId} - PIN is correct`);
      return res.status(200).send({
        status: "Success",
        info: "pin is correct",
        // more: "pin is correct",
      });
    } else {
      Logger.error(`userId: ${userId} - PIN is not correct`);
      return res.status(400).send({
        status: "err",
        error: "pin is not correct",
        // more: "wrong pin",
      });
    }
  });
});

app.post("/settings/verifyPassword", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  const password = req.body.data.password;
  let dbPasswordHash;

  if (!password)
    return res.status(400).send({
      status: "err",
      error: "password undefined",
    });

  // try {
  //   await gcPostgresql.connect();
  // } catch (err) {
  //   Logger.error(err);
  // }

  try {
    const val = await gcPostgresql.userPassword(knex, {
      user_id: userId,
    });
    dbPasswordHash = val[0].password;
  } catch (err) {
    Logger.error(err);
  }

  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }

  bcrypt.compare(password, dbPasswordHash, async (err, result) => {
    if (err) {
      return res.status(400).send({
        status: "err",
        error: "bcrypt compare error",
      });
    }

    if (result) {
      return res.status(200).send({
        status: "Success",
        info: "password verification done",
      });
    } else {
      return res.status(400).send({
        status: "err",
        error: "bcrypt compare error",
      });
    }
  });
});

app.post("/settings/verifyQuestion", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  const question = req.body.data.question;
  const answer = req.body.data.answer;
  let dbAnswer;

  // Logger.info({ question }, { answer });

  if (!answer || !question)
    return res.status(400).send({
      status: "err",
      error: "answer or question undefined",
    });

  // try {
  //   await gcPostgresql.connect();
  // } catch (err) {
  //   Logger.error(err);
  // }

  try {
    const val = await gcPostgresql.userQuestion(knex, {
      user_id: userId,
      question: question,
    });
    Logger.info({
      val,
    });
    dbAnswer = val[0].answer;
  } catch (err) {
    Logger.error(err);
  }

  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }

  if (answer === dbAnswer) {
    return res.status(200).send({
      status: "Success",
      info: "question verification done",
    });
  } else {
    return res.status(400).send({
      status: "err",
      error: "compare error",
    });
  }
});

app.delete("/settings/userPIN", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  try {
    // await gcPostgresql.connect();

    const count = await authenticationFactorsCount(userId);
    Logger.info("authentication factors:: ", {
      count,
    });

    if (count > 1) {
      try {
        await gcPostgresql.deletePIN(knex, {
          user_id: userId,
        });
        // console.log("PIN zostal usuniety");
        res.status(200).send({
          status: "Success",
          info: "pin was deleted",
        });
      } catch (err) {
        Logger.error(err);
        res.status(400).send({
          status: "err",
          error: "pin was not deleted",
        });
      }
    } else {
      res.status(400).send({
        status: "AUTH COUNT",
        info: "NOT OK",
      });
    }
  } catch (err) {
    Logger.error(err);
  }

  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }
});

app.delete("/settings/userPassword", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;

  try {
    // await gcPostgresql.connect();

    const count = await authenticationFactorsCount(userId);
    Logger.info("authentication factors:: ", {
      count,
    });

    if (count > 1) {
      try {
        const val = await gcPostgresql.deletePassword(knex, {
          user_id: userId,
        });
        res.status(200).send({
          status: "Success",
          info: "password was deleted",
        });
      } catch (err) {
        Logger.error(err);
        res.status(400).send({
          status: "err",
          error: "password was not deleted",
        });
      }
    } else {
      res.status(400).send({
        status: "AUTH COUNT",
        info: "NOT OK",
      });
    }
  } catch (err) {
    Logger.error(err);
  }

  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }
});

app.delete("/settings/securityQuestion", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  const question = req.body.data.question;
  // Logger.info({ question });
  // try {
  //   await gcPostgresql.connect();
  // } catch (err) {
  //   Logger.error(err);
  // }

  try {
    await gcPostgresql.deleteSecurityQuestion(knex, {
      user_id: userId,
      question: question,
    });
    res.status(200).send({
      status: "Success",
      info: "security question was deleted",
    });
  } catch (err) {
    Logger.error(err);
    res.status(400).send({
      status: "err",
      error: "security question was not deleted",
    });
  }

  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }
});

app.post("/settings/changeQuestion", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  const replaced = req.body.data.replaced;
  const question = req.body.data.question;
  const answer = req.body.data.answer;

  // try {
  //   await gcPostgresql.connect();
  // } catch (err) {
  //   Logger.error(err);
  // }

  // SELECT EMAILS
  try {
    await gcPostgresql.updateUserQuestion(knex, {
      user_id: userId,
      replaced: replaced,
      question: question,
      answer: answer,
    });
    return res.status(200).send({
      status: "Success",
      info: "security question was updated",
    });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "security question was not updated",
    });
  }

  // CLOSE CONNECTION TO DB
  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }
});

app.get("/settings/email/refresh", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  let emails;
  // OPEN CONNECTION TO DATABASE
  // try {
  //   await gcPostgresql.connect();
  // } catch (err) {
  //   Logger.error(err);
  // }

  // SELECT EMAILS
  try {
    emails = await gcPostgresql.userEmails(knex, {
      user_id: userId,
    });
  } catch (err) {
    Logger.error(err);
  }

  // CLOSE CONNECTION TO DB
  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }

  return res.status(200).send(emails);
});

app.post(
  "/settings/changePIN",
  isAuthenticated,
  csrfProtect,
  async (req, res) => {
    try {
      const userId = jwtDecode(req.session.uid).userId;
      const oldPIN = req.body.data.old_pin;
      const newPIN = req.body.data.new_pin;
      let dbPINHash = "";
      // oldPINHash = "";
      // Logger.info({ oldPIN, newPIN });

      if (!newPIN)
        return res.status(400).send({
          status: "err",
          error: "new pin undefined",
        });

      if (!pin_validation(newPIN)) {
        return res.status(400).send({
          status: "err",
          error: "pin validation failed",
        });
      }

      // try {
      //   await gcPostgresql.connect();
      // } catch (err) {
      //   Logger.error(err);
      // }

      try {
        const val = await gcPostgresql.userPIN(knex, {
          user_id: userId,
        });
        dbPINHash = val[0].pin;
      } catch (err) {
        Logger.error(err);
      }

      Logger.info({
        dbPINHash,
      });

      const savePINToDB = () => {
        bcrypt.hash(newPIN, saltRounds, async (err, hash) => {
          try {
            await gcPostgresql.setPIN(knex, {
              user_id: userId,
              pin: hash,
            });
            // try {
            //   await gcPostgresql.close();
            // } catch (err) {
            //   Logger.error(err);
            // }
            return res.status(200).send({
              status: "Success",
              info: "pin changed",
            });
          } catch (err) {
            Logger.error(err);
          }
        });
      };

      if (dbPINHash === null || dbPINHash === "") {
        savePINToDB();
      } else {
        bcrypt.compare(oldPIN, dbPINHash, async (err, result) => {
          if (err) {
            Logger.error({
              err,
            });
            return res.status(200).send({
              status: "err",
              error: "bcrypt compare error",
            });
          }

          if (result) {
            savePINToDB();
          } else {
            return res.status(200).send({
              status: "err",
              error: "wrong pin",
            });
          }
        });
      }
    } catch (err) {
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "pin was not changed, server error",
      });
    }
  }
);

const pin_validation = (pin) => {
  if (pin.length != 4) {
    Logger.info("Provided PIN does not consist of 4 characters");
    return false;
  }
  var numbers = /[0-9]/g;
  if (!pin.match(numbers)) {
    Logger.info("Provided PIN does not consist of numbers only");
    return false;
  }
  return true;
};

app.post(
  "/settings/changePassword",
  isAuthenticated,
  csrfProtect,
  async (req, res) => {
    try {
      const userId = jwtDecode(req.session.uid).userId;
      const oldPassword = req.body.data.old_password;
      const newPassword = req.body.data.new_password;
      let dbPasswordHash = "";
      // Logger.info({ oldPassword, newPassword });

      if (!newPassword)
        return res.status(400).send({
          status: "err",
          error: "new password undefined",
        });

      if (!password_validation(newPassword)) {
        return res.status(400).send({
          status: "err",
          error: "password validation failed",
        });
      }

      // try {
      //   await gcPostgresql.connect();
      // } catch (err) {
      //   Logger.error(err);
      // }

      try {
        const val = await gcPostgresql.userPassword(knex, {
          user_id: userId,
        });
        dbPasswordHash = val[0].password;
      } catch (err) {
        Logger.error(err);
      }

      Logger.info({
        dbPasswordHash,
      });

      const savePasswordToDB = () => {
        bcrypt.hash(newPassword, saltRounds, async (err, hash) => {
          try {
            await gcPostgresql.setPassword(knex, {
              user_id: userId,
              password: hash,
            });
            // try {
            //   await gcPostgresql.close();
            // } catch (err) {
            //   Logger.error(err);
            // }
            return res.status(200).send({
              status: "Success",
              info: "password changed",
            });
          } catch (err) {
            Logger.error(err);
          }
        });
      };

      if (dbPasswordHash === null || dbPasswordHash === "") {
        savePasswordToDB();
      } else {
        bcrypt.compare(oldPassword, dbPasswordHash, async (err, result) => {
          if (err) {
            return res.status(200).send({
              status: "err",
              error: "bcrypt compare error",
            });
          }

          if (result) {
            savePasswordToDB();
          } else {
            return res.status(200).send({
              status: "err",
              error: "wrong password",
            });
          }
        });
      }
    } catch (err) {
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "server error",
      });
    }
  }
);

const password_validation = (password) => {
  if (password.length < 8) {
    Logger.info("Provided password is too short");
    return false;
  }
  if (password.length > 128) {
    Logger.info("Provided password is too long");
    return false;
  }
  return true;
};

app.post(
  "/settings/securityQuestions",
  csrfProtect,
  limiter,
  async (req, res) => {
    try {
      const data = req.body.data;
      const user_id = jwtDecode(req.session.uid).userId;

      // Logger.info({ data });

      // try {
      //   await gcPostgresql.connect();
      // } catch (err) {
      //   Logger.error(err);
      // }

      try {
        const questions = await gcPostgresql.userSecurityQuestions(knex, {
          user_id: user_id,
        });

        if (questions.length > 9) {
          Logger.error(
            "Maximum number of defined security questions exceeded. Allowed max: 10"
          );
          return res.status(400).send({
            status: "err",
            error: "maximum number of defined security questions exceed 10",
          });
        }

        const questions_to_add = [];
        for (const qn of data) {
          var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/]+/;

          const q = qn.question
            .replace("?", "")
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
            .toLowerCase();

          const a = qn.answer;
          if (
            format.test(q) ||
            q.length > 100 ||
            format.test(a) ||
            a.length > 100
          ) {
            Logger.info(
              `special characters are detected or string is too long`
            );
            return res.status(400).send({
              status: "err",
              error: "special characters detected or string is too long",
            });
          }
          questions_to_add.push(q);
        }

        for (const qst of questions) {
          Logger.info({
            qst,
          });
          const q_2 = qst.question
            .replace("?", "")
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
            .toLowerCase();
          if (questions_to_add.includes(q_2)) {
            Logger.info(`question ${qst.question} has already been submitted`);
            return res.status(400).send({
              status: "err",
              error: "question has already been submitted",
            });
          }
        }

        await gcPostgresql.addSecurityQuestions(knex, {
          user_id: user_id,
          questions: data,
        });
      } catch (err) {
        Logger.error(err);
      }

      // try {
      //   await gcPostgresql.close();
      // } catch (err) {
      //   Logger.error(err);
      // }

      return res.status(200).send({
        status: "ok",
        info: "updated successfully",
      });
    } catch (err) {
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "server error",
      });
    }
  }
);

app.get("/settings/securityQuestions", csrfProtect, async (req, res) => {
  try {
    const user_id = jwtDecode(req.session.uid).userId;
    let questions;
    // try {
    //   await gcPostgresql.connect();
    // } catch (err) {
    //   Logger.error(err);
    // }
    try {
      questions = await gcPostgresql.userSecurityQuestions(knex, {
        user_id: user_id,
      });
    } catch (err) {
      Logger.error(err);
    }
    // try {
    //   await gcPostgresql.close();
    // } catch (err) {
    //   Logger.error(err);
    // }

    return res.status(200).send(questions);
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/api/biometry/enrollments", csrfProtect, async (req, res) => {
  try {
    let file = req.files.audioFile;
    const isBiometryEnrolled = req.body.isBiometryEnrolled;
    const verification = req.body.verification;
    const userId = jwtDecode(req.session.uid).userId;
    // Logger.info({ file }, { isBiometryEnrolled }, { verification });
    let url = `${env.OPEN_ID_BIOMETRY_ENROLLMENT}?userId=${userId}`;

    // console.log(`czy uzytkownik ma biometrie? ${isBiometryEnrolled}`);

    if (isBiometryEnrolled === "false") {
      Logger.info("creating new biometry profile....");

      try {
        const createdProfile = await createBiometryProfile({
          url: url,
        });
        Logger.info({
          createdProfile,
        });
      } catch (err) {
        Logger.error({
          err,
        });
      }

      // próbuje zrobić enrollment długieeeeej próbki
      url = `${env.OPEN_ID_BIOMETRY_ENROLLMENT}/${userId}/enrollments`;
      Logger.info({
        url,
      });

      try {
        const data = await sendBiometrySample({
          url: url,
          file: file,
        });
        // Logger.info({ data });

        if (data.enrollmentStatus === "Enrolled") {
          // try {
          //   await gcPostgresql.connect();
          // } catch (err) {
          //   Logger.error(err);
          // }

          try {
            await gcPostgresql.setUserBiometryStatus(knex, {
              user_id: userId,
              isBiometryEnrolled: true,
            });
          } catch (err) {
            Logger.error(err);
          }

          // try {
          //   await gcPostgresql.close();
          // } catch (err) {
          //   Logger.error(err);
          // }

          return res.status(200).send({
            status: "Success",
            info: "biometry profile created and enrolled successfully",
          });
        } else if (data.enrollmentStatus === "Enrolling") {
          return res.status(200).send({
            status: "Warning",
            info: "enrolling in process...",
          });
        } else {
          return res.status(400).send({
            status: "Error",
            info: "biometry was not enrolled successfully",
          });
        }
      } catch (err) {
        return res.status(400).send({
          status: "Error",
          info: "biometry was not enrolled successfully",
        });
      }
    } else {
      if (verification === "true") {
        url = `${env.OPEN_ID_BIOMETRY_ENROLLMENT}/${userId}/verifications?userId=${userId}&threshold=0.5`;
      } else {
        url = `${env.OPEN_ID_BIOMETRY_ENROLLMENT}/${userId}/enrollments`;
      }

      try {
        const data = await sendBiometrySample({
          url: url,
          file: file,
        });
        // Logger.info({ data });
        if (verification === "true") {
          if (data.recognitionResult.includes("Error")) {
            return res.status(400).send({
              status: "Error",
              info: "biometry was not verify correctly",
            });
          } else {
            return res.status(200).send({
              status: "Success",
              info: "biometry verified successfully",
            });
          }
        } else {
          return res.status(200).send({
            status: "Success",
            info: "biometry enrolled successfully",
          });
        }
      } catch (err) {
        return res.status(400).send({
          status: "Error",
          info: "biometry was not verify/enrolled correctly",
        });
      }
    }
  } catch (err) {
    // return res.status(400).send("NOT OK");
  }
});

const createBiometryProfile = (obj) => {
  return new Promise((resolve, reject) => {
    const url = obj.url;
    const axios = require("axios");
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    axios({
      method: "post",
      port: 8083,
      url: url,
    })
      .then(async (response) => {
        // Logger.info({ response });
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
};

const sendBiometrySample = (obj) => {
  return new Promise((resolve, reject) => {
    const url = obj.url;
    const ffmpeg = require("fluent-ffmpeg");
    ffmpeg(obj.file.tempFilePath)
      .toFormat("wav")
      .on("error", (err) => {
        Logger.error("An error occurred: " + err.message);
      })
      .on("progress", (progress) => {
        Logger.info("Processing: " + progress.targetSize + " KB converted");
      })
      .on("end", () => {
        Logger.info("Processing finished !");
        const promise = new Promise((resolve) => {
          const FormData = require("form-data"); // npm install --save form-data
          var fd = new FormData();
          fd.append(
            "audioFile",
            fs.createReadStream("./uploads/enrollment_speach_append.wav")
          );
          const concat = require("concat-stream");
          fd.pipe(
            concat(
              {
                encoding: "buffer",
              },
              (data) =>
                resolve({
                  data,
                  headers: fd.getHeaders(),
                })
            )
          );
        });

        promise.then(({ data, headers }) => {
          const axios = require("axios");
          process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
          axios({
            method: "post",
            port: 8083,
            url: url,
            data: data,
            headers,
          })
            .then((response) => {
              // console.log({ response });
              if (response.status === 200) {
                resolve(response?.data);
              } else reject(response);
            })
            .catch((response) => {
              // console.log({ response });
              reject(response);
            });
        });
      })
      .save("./uploads/enrollment_speach_append.wav");
  });
};

app.get("/settings/generatePassphrase", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  const passphrase = await generatePassphrase(userId);
  Logger.info({
    passphrase,
  });
  return res.status(200).send(passphrase);
});

const generatePassphrase = (userId) => {
  return new Promise((resolve) => {
    const url = `${env.OPEN_ID_BIOMETRY_ENROLLMENT}/${userId}/passphrase?userId=${userId}`;
    const axios = require("axios");
    axios({
      method: "post",
      port: 8083,
      url: url,
    })
      .then((response) => {
        if (response?.data) {
          resolve(response.data);
        } else
          resolve({
            passphrase: "mój głos jest moim hasłem",
            currentTimestamp: "",
            passphraseExpirationTime: "",
          });
      })
      .catch((res) => {
        resolve({
          passphrase: "mój głos jest moim hasłem",
          currentTimestamp: "",
          passphraseExpirationTime: "",
        });
      });
  });
};

app.post("/consentId", isAuthenticated, csrfProtect, async (req, res) => {
  try {
    const userUUID = jwtDecode(req.session.uid).userId;
    const service = req.body.data.service;
    let consentID = "";
    try {
      const { getConsentID } = require("./blockchain/blockchain");
      consentID = await getConsentID({
        userId: userUUID,
        sourceName: service,
      });
      Logger.info(`CONSENT ID::`);
      Logger.info({
        consentID,
      });
    } catch (err) {
      Logger.error({
        err,
      });
    }

    return res.status(200).send(consentID);
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/tests/consent", async (req, res) => {
  try {
    const userUUID = req.body?.user_id;
    const service = req.body?.service;
    let consentID = "";

    if (!userUUID || userUUID === undefined) {
      Logger.error("user_id not found!");
      return res.status(400).send({
        status: "err",
        error: "user_id not found",
      });
    }

    if (!service || service === undefined) {
      Logger.error("service not found!");
      return res.status(400).send({
        status: "err",
        error: "service not found",
      });
    }

    Logger.info(
      `Getting consent id for userId: ${userUUID}, service: ${service}`
    );

    try {
      const { getConsentID } = require("./blockchain/blockchain");
      consentID = await getConsentID({
        userId: userUUID,
        sourceName: service,
      });
      Logger.info(`CONSENT ID::${consentID}`);
      // Logger.info({ consentID });
    } catch (err) {
      Logger.error({
        err,
      });
      return res.status(400).send({
        status: "err",
        error: "Blockchain server error",
      });
    }
    // Logger.info({ consentID });
    return res.status(200).send({
      consentID,
    });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "Selfcare server error",
    });
  }
});

const getAuthenticationFactors = (dt) => {
  return new Promise(async (resolve) => {
    let obj = {};
    const user_id = dt.user_id;
    const factors = dt.factors;
    // sprawdzic czy uzytkownik ma jakies telefony, maile, czy ma pin, hasło, i biometrie oraz jakie pytania ma zdefiniowane
    // SELECT EMAILS

    if (factors.includes("all") || factors.includes("emails")) {
      try {
        obj.emails = await gcPostgresql.userEmails(knex, {
          user_id: user_id,
        });
      } catch (err) {
        Logger.error(err);
      }
    }

    // SELECT phones
    if (factors.includes("all") || factors.includes("phones")) {
      try {
        obj.phones = await gcPostgresql.userPhones(knex, {
          user_id: user_id,
        });
      } catch (err) {
        Logger.error(err);
      }
    }

    // CHECK PASSWORD
    if (factors.includes("all") || factors.includes("password")) {
      try {
        obj.password = await gcPostgresql.userPassword(knex, {
          user_id: user_id,
        });
      } catch (err) {
        Logger.error(err);
      }
    }

    // CHECK PIN
    if (factors.includes("all") || factors.includes("pin")) {
      try {
        obj.pin = await gcPostgresql.userPIN(knex, {
          user_id: user_id,
        });
      } catch (err) {
        Logger.error(err);
      }
    }

    // check biometry
    if (factors.includes("all") || factors.includes("is_biometry_enrolled")) {
      try {
        obj.is_biometry_enrolled = await gcPostgresql.isBiometryEnrolled(knex, {
          user_id: user_id,
        });
      } catch (err) {
        Logger.error(err);
      }
    }

    // security questions
    if (factors.includes("all") || factors.includes("security_questions")) {
      try {
        obj.security_questions = await gcPostgresql.userSecurityQuestions(
          knex,
          {
            user_id: user_id,
          }
        );
      } catch (err) {
        Logger.error(err);
      }
    }

    resolve(obj);
  });
};

const authenticationFactorsCount = (user_id) => {
  return new Promise(async (resolve) => {
    const factors = await getAuthenticationFactors({
      user_id: user_id,
      factors: ["all"],
    });
    // check if user has min two factors defined
    let count = 0;
    for (const key in factors) {
      // console.log(`sprawdzam skladnik uwierzytelniajacy ${key}`);
      // if (key != "security_questions") {
      if (key === "is_biometry_enrolled") {
        if (factors[key][0].is_biometry_enrolled) {
          count++;
        }
      } else if (key === "security_questions") {
        factors[key].length > 0 && count++;
      } else {
        if (factors[key].length > 0) {
          for (const e of factors[key]) {
            if (key === "password" || key === "pin") {
              if (e[key] && e[key] != null) {
                count++;
              }
            } else if (key === "emails") {
              if (e["email_address"] && e["email_address"] != null) {
                count++;
              }
            } else if (key === "phones") {
              if (e["phone_number"] && e["phone_number"] != null) {
                count++;
              }
            }
          }
        }
      }
      Logger.info(`counter:: ${count}`);
      // }
    }
    resolve(count);
  });
};

app.post(
  "/settings/phone/delete",
  isAuthenticated,
  csrfProtect,
  async (req, res) => {
    try {
      const lang = req.query.lang;
      const phone_number = req.body.data.phone_number;

      const user_id = jwtDecode(req.session.uid).userId;

      try {
        // await gcPostgresql.connect();

        const count = await authenticationFactorsCount(user_id);
        Logger.info("authentication factors:: ", {
          count,
        });

        if (count > 1) {
          await gcPostgresql.deletePhone(knex, {
            user_id: user_id,
            phone_number: phone_number,
          });
          // await gcPostgresql.close();
          return res.status(200).send({
            status: 200,
            info: "OK",
          });
        } else {
          // await gcPostgresql.close();
          return res.status(400).send({
            status: "auth count",
            info: "not enough authentication factors defined",
          });
        }
      } catch (err) {
        return res.status(400).send({
          error: 400,
          info: "server error",
        });
      }
    } catch (err) {
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "server error",
      });
    }
  }
);

app.get("/settings/phone/get", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  let phones;
  // try {
  //   await gcPostgresql.connect();
  // } catch (err) {
  //   Logger.error(err);
  // }

  try {
    phones = await gcPostgresql.userPhones(knex, {
      user_id: userId,
    });
  } catch (err) {
    Logger.error(err);
  }

  // try {
  //   await gcPostgresql.close();
  // } catch (err) {
  //   Logger.error(err);
  // }

  return res.status(200).send(phones);
});

app.post("/updateManually", limiter, csrfProtect, async (req, res) => {
  try {
    const picture = req.files?.picture;
    let data = req.body.data;
    const userId = jwtDecode(req.session.uid).userId;

    if (picture) {
      // console.log({ picture });
      if (
        (picture.mimetype != "image/jpg" &&
          picture.mimetype != "image/jpeg" &&
          picture.mimetype != "image/png") ||
        picture.size > 50000000
      ) {
        return res.status(400).send({
          status: "image err",
          error: "image mimetype is not correct or file is too big",
        });
      }
      data = JSON.parse(data);
      const { bucketPath } = await gcStorage.uploadUserImage(userId, picture);
      data["picture"] = bucketPath;
      Logger.info("user image uploaded to storage bucket");
    }

    // Logger.info({ data });
    const { convertToBigQuerySchema } = require("./mapper/bqMapper");
    const bq_data = convertToBigQuerySchema({
      userId: userId,
      checkedData: "all",
      data: data,
    });
    Logger.info({
      bq_data,
    });
    await gcBigquery.publishMultipleToTopics(userId, bq_data);
    return res.status(200).send("USER DATA UPDATED");
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/tests/data/updateManually", async (req, res) => {
  try {
    let data = JSON.parse(req.body.data);
    const userId = req.body.user_id;

    if (!userId || userId === undefined) {
      Logger.error("user_id not found!");
      return res.status(400).send({
        status: "err",
        error: "user_id not found",
      });
    }

    if (!data || data === undefined) {
      Logger.error("data not found!");
      return res.status(400).send({
        status: "err",
        error: "data not found",
      });
    }

    Logger.info(`Updating data: ${data} manually for userId: ${userId}`);

    data.source = "manually";
    data.loa = "low";
    const { convertToBigQuerySchema } = require("./mapper/bqMapper");
    const bq_data = convertToBigQuerySchema({
      userId: userId,
      checkedData: "all",
      data: data,
    });

    Logger.info(`Publishing data to topics`);
    await gcBigquery.publishMultipleToTopics(userId, bq_data);
    Logger.info("All data published!");
    return res.status(200).send({
      status: 200,
      info: "Success - data updated manually",
    });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/publishDataToTopic", csrfProtect, async (req, res) => {
  try {
    let data = req.body.data;
    Logger.info("publishing user data to topic routes::");
    const userId = jwtDecode(req.session.uid).userId;
    await gcBigquery.publishMultipleToTopics(userId, data);
    return res.status(200).send("all messages published!");
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post(
  "/identities/email",
  isAuthenticated,
  csrfProtect,
  async (req, res) => {
    const lang = req.query.lang;
    try {
      const response = await refreshTokenRequest(req);
      // Logger.info("email response::", response.data);
      const userData = jwtDecode(response.data.id_token.id_token);
      // Logger.info("current user data", {
      //   userData,
      // });
      const tokens = saveTokens(response);
      req.session.tokens = tokens;
      return res.status(200).render(`main/partials${req.path}`, {
        emails: userData.emails,
        dictionary: language[lang],
      });
    } catch (err) {
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "server error",
      });
    }
  }
);

app.post("/settings/email/add", isAuthenticated, csrfProtect, (req, res) => {
  try {
    const email = req.body.email;
    const user_id = jwtDecode(req.session.uid).userId;
    const client_id = env.OPEN_ID_CLIENT_ID;
    // Logger.info({ email }, { user_id }, { client_id });
    const https = require("https");
    const post_data = {
      email: email,
      client_id: client_id,
      user_id: user_id,
      httpsAgent: new https.Agent({
        rejectUnauthorized: false,
      }),
    };

    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer xxxxxx",
    };
    Logger.info({
      post_data,
    });
    const axios = require("axios");
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    axios
      .post(`${env.OPEN_ID_EMAIL_ENDPOINT}`, post_data, {
        headers: headers,
      })
      .then((response) => {
        // console.log({ response });
        Logger.info("open id email response::", response.data);
        res.status(200).send(response.data);
      })
      .catch((err) => {
        Logger.error(err);
        return res.status(502).send({
          status: "err",
          error: "server error",
        });
      });
    // return res.status(200).send("ok");
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/tests/email", (req, res) => {
  try {
    const email = req.body?.email;
    const user_id = req.body?.user_id;
    const client_id = env.OPEN_ID_CLIENT_ID;
    // Logger.info({ email }, { user_id }, { client_id });

    if (!email || email === undefined) {
      Logger.error("email not found!");
      return res.status(400).send({
        status: "err",
        error: "email not found",
      });
    }

    if (!user_id || user_id === undefined) {
      Logger.error("user_id not found!");
      return res.status(400).send({
        status: "err",
        error: "user_id not found",
      });
    }

    if (!client_id || client_id === undefined) {
      Logger.error("client_id not found!");
      return res.status(400).send({
        status: "err",
        error: "client_id not found",
      });
    }

    const https = require("https");
    const post_data = {
      email: email,
      client_id: client_id,
      user_id: user_id,
      httpsAgent: new https.Agent({
        rejectUnauthorized: false,
      }),
    };

    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer xxxxxx",
    };
    // Logger.info({ post_data });
    Logger.info(
      `Adding new email - POST request ${env.OPEN_ID_EMAIL_ENDPOINT}, POST data: ${post_data}`
    );
    const axios = require("axios");
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    axios
      .post(`${env.OPEN_ID_EMAIL_ENDPOINT}`, post_data, {
        headers: headers,
      })
      .then((response) => {
        Logger.info(`OpenId email response::${response.data}`);
        res.status(200).send(response.data);
      })
      .catch((err) => {
        Logger.error(err);
        return res.status(502).send({
          status: "err",
          error: "OpenId server error",
        });
      });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "Selfcare server error",
    });
  }
});

app.post(
  "/settings/email/delete",
  isAuthenticated,
  csrfProtect,
  async (req, res) => {
    const email = req.body.data.email;
    const user_id = jwtDecode(req.session.uid).userId;

    try {
      // await gcPostgresql.connect();
      const count = await authenticationFactorsCount(user_id);
      Logger.info("authentication factors counter:: ", {
        count,
      });
      if (count >= 2) {
        await gcPostgresql.deleteEmail(knex, {
          user_id: user_id,
          email: email,
        });
        // await gcPostgresql.close();
        return res.status(200).send({
          status: 200,
          info: "email deleted successfully",
        });
      } else {
        // await gcPostgresql.close();
        return res.status(400).send({
          status: "auth count",
          info: "not enough authentication factors defined",
        });
      }
    } catch (err) {
      Logger.error(err);
      return res.status(400).send({
        error: 400,
        info: err,
      });
    }
  }
);

app.post("/settings/email/resend", csrfProtect, async (req, res) => {
  const email = req.body.data.email;
  const clientId = env.OPEN_ID_CLIENT_ID;
  const url = env.OPEN_ID_RESEND_EMAIL_ENDPOINT;

  // Logger.info({ email }, { clientId }, { url });

  const https = require("https");
  const post_data = {
    email: email,
    client_id: clientId,
    httpsAgent: new https.Agent({
      rejectUnauthorized: false,
    }),
  };

  const headers = {
    "Content-Type": "application/json",
    Authorization: "Bearer xxxxxx",
  };
  // Logger.info({ post_data });
  const axios = require("axios");
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
  axios
    .post(url, post_data, {
      headers: headers,
    })
    .then((response) => {
      Logger.info("open id email resend response::", response.data);
      return res.status(200).send(response.data);
    })
    .catch((err) => {
      Logger.error(err);
      return res.status(502).send({
        status: "err",
        error: "external server error",
      });
    });
});

app.post(
  "/identities/phones",
  isAuthenticated,
  csrfProtect,
  async (req, res) => {
    const lang = req.query.lang;
    try {
      const response = await refreshTokenRequest(req);
      const userData = jwtDecode(response.data.id_token.id_token);
      // Logger.info("[current user data]", {
      //   userData,
      // });
      let phones;
      phones = userData.phone_numbers;
      const tokens = saveTokens(response);
      req.session.tokens = tokens;
      return res.status(200).render(`main/partials${req.path}`, {
        phones: phones,
        dictionary: language[lang],
      });
    } catch (err) {
      Logger.error(err);
      return res.status(400).send({
        status: "err",
        error: "server error",
      });
    }
  }
);

app.post(
  [
    "/identities/identities",
    "/profiles/profiles",
    "/profiles/investment",
    "/profiles/business",
    "/profiles/budget",
  ],
  isAuthenticated,
  csrfProtect,
  (req, res) => {
    const lang = req.query.lang;
    res.status(200).render(`main/partials${req.path}`, {
      dictionary: language[lang],
    });
  }
);

app.post("/api/banqup", isAuthenticated, csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  const bank_id = req.body.data.bank_id;
  Logger.info({
    userId,
  });
  Logger.info({
    bank_id,
  });

  // check if user is registered in a banqware service
  const bnqwr = require("./banqware/banqware");
  try {
    Logger.info(`Checking if user exists in banqware service...`);
    const user = await bnqwr.getSingleUser({
      userId: userId,
    });
    Logger.info(`User exists...`);
    Logger.info({
      user,
    });
  } catch (err) {
    // Logger.error({ err });
    if (err.response && err.response?.status === 404) {
      Logger.info(`User not found in banqware service...`);
      Logger.info(`Registering a new user in banqware service...`);
      try {
        const newUser = await bnqwr.createNewUser(userId);
        Logger.info(`New user created...`);
        Logger.info({
          newUser,
        });
      } catch (err) {
        Logger.error(err);
      }
    } else {
      Logger.error(err);
      return res.status(400).send({
        status: 400,
        info: "error",
      });
    }
  }

  // creating post request to get consent from banqware api
  Logger.info(`Creating a user consent in a context of a given bank...`);
  try {
    const consent = await bnqwr.createNewConsent({
      userId: userId,
      bank_id: bank_id,
      ip: req.ip,
    });
    Logger.info(`Consent created successfully...`);
    Logger.info({
      consent,
    });
    return res.status(200).send({
      consent: consent,
    });
  } catch (err) {
    Logger.error({
      err,
    });
    return res.status(401).send({
      status: 401,
      info: "error",
    });
  }

  // return res.status(200).send({ status: 200, info: "OK" });
});

app.get("/BanqupRedirect", isAuthenticated, csrfProtect, async (req, res) => {
  const scope = req.query.scope;
  const consentId = req.query.consentId;
  const userId = req.query.userId;
  const clientId = req.query.clientId;
  Logger.info(`Getting redirect from banqware service...`);
  Logger.info({
    scope,
  });
  Logger.info({
    consentId,
  });
  Logger.info({
    userId,
  });
  Logger.info({
    clientId,
  });

  let lang = getLanguage(req);
  // get username from bigquery.
  const username = await getUsername(clientId);

  // profile img
  let profile_url = await getProfileImage(clientId);
  Logger.info({
    profile_url,
  });

  const security_questions = await getAuthenticationFactors({
    user_id: clientId,
    factors: ["security_questions"],
  });

  Logger.info("Rendering profile budget webpage...");
  return res.status(200).render("banqup/banqup_redirect.ejs", {
    security_questions: security_questions,
    username: username ? username : "",
    dictionary: language[lang] ? language[lang] : language["eng"],
    profile_image: profile_url,
    csrfToken: req.csrfToken(),
  });
});

app.post(
  "/api/banqup/bankAccount",
  isAuthenticated,
  csrfProtect,
  (req, res) => {
    const consentId = req.body.data.consentId;
    const userId = jwtDecode(req.session.uid).userId;
    Logger.info(`Getting consent to download bank account data::`);
    Logger.info({
      consentId,
    });

    setTimeout(async () => {
      // check all bank accounts that are connected with that consent
      const bnqwr = require("./banqware/banqware.js");
      try {
        const consentAccounts = await bnqwr.getSingleConsent({
          userId: userId,
          consentId: consentId,
        });
        Logger.info(`Found accounts connected with consent`);
        Logger.info({
          consentAccounts,
        });

        // getting data from accounts
        Logger.info(`Getting data from bank accounts...`);
        const data = {
          bank_account: [],
          transactions: [],
        };
        const linkedAccounts = consentAccounts["linkedAccounts"];
        for (const account of linkedAccounts) {
          const accountId = account["accountId"];

          // get data about single bank account
          Logger.info(`getting data about single bank account ID::`);
          Logger.info({
            accountId,
          });
          try {
            const accountData = await bnqwr.getSingleBankAccount({
              userId: userId,
              accountId: accountId,
            });
            Logger.info(`Data downloaded successfully`);
            data.bank_account.push(accountData);
          } catch (err) {
            Logger.error(`ERROR: data was not download properly`);
          }

          // get transactions of a single bank account
          Logger.info(`getting transactions from single bank account ID::`);
          Logger.info({
            accountId,
          });
          try {
            const transactionsData = await bnqwr.getListOfTransactions({
              userId: userId,
              accountId: accountId,
            });
            Logger.info(`Data downloaded successfully`);
            data.transactions.push(transactionsData);
          } catch (err) {
            Logger.error(`ERROR: data was not download properly`);
          }
        }

        return res.status(200).send({
          status: "OK",
          data: data,
        });
      } catch (err) {
        Logger.error({
          err,
        });
        return res.status(401).send({
          status: 401,
          info: "server error",
        });
      }
    }, 10000);
  }
);

app.post(
  "/polishapi/request/authorize",
  isAuthenticated,
  csrfProtect,
  async (req, res) => {
    try {
      // request for authorization and getting url address with authorization form
      const polishapi = require("./polishapi/polishapi");
      const response = await polishapi.authorize();
      const aspspRedirectUri = response.aspspRedirectUri;
      const requestId = response.responseHeader.requestId;
      return res.status(200).send({
        status: "ok",
        data: {
          aspspRedirectUri: aspspRedirectUri,
          requestId: requestId,
        },
      });
    } catch (err) {
      Logger.error(err);
      return res.status(401).send({
        status: 401,
        info: "server error",
      });
    }
  }
);

app.get(
  "/polishapi/response",
  isAuthenticated,
  csrfProtect,
  async (req, res) => {
    try {
      // redirected from pkobapi service with parameters
      const userId = jwtDecode(req.session.uid).userId;
      let lang = getLanguage(req);
      // get username from bigquery.
      const username = await getUsername(userId);

      // profile img
      let profile_url = await getProfileImage(userId);
      // Logger.info({
      //   profile_url,
      // });

      const security_questions = await getAuthenticationFactors({
        user_id: userId,
        factors: ["security_questions"],
      });

      Logger.info("Rendering profile budget webpage...");
      return res.status(200).render("banqup/banqup_redirect.ejs", {
        security_questions: security_questions,
        username: username ? username : "",
        dictionary: language[lang] ? language[lang] : language["eng"],
        profile_image: profile_url,
        csrfToken: req.csrfToken(),
      });
    } catch (err) {
      Logger.error(err);
      return res.status(401).send({
        status: 401,
        info: "server error",
      });
    }
  }
);

app.post(
  "/polishapi/accountsInformation",
  isAuthenticated,
  csrfProtect,
  async (req, res) => {
    try {
      const code = req.body.data.code;
      const state = req.body.data.state;
      Logger.info(`Downloading accounts information data...`);

      // request for access token to get information about the accounts
      const polishapi = require("./polishapi/polishapi");
      const response = await polishapi.access_token(code);
      const access_token = response.access_token;
      const previlege_list = response.scope_details.privilegeList;
      let data = [];
      for await (const account of previlege_list) {
        const account_number = account.accountNumber;
        const account_information = await polishapi.get_account({
          accessToken: access_token,
          accountNumber: account_number,
        });
        data.push(account_information.account);
        const transactions_done = await polishapi.get_transactions_done({
          accessToken: access_token,
          accountNumber: account_number,
        });
        data[data.length - 1]["historyTransactions"] = [];
        data[data.length - 1]["historyTransactions"] =
          transactions_done.transactions;
      }

      return res.status(200).send({
        status: "ok",
        data: data,
      });
    } catch (err) {
      Logger.error(err);
      return res.status(401).send({
        status: 401,
        info: "server error",
      });
    }
  }
);

app.post("/user/label", isAuthenticated, csrfProtect, async (req, res) => {
  const labels = req.body.data.labels;
  const scope = req.body.data.scope;
  const userId = jwtDecode(req.session.uid).userId;
  const obj = {};
  try {
    for (const label of labels) {
      const data = await gcBigquery.getLabel(userId, label, scope);
      obj[label] = data[0][label] ? data[0][label] : "";
    }

    return res.status(200).send(obj);
  } catch (err) {
    Logger.error(err);
    return res.status(401).send({
      status: 401,
      info: "server error",
    });
  }
});

app.post("/profiles/business/krs/save", csrfProtect, async (req, res) => {
  try {
    const userId = jwtDecode(req.session.uid).userId;
    const data = req.body.data.data;
    // Logger.info({ data });

    if (!data || data === undefined) {
      return res.status(400).send({
        status: "Error",
        err: "no body data, krs profile not saved",
      });
    }

    const result = await gcStorage.uploadJSONData(
      userId,
      "business_profile/KRS",
      data
    );

    Logger.info({
      result,
    });
    return res.status(200).send({
      status: "Success",
      info: "krs profile saved",
    });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "Error",
      err: "krs business profile not saved",
    });
  }
});

app.post("/profiles/business/ceidg/save", csrfProtect, async (req, res) => {
  try {
    const userId = jwtDecode(req.session.uid).userId;
    const data = req.body.data.data;
    // Logger.info({ data });
    if (!data || data === undefined) {
      return res.status(400).send({
        status: "Error",
        err: "no body data, ceidg profile not saved",
      });
    }

    const result = await gcStorage.uploadJSONData(
      userId,
      "business_profile/CEIDG",
      data
    );
    // Logger.info({ result });
    return res.status(200).send({
      status: "Success",
      info: "ceidg business profile saved",
    });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "Error",
      err: "ceidg business profile not saved",
    });
  }
});

app.post("/profiles/investment/save", csrfProtect, async (req, res) => {
  try {
    const userId = jwtDecode(req.session.uid).userId;
    const data = req.body.data.data;
    // Logger.info({ data });
    if (!data || data === undefined) {
      return res.status(400).send({
        status: "Error",
        err: "no body data, investment profile not saved",
      });
    }

    const result = await gcStorage.uploadJSONData(
      userId,
      "investment_profile",
      data
    );
    // Logger.info({ result });
    return res.status(200).send({
      status: "Success",
      info: "investment profile saved",
    });
  } catch (err) {
    return res.status(400).send({
      status: "Error",
      info: "investment profile was not saved",
    });
  }
});

app.post("/profiles/credit/save", csrfProtect, async (req, res) => {
  try {
    const userId = jwtDecode(req.session.uid).userId;
    const data = req.body.data.data;
    // Logger.info({ data });
    if (!data || data === undefined) {
      return res.status(400).send({
        status: "Error",
        err: "no body data, credit profile not saved",
      });
    }
    Logger.info(`UPLOADING CREDIT PROFILE JSON FILE`);
    const result = await gcStorage.uploadJSONData(
      userId,
      "credit_profile",
      data
    );
    Logger.info(`CREDIT PROFILE SAVED!`);
    return res.status(200).send({
      status: "Success",
      info: "credit profile saved",
    });
  } catch (err) {
    Logger.error({
      err,
    });
    return res.status(400).send({
      status: "Error",
      info: "credit profile was not saved",
    });
  }
});

app.post(
  [
    "/identities/profile",
    "/identities/profile_extended",
    "/identities/shopping",
    "/identities/identity",
    "/profiles/credit",
  ],
  isAuthenticated,
  csrfProtect,
  async (req, res) => {
    const lang = req.query.lang;
    try {
      const splittedPath = req.path.split("/");
      const scope = splittedPath[splittedPath.length - 1].toUpperCase();
      Logger.info("downloading data from table:: ", {
        scope,
      });
      let userData;
      try {
        userData = await gcBigquery.selectUserScopeBigQueryData(
          jwtDecode(req.session.uid).userId,
          scope
        );
      } catch (err) {
        // ....
      }

      if (userData?.picture && userData?.picture != undefined) {
        try {
          const url = await gcStorage.downloadFile(userData.picture.value);
          // Logger.info({
          //   url,
          // });
          userData.picture.value = url.split("/public/")[1];
        } catch (err) {
          // ....
        }
      }
      // Logger.info({
      //   userData,
      // });
      return res.render(`main/partials${req.path}`, {
        data: userData,
        dictionary: language[lang],
      });
    } catch (err) {
      Logger.error(err);
      return res.status(401).send({
        status: "err",
        error: "server error",
      });
    }
  }
);

app.post("/permissions", isAuthenticated, csrfProtect, async (req, res) => {
  try {
    const lang = req.query.lang;
    const e_show = req.query.e_show;
    const userId = jwtDecode(req.session.uid).userId;
    let permissions = [];
    try {
      const blockchain = require("./blockchain/blockchain");
      permissions = await blockchain.getPermissions({
        userId: userId,
        onlyActive: false,
      });
      Logger.info(`PERMISSIONS DOWNLOADED SUCCESSFULLY::`);
      Logger.info({ permissions });
    } catch (err) {
      Logger.error({
        err,
      });
    }
    // Logger.info({ permissions });
    return res.status(200).render(`main/partials${req.path}`, {
      dictionary: language[lang],
      permissions: permissions,
      e_show: e_show ? [e_show] : [],
    });
  } catch (err) {
    Logger.error(err);
    return res.status(404).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/permissions/update", csrfProtect, async (req, res) => {
  const userId = jwtDecode(req.session.uid).userId;
  const access_token = jwtDecode(req.session.tokens).access_token;
  const refresh_token = jwtDecode(req.session.tokens).refresh_token;
  const permissionId = req.body.data.permission_id;
  const viewerId = req.body.data.viewerId;
  const scope = req.body.data.scope;
  const status = req.body.data.status;
  const type = req.body.data.type;

  // Logger.info({ permissionId }, { viewerId }, { scope }, { status });

  try {
    const blockchain = require("./blockchain/blockchain");
    const data = await blockchain.updatePermission({
      userId: userId,
      permissionId: permissionId,
      viewerId: viewerId,
      scope: scope,
      status: status,
      type: type,
      access_token: access_token,
      refresh_token: refresh_token,
    });
    Logger.info(`PERMISSION UPDATED SUCCESSFULLY::`);
    return res.status(200).send({
      status: "Success",
      info: "permission update successfully",
    });
  } catch (err) {
    Logger.error({
      err,
    });
    return res.status(400).send({
      status: "Error",
      err: "permission was not updated successfully",
    });
  }
});

app.post("/data", isAuthenticated, csrfProtect, async (req, res) => {
  try {
    const lang = req.query.lang;
    const userId = jwtDecode(req.session.uid).userId;
    const data_filter = req.query.filter;
    Logger.info({
      data_filter,
    });
    let files;
    try {
      files = await gcStorage.getUserFiles(userId);
      // Logger.info({ files });
    } catch (err) {
      Logger.error(err);
    }
    return res.status(200).render(`main/partials${req.path}`, {
      data: files,
      dictionary: language[lang],
      data_filter: data_filter ? data_filter : "",
    });
  } catch (err) {
    Logger.error(err);
    return res.status(404).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/tests/userScope", async (req, res) => {
  try {
    const userId = req.body.user_id;
    const scope = req.body.scope;

    if (!userId || userId === undefined) {
      Logger.error("user_id not found!");
      return res.status(404).send({
        status: "err",
        error: "user_id not found!",
      });
    }

    if (!scope || scope === undefined) {
      Logger.error("scope not found!");
      return res.status(404).send({
        status: "err",
        error: "scope not found!",
      });
    }

    Logger.info(`Selecting scope: "${scope}" data for userId: ${userId}`);
    const scopeData = await gcBigquery.selectUserScope(userId, scope);
    Logger.info(`Data selected!`);
    return res.status(200).send({
      data: scopeData,
      scope: scope,
      info: "Success!",
      status: 200,
    });
  } catch (err) {
    Logger.error(err);
    return res.status(404).send({
      status: "err",
      error: "Selfcare server error",
    });
  }
});

app.post("/history", isAuthenticated, csrfProtect, async (req, res) => {
  try {
    const lang = req.query.lang;
    const data_filter = req.query.filter;
    Logger.info({
      data_filter,
    });
    // jwtDecode(req.session.uid).userId
    const userId = jwtDecode(req.session.uid).userId;
    const historyData = await gcBigquery.getAllData(userId);
    let authHistory = [],
      consents = [];
    try {
      const blockchain = require("./blockchain/blockchain");
      consents = await blockchain.getConsents({
        userId: userId,
      });
      authHistory = await blockchain.getAuthorizationHistory({
        userId: userId,
      });
      Logger.info(
        `:: CONSENTS AND AUTHORIZATION HISTORY DOWNLOADED SUCCESSFULLY ::`
      );
    } catch (err) {
      Logger.error({
        err,
      });
    }

    // console.log({ historyData }, { authHistory });
    return res.status(200).render(`main/partials${req.path}`, {
      data: historyData,
      consents: consents,
      auth_history: authHistory,
      dictionary: language[lang],
      data_filter: data_filter ? data_filter : "",
    });
  } catch (err) {
    Logger.error(err);
    return res.status(404).send({
      status: "err",
      error: "server error",
    });
  }
});

app.post("/api/gcs/download", csrfProtect, async (req, res) => {
  const path = req.body.data.path;
  let url;
  // Logger.info({ path });
  try {
    url = await gcStorage.downloadFile(path);
    Logger.info("gcs file downloaded!");
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }

  // set timeout to delete file
  setDeleteFile(120000, url);

  return res.status(200).send({
    url: url,
  });
});

app.post("/api/gcs/deleteFile", async (req, res) => {
  const path = req.body.data.path;
  let url;
  Logger.info("DELETING GOOGLE CLOUD JSON FILE...");
  Logger.info({
    path,
  });
  try {
    await gcStorage.deleteFile(path);
    Logger.info("GCS FILE DELETED");
    return res.status(200).send({
      status: 200,
      info: "File removed successfully",
    });
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

const setDeleteFile = (time = 30000, path) => {
  const file_path = path;
  setTimeout(() => {
    try {
      const fs = require("fs");
      fs.unlinkSync(file_path);
      Logger.info(
        `file ${
          file_path.split("/")[file_path.split("/").length - 1]
        } was successfully removed`
      );
    } catch (err) {
      Logger.error(err);
      Logger.error(
        `file ${
          file_path.split("/")[file_path.split("/").length - 1]
        } was not removed`
      );
    }
  }, time);
};

app.post("/networkDrive/deviceAuthorization", async (req, res) => {
  const deviceCode = req.body.code;
  Logger.info("device authorization... code from network drive: ", {
    deviceCode,
  });
  const codeGenerator = require("./generator/code_generator");
  try {
    const networkDriveAppSecretCode = env.NETWORK_DRIVE_APP_SECRET_CODE;
    // Logger.info({ networkDriveAppSecretCode });
    let userId, username;
    if (deviceCode != networkDriveAppSecretCode) {
      userId = await codeGenerator.verifyNetworkDriveCode(knex, deviceCode);
      username = await getUsername(userId);
    } else {
      userId = env.TESTER_ACCOUNT_USER_ID;
      username = env.NETWORK_DRIVE_APP_USERNAME;
    }

    const jwt = require("jsonwebtoken");
    const token = jwt.sign(
      {
        username: username,
        user_id: userId,
      },
      env.SECRET
    );
    // Logger.info({ token });
    return res.status(200).send(token);
  } catch (err) {
    return res.status(404).send("code invalid");
  }
});

app.post("/scraper/:websiteService", limiter, async (req, res) => {
  try {
    Logger.info(`scraping from service: ${req.params.websiteService}`);
    // send token in header with user ID
    if (!req.headers.authorization) {
      return res.status(403).json({
        error: "No credentials sent!",
      });
    }

    const token = req.headers.authorization;
    const service = req.params.websiteService;
    let file = req.files.file;

    const userId = jwtDecode(token.split(" ")[1]).user_id;
    // Logger.info({ service }, { file }, { token }, { userId });

    try {
      // consent to blockchain... zapisuej zgode na blockchainie i zwraca jej id albo po prostu zwraca jej id jak zgoda juz istnieje tylko modyfikuje timestamp
      const { getConsentID } = require("./blockchain/blockchain");
      const consentID = await getConsentID({
        userId: userId,
        sourceName: service,
      });

      Logger.info(`:: CONSENT DOWNLOADED SUCCESSFULLY ::`);
      Logger.info({
        consentID,
      });
    } catch (err) {
      Logger.error({
        err,
      });
    }

    if (
      path.extname(file.name) != ".html" &&
      path.extname(file.name) != ".mhtml"
    ) {
      return res.status(401).send({
        error: "Error - wrong extension",
        info: `The extension of file ${path.extname(
          file.name
        )} is incorrect. Possible values: html or mhtml`,
      });
    }

    file = {
      name: `${file.name}`,
      type: service,
      tempFilePath: file.tempFilePath,
    };

    const { bucketPath, bucketName } = await gcStorage.uploadFileToBucket(
      userId,
      "scraper",
      file
    );

    // Logger.info({ bucketPath });
    try {
      // const checkedData = await getUserSelectedData(userId);
      await runScraper(file, bucketName, bucketPath)
        .then(async (data) => {
          // Logger.info({ data });
          const { convertToBigQuerySchema } = require("./mapper/bqMapper");
          const bq_data = convertToBigQuerySchema({
            userId: userId,
            checkedData: "all",
            data: data,
          });
          try {
            // inform frontend that process completed
            const wsId = await ws.userIdToWsIdMapper(userId);
            ws.publishRedisChannelData(`user-data-${wsId}`, {
              event: "network_drive_update",
              status: "done",
              info: "webscraping data update done",
              source: service,
              data: bq_data,
              json: data.jsonStoragePath,
            });
            // await ws.sendMessage(userId, {
            //   event: "network_drive_update",
            //   status: "done",
            //   info: "webscraping data update done",
            //   source: service,
            //   data: bq_data,
            //   json: data.jsonStoragePath,
            // });
          } catch (err) {
            Logger.info(
              "========== data not send to selfcare frontend - user is not connected =============="
            );
          }
          return res.status(200).send("ok");
        })
        .catch((err) => {
          return res.status(401).send({
            error: "err",
            info: `error occured while processing scraper script`,
          });
        });
    } catch (err) {
      Logger.error(err);
    }
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

const runScraper = (file, bucketName, bucketPath) => {
  return new Promise(async (resolve, reject) => {
    const { exec } = require("child_process");
    Logger.info(
      `scraper script: bucketName=${bucketName} bucketPath=${bucketPath} service=${file.type}`
    );
    await exec(
      `node ${__dirname}/scraper/app.js bucketName=${bucketName} bucketPath=${bucketPath} service=${file.type}`,
      async (error, stdout, stderr) => {
        if (error !== null) {
          Logger.error("scraper exec error::", {
            error,
          });
          reject(error);
        } else {
          try {
            Logger.info({ stdout });
            const data = JSON.parse(stdout);
            resolve(data);
          } catch (err) {
            reject(err);
          }
        }
      }
    );
  });
};

app.post("/custom_protocol_check", csrfProtect, (req, res) => {
  try {
    const customProtocolCheck = require("custom-protocol-check");
    const protocol = req.body.data.protocol;
    // Logger.info({ stdout });
    ({
      protocol,
    });
    if (!protocol) {
      return res.status(400).json({
        status: 400,
        details: "no protocol body data",
      });
    }

    customProtocolCheck(
      `${protocol}`,
      () => {
        // Logger.info({ stdout });
        ("Custom protocol not found.");
        res.status(404).json({
          status: 404,
          details: "custom protocol not found",
        });
      },
      () => {
        // Logger.info({ stdout });
        ("Custom protocol found and opened the file successfully.");
        res.status(200).json({
          status: 200,
          details: "success! protocol found",
        });
      },
      5000
    );
  } catch (err) {}
});

app.get("/logout", csrfProtect, (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      Logger.error(err);
    } else {
      res.redirect("/");
    }
  });
});

// health checker
app.get("/lbstatus", (req, res) => {
  return res.status(200).send("selfcare is alive...");
});

// read ssl certificate
var privateKey = fs.readFileSync(env.SSL_PRIVKEY);
var certificate = fs.readFileSync(env.SSL_CERTIFICATE);

var credentials = {
  key: privateKey,
  cert: certificate,
  ciphers: [
    "ECDHE-RSA-AES256-SHA384",
    "DHE-RSA-AES256-SHA384",
    "ECDHE-RSA-AES256-SHA256",
    "DHE-RSA-AES256-SHA256",
    "ECDHE-RSA-AES128-SHA256",
    "DHE-RSA-AES128-SHA256",
    "HIGH",
    "!aNULL",
    "!eNULL",
    "!EXPORT",
    "!DES",
    "!RC4",
    "!MD5",
    "!PSK",
    "!SRP",
    "!CAMELLIA",
  ].join(":"),
};

//pass in your credentials to create an https server
var httpsServer = https.createServer(credentials, app);
httpsServer.listen(env.PORT, () => {
  Logger.info(`https server is running on port ${env.PORT} ...`);
});

ws = WebSocket.ws(httpsServer);

// const getUserSelectedData = (userId) => {
//   return new Promise(async (resolve) => {
//     ws.onMessage(userId, (client) => {
//       client.onmessage = async (message) => {
//         try {
//           const message_json = JSON.parse(message.data.toString());
//           if (message_json.event && message_json.event === "response_data") {
//             if (message_json.checked) {
//               //   if (message_json.data.length > 0) {
//               // Logger.info(message_json);
//
//               const dataChecked = message_json.data;
//               resolve(dataChecked);
//             }
//           }
//         } catch (err) {
//           Logger.error(
//             "Error while parsing websocket message with user selected data"
//           );
//           resolve("all");
//         }
//       };
//     });
//     try {
//       await ws.sendMessage(userId, {
//         event: "get_data",
//         checked: true,
//       });
//     } catch (err) {
//       resolve("all");
//     }
//   });
// };

app.post("/setWebsocketClient", csrfProtect, async (req, res) => {
  // Logger.info("sprobuje zrobic redirecta...");
  // return res.redirect(307, "10.205.2.31/setWebsocketClient");
  const userId = jwtDecode(req.session.uid)?.userId;
  const wsId = req.body.data.wsId;
  // Logger.info({ userId }, { wsId });
  try {
    await ws.setClient(wsId, userId);
    Logger.info("Websocket client created in redis");
    return res.status(200).send("Websocket client created in redis");
  } catch (err) {
    Logger.error(err);
    return res.status(400).send({
      status: "err",
      error: "server error",
    });
  }
});

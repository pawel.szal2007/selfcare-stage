exports.getHash = (data) => {
  return new Promise(async resolve => {
    console.log('hash function...');
    const crypto = require('crypto')
    const hash = await crypto.createHash('sha256').update(data).digest('hex');
    console.log(hash); // 9b74c9897bac770ffc029102a200c5de
    resolve(hash);
  })
}

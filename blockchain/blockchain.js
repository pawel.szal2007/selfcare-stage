// const blockchain_api = require("../configuration").projectVariable
//   .BLOCKCHAIN_API;
let env = require("dotenv").config();
const dotenvParseVariables = require("dotenv-parse-variables");
env = dotenvParseVariables(env.parsed);
const Logger = require("../winston-config");

exports.getConsents = (object) => {
  return new Promise((resolve, reject) => {
    let url = `${env.BLOCKCHAIN_API_DOMAIN}${env.BLOCKCHAIN_API_CONSENT_PATH}?`;
    for (const property in object) {
      url += `${property}=${object[property]}&`;
    }
    Logger.info("GET CONSENT REQUEST::");
    Logger.info({ url });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .get(url, {
        headers: {
          Authorization: env.HLF_API_TOKEN,
        },
      })
      .then((response) => {
        const data = response.data;
        // console.log({ data });
        resolve(data);
      })
      .catch((err) => {
        // console.log({ err });
        // const errObj = err.data;
        // console.log({ errObj });
        reject(err);
      });
  });
};

exports.getConsentID = (object) => {
  return new Promise((resolve, reject) => {
    let url = `${env.BLOCKCHAIN_API_DOMAIN}${env.BLOCKCHAIN_API_CONSENT_PATH}?`;
    for (const property in object) {
      url += `${property}=${object[property]}&`;
    }
    Logger.info("GET CONSENT ID REQUEST::");
    Logger.info({ url });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .get(url, {
        headers: {
          Authorization: env.HLF_API_TOKEN,
        },
      })
      .then(async (response) => {
        const data = response.data;
        // console.log('getConsent response.data:', {data});
        if (data) {
          let timestamp = 0;
          let consentId;
          for (const consent of data) {
            const consentTimestamp = new Date(consent.CreationDate).getTime();
            if (timestamp < consentTimestamp) {
              timestamp = consentTimestamp;
              consentId = consent.ConsentId;
            }
          }
          if (!consentId)
            reject({ data: "consent not properly found with timestamp" });
          Logger.info(":: CONSENT EXISTS ::");
          Logger.info({ consentId });
          await putConsentMetadata(consentId);
          resolve(consentId);
        } else {
          Logger.info("CONSENT NOT FOUND, CREATING NEW BLOCKCHAIN CONSENT...");
          const consentId = await addConsent(object.userId, object.sourceName);
          resolve(consentId);
        }
      })
      .catch((err) => {
        // const errObj = err.data;
        // console.log({ errObj });
        reject(err);
      });
  });
};

const addConsent = (userId, sourceName) => {
  return new Promise((resolve, reject) => {
    let url = `${env.BLOCKCHAIN_API_DOMAIN}${env.BLOCKCHAIN_API_CONSENT_PATH}`;
    // new Date(new Date().getTime() + 100000000000000)
    const data = {
      UserId: userId,
      SourceName: sourceName,
      ValidFrom: new Date(),
      ValidTo: new Date("2999-12-31T10:20:30Z").toISOString(),
      ExternalConsentId: "string",
      AutocreatedPermissions: true,
      Metadata: "string",
      ActiveNow: true,
    };
    Logger.info("ADD COSNENT REQUEST::");
    Logger.info({ url });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .post(url, data, {
        headers: {
          Authorization: env.HLF_API_TOKEN,
        },
      })
      .then((response) => {
        const data = response.data;
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

const putConsentMetadata = (consentId) => {
  return new Promise((resolve, reject) => {
    let url = `${env.BLOCKCHAIN_API_DOMAIN}${env.BLOCKCHAIN_API_CONSENT_PATH}/${consentId}/metadata`;
    Logger.info("PUT CONSENT METADATA::");
    Logger.info({ url });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .put(
        url,
        { Metadata: "updating data" },
        {
          headers: {
            accept: "text/plain",
            "Content-Type": "application/json",
            Authorization: env.HLF_API_TOKEN,
          },
        }
      )
      .then((response) => {
        const data = response.data;
        // console.log("putConsentMetadata response.data:", { data });
        resolve(data);
      })
      .catch((err) => {
        // const errObj = err;
        // console.log("putConsentMetadata response error:", { errObj });
        reject(err);
      });
  });
};

exports.addDataPackage = (obj) => {
  return new Promise((resolve, reject) => {
    Logger.info({ url });
    let url = `${env.BLOCKCHAIN_API_DOMAIN}${env.BLOCKCHAIN_API_DATA_PACKAGE_PATH}`;
    const data = JSON.stringify({
      UserId: obj.userId,
      ConsentId: obj.consentId,
      SourceName: obj.sourceName,
      DataFrom: "2021-06-02T13:09:13.821Z",
      DataTo: new Date("2999-12-31T10:20:30Z").toISOString(),
      ContentHash: obj.dataHash,
      ContentReference: "string",
      Metadata: "string",
    });
    Logger.info("ADDING DATA PACKAGE TO BLOCKCHAIN::");
    Logger.info({ data });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .post(url, data, {
        headers: {
          "Content-Type": "application/json",
          Authorization: env.HLF_API_TOKEN,
        },
      })
      .then((response) => {
        const data = response.data;
        // console.log("dataPackage response.data:", { data });
        resolve(data);
      })
      .catch((err) => {
        // const errObj = err.data;
        // console.log("dataPackage response error:", { errObj });
        reject(err);
      });
  });
};

const getPermissions = (obj) => {
  return new Promise((resolve, reject) => {
    let url = `${env.BLOCKCHAIN_API_DOMAIN}${env.BLOCKCHAIN_API_PERMISSIONS_PATH}?`;
    console.log({ url });
    for (const property in obj) {
      url += `${property}=${obj[property]}&`;
    }
    url = url.slice(0, -1);
    // url += "onlyActive=false";
    Logger.info("GETTING PERMISSIONS FROM BLOCKCHAIN::");
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .get(url, {
        headers: {
          Authorization: env.HLF_API_TOKEN,
        },
      })
      .then((response) => {
        const data = response.data;
        console.log({ data });
        resolve(data);
      })
      .catch((err) => {
        // console.log({ err });
        // const errObj = err.data;
        // console.log({ errObj });
        reject(err);
      });
  });
};

const updatePermission = (obj) => {
  return new Promise(async (resolve, reject) => {
    if (obj.permissionId) {
      try {
        const data = await updatePermissionById(obj);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    } else {
      // dodajemy nową permission o danym statusie
      // post na ten url co jest utworzony wyzej
      // poszukaj czy jest zgoda na blockchainie taka i wtedy ja aktywuj albo bezayktuj
      try {
        const response = await getPermissions({
          userId: obj.userId,
          viewerId: obj.viewerId,
          scope: obj.scope,
          permissionStatus: "Suspended",
          onlyActive: false,
        });
        console.log("znalezione zgody dla uzytkownika: ", { response });
        obj.permissionId = response[0].PermissionId;
        const data = await updatePermissionById(obj);
        resolve(data);
      } catch (err) {
        Logger.info("PERMISSION NOT FOUND - ADDING NEW PERMISSION::");
        try {
          const data = await addNewPermission(obj);
          resolve(data);
        } catch (err) {
          reject(err);
        }
      }
    }
  });
};

const updatePermissionById = (obj) => {
  return new Promise((resolve, reject) => {
    let url = `${env.BLOCKCHAIN_API_DOMAIN}${env.BLOCKCHAIN_API_PERMISSIONS_PATH}`;
    if (obj.permissionId) {
      // aktualizacja istniejącej
      if (obj.status) {
        // aktywacja permission o danym id
        url += `/${obj.permissionId}/activate`;
      } else {
        // uniewaznienie permission
        url += `/${obj.permissionId}/suspend`;
      }
      Logger.info("UPDATING PERMISSION BY ID::");
      process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
      const axios = require("axios");
      axios
        .put(url, {
          headers: {
            Authorization: env.HLF_API_TOKEN,
          },
        })
        .then((response) => {
          const data = response.data;
          resolve(data);
        })
        .catch((err) => {
          // const errObj = err.data;
          reject(err);
        });
    }
  });
};

const addNewPermission = (obj) => {
  return new Promise((resolve, reject) => {
    let url = `${env.BLOCKCHAIN_API_DOMAIN}${env.BLOCKCHAIN_API_PERMISSIONS_PATH}`;
    const data = JSON.stringify({
      UserId: obj.userId,
      ViewerId: obj.viewerId,
      Scope: obj.scope,
      PermissionType: obj.type,
      ValidFrom: "2021-03-03T19:46:27.141Z",
      ValidTo: new Date("2999-12-31T10:20:30Z").toISOString(),
      AccessToken: obj.access_token,
      RefreshToken: obj.refresh_token,
      Metadata: "",
    });
    Logger.info("NEW PERMISSION REQUEST::");
    Logger.info({ url });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .post(url, data, {
        headers: {
          "Content-Type": "application/json",
          Authorization: env.HLF_API_TOKEN,
        },
      })
      .then((response) => {
        // console.log(response);
        const data = response.data;
        // console.log("dataPackage response.data:", { data });
        resolve(data);
      })
      .catch((err) => {
        // const errObj = err.data;
        // console.log("permission post response error:", { errObj });
        reject(err);
      });
  });
};

exports.getAuthorizationHistory = (obj) => {
  return new Promise((resolve, reject) => {
    let url = `${env.BLOCKCHAIN_API_DOMAIN}${env.BLOCKCHAIN_API_AUTHORIZATION_HISTORY_PATH}?`;
    // console.log({ url });
    for (const property in obj) {
      url += `${property}=${obj[property]}&`;
    }
    Logger.info("GETTING BLOCKCHAIN AUTHORIZATION HISTORY::");
    Logger.info({ url });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .get(url, {
        headers: {
          Authorization: env.HLF_API_TOKEN,
        },
      })
      .then((response) => {
        const data = response.data;
        // console.log({ data });
        resolve(data);
      })
      .catch((err) => {
        // console.log({ err });
        // const errObj = err.data;
        // console.log({ errObj });
        reject(err);
      });
  });
};

exports.addNewPermission = addNewPermission;
exports.getPermissions = getPermissions;
exports.updatePermission = updatePermission;

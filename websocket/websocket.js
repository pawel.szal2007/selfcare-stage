exports.ws = (server) => {
  let env = require("dotenv").config();
  const dotenvParseVariables = require("dotenv-parse-variables");
  env = dotenvParseVariables(env.parsed);
  const fs = require("fs");

  const Logger = require("../winston-config");
  const { WebSocketServer } = require("ws");
  let subscriber, publisher;

  // const express = require("express");
  // const app = express();
  // const session = require("express-session");
  //
  // const connectRedis = require("connect-redis");
  // const RedisStore = connectRedis(session);
  const redis = require("redis");
  let redisClient;

  if (process.env.NODE_ENV === "development") {
    redisClient = redis.createClient({
      host: env.REDIS_HOST,
      port: env.REDIS_PORT,
    });
  } else {
    redisClient = redis.createClient({
      host: env.REDIS_HOST,
      port: parseInt(env.REDIS_PORT),
      auth_pass: env.REDIS_AUTH_PASS,
      tls: {
        ca: [Buffer.from(fs.readFileSync(env.REDIS_CA_PATH), "base64")],
      },
    });
  }

  redisClient.on("error", (err) => {
    Logger.error(
      `Could not establish a connection with redis websocket session: ${err}`
    );
  });

  redisClient.on("connect", () => {
    Logger.info(`Connected to websocket session redis successfully`);

    subscriber = redisClient.duplicate();

    subscriber.on("error", (err) => {
      Logger.error("Subscriber was not connected successfully to redis db...");
    });

    subscriber.on("connect", async () => {
      Logger.info("Subscriber connected successfully...");
    });

    publisher = redisClient.duplicate();
    // await publisher.connect();

    publisher.on("error", (err) => {
      Logger.error("Publisher was not connected successfully to redis db...");
      resolve();
    });

    publisher.on("connect", () => {
      Logger.info("Publisher connected successfully...");
    });
  });

  // app.use(
  //   session({
  //     store: new RedisStore({ client: redisClient }),
  //     secret: env.SESSION_SECRET,
  //     resave: true,
  //     saveUninitialized: true,
  //     proxy: true,
  //     key: "JSESSIONID_WS",
  //     cookie: {
  //       secure: true,
  //       httpOnly: true,
  //       sameSite: "lax",
  //     },
  //   })
  // );

  const wss = new WebSocketServer({
    noServer: true,
    path: "/websocket/",
  });

  // function heartbeat() {
  //   this.isAlive = true;
  // }

  wss.getUniqueID = () => {
    s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };
    return s4() + s4() + "-" + s4();
  };

  wss.on("connection", (ws) => {
    Logger.info("a new client connected...");
    ws.id = wss.getUniqueID();
    // redisClient.set(ws.id, JSON.stringify(ws));
    // console.log("sprawdzmy czy cos zapisałem sensownego...");
    // redisClient.get(ws.id, (err, data) => {
    //   if (!err) {
    //     console.log({ data });
    //   } else {
    //     console.log(err);
    //   }
    // });
    // console.log("sprobuje zrobic loga id...");
    // req.session.uid = ws.id;
    // console.log("req session uid: ", req.session.uid);
    // session(
    //   req,
    //   {
    //     store: new RedisStore({ client: redisClient }),
    //     secret: env.SESSION_SECRET,
    //     resave: true,
    //     saveUninitialized: true,
    //     proxy: true,
    //     key: "JSESSIONID_WS",
    //     cookie: {
    //       secure: true,
    //       httpOnly: true,
    //       sameSite: "lax",
    //     },
    //   },
    //   () => {
    //     console.log("Session is parsed!");
    //   }
    // );
    // ws.isAlive = true;
    // ws.on("pong", heartbeat);
    // ws._socket.timeout = 60000;
    // console.log({ ws });
    // ws.isAlive = true;
    // ws.on("pong", heartbeat);
    // session(req.upgradeReq, {}, function () {
    //   console.log("cos chujowego");
    //   console.log(req.upgradeReq.session);
    //   // do stuff with the session here
    // });

    ws.on("message", (message) => {
      const message_json = JSON.parse(message.toString());
      if (message_json.event && message_json.event === "connect") {
        if (message_json.data) {
          Logger.info({ message_json });
          // const userId = message_json.data.userId;
          // check if user already has some websocket and terminate this
          // const length = localStorage.length;
          // for (let i = 0; i < length; i++) {
          //   const userId = localStorage.key(i);
          //   const wsId = localStorage.getItem(userId);
          //   if (userId === message_json.data.userId) {
          //     if (wsId != ws.id) {
          //       for (const client of wss.clients) {
          //         if (client.id === wsId) {
          //           console.log("client already exists... closing " + wsId);
          //           client.close();
          //         }
          //       }
          //     }
          //   }
          // }
          //
          // for (const client of wss.clients) {
          //   if (client.id === userId) {
          //     console.log("client already exists... closing " + userId);
          //     client.close();
          //     return;
          //   }
          // }

          // localStorage.setItem(userId, ws.id);
          // ws.id = userId;
          subscribeRedisChannel(ws.id, `user-data-${ws.id}`);
          // subscribeRedisChannel(ws.id, `phoneid-${ws.id}`);
          ws.send(
            JSON.stringify({
              event: "accept",
              data: { info: "user connected successfully", wsId: ws.id },
            })
          );
        } else {
          ws.send(
            JSON.stringify({
              event: "error",
              data: {
                info:
                  'websocket error - no data send or userId not found in message of type "register"',
              },
            })
          );
        }
      } else if (message_json.event && message_json.event === "ping") {
        // console.log("ping from client...");
        ws.send(JSON.stringify({ event: "pong" }));
      }

      // if (message_json.event && message_json.event === "pong") {
      //   console.log({ message_json });
      // }
      // ws.send(`got your message and its: ${message}`)
    });

    ws.on("error", (error) => {
      Logger.error({ error });
    });

    ws.on("close", (ev) => {
      redisClient.del(ws.id, (err, reply) => {
        Logger.error({ err });
        Logger.info({ reply });
        Logger.info(`disconnected and deleted: ${ws.id}`);
        // console.log(`event: ${ev}`);
      });
      // const length = localStorage.length;
      // for (const client of wss.clients) {
      //   if (client.id === userId) {
      //     console.log("client already exists... closing " + userId);
      //     client.close();
      //   }
      // }
      // for (let i = 0; i < length; i++) {
      //   const userId = localStorage.key(i);
      //   const wsId = localStorage.getItem(userId);
      //   if (ws.id === wsId) {
      //     localStorage.removeItem(userId);
      //   }
      // }
    });
  });

  // const interval = setInterval(function ping() {
  //   wss.clients.forEach(function each(ws) {
  //     if (ws.isAlive === false) return ws.terminate();
  //
  //     ws.isAlive = false;
  //     ws.ping();
  //   });
  // }, 5000);

  wss.on(
    "close",
    (close = (ev) => {
      Logger.info(`wss close: ${ev}`);
      // clearInterval(interval);
    })
  );

  const subscribeRedisChannel = async (wsId, channel) => {
    try {
      // const channel = `user-data-${wsId}`;
      Logger.info(`Creating subscriber for channel: ${channel}`);
      await subscriber.subscribe(channel, (message) => {
        Logger.info(`Subscriber created successfully`); // 'message'
        Logger.info(`Setting listening for messages on channel ${channel}`);
        subscriber.on("message", async (channel, message) => {
          Logger.info(`On message for channel ${channel} / wsId: ${wsId}`);
          if (message && message != null) {
            // odeslij dane do uzytkownika bo na tej maszynce jest polaczenie websocket
            try {
              await sendMessage(wsId, message);
            } catch (err) {
              Logger.error(err);
            }
            Logger.info(`Unsubscribe channel ${channel}`);
            await subscriber.unsubscribe(channel);
          }
        });

        // else setSubscriber(channel);
      });
      // await subscriber.pSubscribe(channel, async (message, channel) => {
      //   Logger.info(`Received message by subscriber: ${message}`); // 'message'
      //   if (message && message != null) {
      //     Logger.info(`Unsubscribe channel ${channel}`);
      //     await subscriber.unsubscribe(channel);
      //     // odeslij dane do uzytkownika bo na tej maszynce jest polaczenie websocket
      //   }
      // });
      // Logger.info(`Listening on subscribed channel: ${channel}`);

      // await subscriber.connect();
    } catch (err) {
      Logger.error(err);
    }
  };

  const userIdToWsIdMapper = (userId) => {
    return new Promise((resolve, reject) => {
      const crypto = require("crypto");
      const hash = crypto
        .createHash("sha256", env.SECRET)
        .update(userId)
        .digest("hex");
      Logger.info(`Getting wsId for user hash ${hash}`);
      redisClient.hget(hash, "wsId", async (err, wsId) => {
        if (err) {
          reject(err);
        }
        Logger.info(`found wsId: ${wsId} for user: ${hash}`);
        resolve(wsId);
      });
    });
  };

  const publishRedisChannelData = async (channel, data) => {
    // return new Promise(async (resolve, reject) => {
    try {
      // const channel = `user-data-${wsId}`;
      Logger.info(`Publishing message to channel: ${channel}`);
      await publisher.publish(channel, JSON.stringify(data));
      Logger.info("Message published");
      // Logger.info(`Publisher quitting...`);
      // publisher.quit();
      // Logger.info("Publisher connected successfully to redis db...");
    } catch (err) {
      Logger.error(err);
      resolve();
    }
    // });
  };

  const sendMessage = async (wsId, message) => {
    return new Promise((resolve, reject) => {
      try {
        // const userId = _userId;
        // const crypto = require("crypto");
        // const hash = crypto
        //   .createHash("sha256", env.SECRET)
        //   // updating data
        //   .update(_userId)
        //   // Encoding to be used
        //   .digest("hex");
        // console.log({ hash });
        // redisClient.hget(hash, "wsId", (err, wsId) => {
        // if (err) {
        //   reject(err);
        // }
        Logger.info(`Sending message to wsId: ${wsId}`);
        // redisClient.get("clients", (err, clients) => {
        // if (err) {
        //   throw `no user found - user ${_userId} is not connected`;
        // } else {
        for (const client of wss.clients) {
          if (client.id === wsId) {
            Logger.info(`Message sent successfully!`);
            client.send(message);
            resolve();
          }
        }
        reject(
          `User with wsId: ${wsId} was not found on websocket server, message was not sent`
        );
        // console.log("[WS] sending a message to: ", {
        //   wsId,
        // });
        // console.log({ client });
        // JSON.parse(client).send(JSON.stringify(message));
        // return;
        // }
        // });
        // for (const client of wss.clients) {
        //   console.log("aktywny klient id: ", client.id);
        //   if (client.id === wsId) {
        //     console.log("[WS] sending a message to: ", {
        //       wsId,
        //     });
        //     client.send(JSON.stringify(message));
        //     return;
        //   }
        // }
        // });
      } catch (err) {
        reject(err);
      }
    });
  };

  // const onMessage = async (wsId, callback) => {
  //   try {
  //     // const userId = _userId;
  //     // const crypto = require("crypto");
  //     // const hash = crypto
  //     //   .createHash("sha256", env.SECRET)
  //     //   // updating data
  //     //   .update(_userId)
  //     //   // Encoding to be used
  //     //   .digest("hex");
  //     // // console.log({ hash });
  //     //
  //     // redisClient.hget(hash, "wsId", (err, wsId) => {
  //     //   if (err) {
  //     //     throw err;
  //     //   } else {
  //     // console.log(`getting clients from redis`);
  //     // redisClient.get("clients", (err, clients) => {
  //     // if (!err) {
  //     for (const client of wss.clients) {
  //       if (client.id === wsId) {
  //         Logger.info(`[WS] on message for client: ${wsId}`);
  //         callback(client);
  //       }
  //     }
  //     //   console.log("[WS] on message for client: ", { wsId });
  //     //   callback(client);
  //     // } else {
  //     //   throw "user not found";
  //     // }
  //     // });
  //     // }
  //     // });
  //   } catch (err) {
  //     throw err;
  //   }
  // };

  const setClient = async (wsUID, userId) => {
    return new Promise(async (resolve, reject) => {
      try {
        const crypto = require("crypto");
        const hash = crypto
          .createHash("sha256", env.SECRET)
          // updating data
          .update(userId)
          // Encoding to be used
          .digest("hex");
        Logger.info(`Setting a userId to wsId mapper in redis`);
        await redisClient.hset(hash, "wsId", wsUID, redis.print);
        resolve();
      } catch (err) {
        Logger.error("Couldn't set userId to wsId mapper in redis: " + err);
        reject(err);
      }
    });
  };

  server.on("upgrade", (request, socket, head) => {
    wss.handleUpgrade(request, socket, head, (websocket) => {
      wss.emit("connection", websocket, request);
    });
  });

  return {
    // sendMessage: sendMessage,
    // onMessage: onMessage,
    userIdToWsIdMapper: userIdToWsIdMapper,
    setClient: setClient,
    publishRedisChannelData: publishRedisChannelData,
  };
};

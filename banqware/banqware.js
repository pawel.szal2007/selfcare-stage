let env = require("dotenv").config();
const dotenvParseVariables = require("dotenv-parse-variables");
env = dotenvParseVariables(env.parsed);
const Logger = require("../winston-config");

exports.getSingleUser = (obj) => {
  return new Promise((resolve, reject) => {
    // ${env.BANQUP_PROXY_API}
    let url = `http://10.205.2.11:8448${env.BANQUP_PROXY_API_GET_SINGLE_USER}${obj.userId}`;
    // console.log({ url });
    // for (const property in obj) {
    //   url += `${property}=${obj[property]}&`;
    // }
    Logger.info("GET REQUEST::");
    Logger.info({ url });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .get(url)
      .then((response) => {
        const data = response.data;
        // console.log({ data });
        resolve(data);
      })
      .catch((err) => {
        console.log({ err });
        reject(err);
      });
  });
};

exports.createNewUser = (userId) => {
  return new Promise((resolve, reject) => {
    let url = `http://10.205.2.11:8448${env.BANQUP_PROXY_API_CREATE_USER}`;
    const data = {
      businessContext: "individual",
      clientId: userId,
      updateOffline: true,
    };
    Logger.info("POST REQUEST::");
    Logger.info({ data });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .post(url, data)
      .then((response) => {
        const data = response.data;
        // L("addConsent response.data:", { data });
        resolve(data);
      })
      .catch((err) => {
        // const errObj = err.data;
        // console.log("addConsent response error:", { errObj });
        reject(err);
      });
  });
};

exports.createNewConsent = (obj) => {
  return new Promise((resolve, reject) => {
    let url = `http://10.205.2.11:8448${env.BANQUP_PROXY_API_CREATE_CONSENT}${obj.userId}/consent`;
    const data = {
      bankId: obj.bank_id,
      bankIdType: "id",
      consentAccountsCallbackUrl: env.BANQUP_CONSENT_ACCOUNTS_CALLBACK_URL,
      consentCallbackUrl: env.BANQUP_CONSENT_CALLBACK_URL,
      fetchTransactionDetails: true,
      redirectUrl: env.BANQUP_REDIRECT_URL,
      updateCallbackUrl: env.BANQUP_UPDATE_CALLBACK_URL,
      userBusinessContext: "individual",
      withUpdate: true,
    };
    const config = {
      headers: {
        "X-IP-Address": obj.ip,
      },
    };
    Logger.info("POST REQUEST::");
    Logger.info({ data });
    Logger.info({ config });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .post(url, data, config)
      .then((response) => {
        const data = response.data;
        // L("addConsent response.data:", { data });
        resolve(data);
      })
      .catch((err) => {
        // const errObj = err.data;
        // console.log("addConsent response error:", { errObj });
        reject(err);
      });
  });
};

exports.getSingleConsent = (obj) => {
  return new Promise((resolve, reject) => {
    // ${env.BANQUP_PROXY_API}
    let url = `http://10.205.2.11:8448${env.BANQUP_PROXY_API_GET_SINGLE_CONSENT}${obj.userId}/consent/${obj.consentId}`;
    Logger.info("GET REQUEST::");
    Logger.info({ url });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .get(url)
      .then((response) => {
        const data = response.data;
        // console.log({ data });
        resolve(data);
      })
      .catch((err) => {
        console.log({ err });
        reject(err);
      });
  });
};

exports.getSingleBankAccount = (obj) => {
  return new Promise((resolve, reject) => {
    // ${env.BANQUP_PROXY_API}
    let url = `http://10.205.2.11:8448${env.BANQUP_PROXY_API_GET_SINGLE_BANK_ACCOUNT}${obj.userId}/account/id/${obj.accountId}`;
    Logger.info("GET REQUEST::");
    Logger.info({ url });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .get(url)
      .then((response) => {
        const data = response.data;
        // console.log({ data });
        resolve(data);
      })
      .catch((err) => {
        console.log({ err });
        reject(err);
      });
  });
};

exports.getListOfTransactions = (obj) => {
  return new Promise((resolve, reject) => {
    // ${env.BANQUP_PROXY_API}
    let url = `http://10.205.2.11:8448${env.BANQUP_PROXY_API_GET_LIST_OF_TRANSACTIONS}${obj.userId}/account/id/${obj.accountId}/transactions`;
    Logger.info("GET REQUEST::");
    Logger.info({ url });
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const axios = require("axios");
    axios
      .get(url)
      .then((response) => {
        const data = response.data;
        // console.log({ data });
        resolve(data);
      })
      .catch((err) => {
        console.log({ err });
        reject(err);
      });
  });
};
